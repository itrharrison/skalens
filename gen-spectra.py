import numpy as np
import os
import pdb

clkappa_dir = '/home/harrison/clkappa_theory/'

def run_spectra(spaces, parameters, values):

  for space in spaces:
    pars = parameters[space]
    for fid in pars:
      val1 = values[fid][0]
      nfid = pars[~(pars==fid)][0]
      variable = values[nfid]
      for val2 in variable:
        name = 'fisher-{0}-{5}-{1}{2}-{3}{4}'.format('cl_kappa',
                                                 fid, val1,
                                                 nfid, val2, space)

        run_dir = './fisher-points/{0}/'.format(name)
        
        if not os.path.exists(run_dir):
          os.makedirs(run_dir)
          os.makedirs(run_dir+'theory_cls')

        ini_filename = run_dir+'params_clkappa_zdist.dat'.format(name)
        template = '{1}params_clkappa_zdist.{0}.template'.format(space, clkappa_dir)
        ini_template = open(template).read()

        if fid==pars[0]:
          ini_file = ini_template.format(value1=val1, value2=val2)
          #pdb.set_trace()
        else:
          ini_file = ini_template.format(value1=val2, value2=val1)

        open(ini_filename, 'w').write(ini_file)

        os.system('cp {1}clkappa_zdist {0}'.format(run_dir, clkappa_dir))
        os.chdir(run_dir)
        cmd = './clkappa_zdist params_clkappa_zdist.dat'
        #pdb.set_trace()
	os.system(cmd)
	
        print(os.getcwd(), cmd)
        os.chdir('../../')

if __name__=='__main__':
  
  spaces = [
            'w0wa',
            'matter'
            ]

  parameters = {
                'w0wa' : np.array(['w0', 'wa']),
                'matter' : np.array(['omega_m','sigma_8'])
                }
  
  values = {'w0' : [-1.0, -1.05, -0.95, -1.025, -0.975, -1.0125, -0.9875, -0.98125, -1.01875, -0.9625, -1.0375, -1.1, -0.9, -1.00625, -0.99375],
            'wa' : [0.0, 0.05, -0.05, 0.025, -0.025, 0.0125, -0.0125, 0.01875, -0.01875, 0.0375, -0.0375, -0.1, 0.1, -0.00625, 0.00625],
            'omega_m' : [0.262, 0.162,  0.212,  0.312,  0.362],
            'sigma_8' : [0.798, 0.758, 0.718, 0.838, 0.878]
            }

  run_spectra(spaces, parameters, values)
