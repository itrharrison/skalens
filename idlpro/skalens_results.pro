; May 2015
; assess results for lensing pipeline
;comparison of input theory spectra with measured spectra from the simulation
; computation of covariance matrix for FoMs and writing to file (IDL
; inversion not good enough)

pro run_run
;thrs=[0.4,0.42,0.44,0.46,0.48,0.5,0.52,0.54,0.56,0.58,0.6]
;thrs=[0.3,0.4,0.5]
thrs=[0.4]
for j=0,n_elements(thrs)-1 do begin
thr=thrs(j)
PRINT,'*************',THR
run_pipeline,thr
endfor
end

pro run_pipeline,thr
; run the processing for all listed folders in path. 
path='/share/c5/harrison/skalens/'
;path='/share/c4/harrison/skalens/'
;path='/share/c8/bonaldi/skalens_runs/'
;spawn,'ls '+path,dirs

;dirs=['ska1-mid-b2016-05-11-084059','ska1-mid-b12016-05-11-084101','ska1-mid-a2016-05-11-084048','ska1-mid-a12016-05-11-084053','ska1-mid-a22016-05-11-084056']
dirs=['mighteeplus-tier12016-07-02-131944','mighteeplus-tier22016-07-02-131946','mighteeplus-tier32016-07-02-132058']
;stop
;thr=0.6

close,10
openw,10,'FOMS_'+strcompress(thr,/remove_all)+'.txt'

for i=0,n_elements(dirs)-1 do begin
dir=dirs(i)
n=strlen(dir)
tag=strmid(dir,n-3,n)
if (tag eq 'png') then goto, next
;test=where(dir eq exclude)
;if test(0) eq -1 then begin
pipeline,dir,path,thr
print,dir
;printf,10,dir

;endif
next:
endfor

close,10
end




pro pipeline,dir,path,thr

;for the tag I remove the last 17 characters containing the date
n=strlen(dir)
tag=strmid(dir,0,n-17)
print,dir,' ',tag

dir2=path+dir

spawn,'ls '+dir2+'/theory_spectra.sav',result2
;stop
if (result2(0) eq '') then begin
; combine theory spectra for the first 5 small redshift bins
average_theory_new,dir2  
run_combine,dir2
endif
run_fig,dir2,tag ; produce triangular plot
; compute full convariance and figure of merit
longcl,dir2,tag
;fullcovariance,dir2,tag,thr

end


pro longcl,dir0,tag
 ; construct spectrum with all auto and cross-spectra 
  readcol,'binfile_log.txt',l1,l2
  nbinl=n_elements(l1)
  nbins=6
  n_iter=96

  size=0
  for i=0,nbins-1 do begin
     for j=0,i do begin
        size=size+nbinl  
     endfor
  endfor

  longcl=fltarr(size)

  for ii=1,n_iter do begin
     is=0
     longcl(*)=-1.              ;flag
     iis=strcompress(ii,/remove_ALL)
     dir=dir0+'/realisation_'+iis+'/'
     filename=dir+'longcl.txt'

     for i=0,nbins-1 do begin
        for j=0,i do begin
           bin1=strcompress(j+1,/remove_all)
           bin2=strcompress(i+1,/remove_all)


           
;check if file exists

           close,1 & OPENR, 1,dir+'specdec/cl_'+bin2+bin1+'_'+tag+'_dec.txt',error=err1
           close,1 & OPENR, 1,dir+'specdec/cl_noise_'+bin2+bin2+'_'+tag+'_dec.txt',error=err2

           if ((err1 eq 0) and (err2 eq 0)) then begin
              readcol,dir+'specdec/cl_'+bin2+bin1+'_'+tag+'_dec.txt',l1,l2,e,b,eb,/silent
              readcol,dir+'specdec/cl_noise_'+bin2+bin2+'_'+tag+'_dec.txt',l1,l2,noie,noib,noieb,/silent

              if (bin1 ne bin2) then noie=noie*0 & noib=noib*0 & noieb=noieb*0. 
; noise bias subtraction
              e=e-noie
              b=b-noib
              eb=eb-noieb
;print,ii
;stop
              if (n_elements(e) eq nbinl) then longcl(is:is+nbinl-1)=e
              ;print,is,is+nbinl-1

           endif

           is=is+nbinl

           
        endfor
     endfor

     close,1
     openw,1,filename,error=err3
     if (err3 eq 0) then begin
        for s=0,size-1 do begin
           printf,1,longcl(s)
        endfor
     endif
     close,1
  endfor

end


pro fullcovariance,dir0,tag,thr


  readcol,'binfile_log.txt',l1,l2
  nbinl=n_elements(l1)
  nbins=6
  n_iter=96
  thrs=strcompress(thr,/remove_all)
  filename=dir0+'/covariance_'+thrs+'.txt'
  filename_full=dir0+'/covariance_full.txt'
  filename_out=dir0+'/covariance_inv.dat'
  filename_test=dir0+'/covariance_invtest.dat'
  filename_fom=dir0+'/fom.dat'
  filename_pb=dir0+'/Pb.dat'

  size=0
  for i=0,nbins-1 do begin
     for j=0,i do begin
        size=size+nbinl  
     endfor
  endfor
  print,size


  fom=0.
  flag=-1.
  bigarray=fltarr(size,n_iter)
  bigarray(*,*)=-1
  for ii=1,n_iter do begin
                                ;is=0
                                ;longcl(*)=-1.              ;flag
     iis=strcompress(ii,/remove_ALL)
     dir=dir0+'/realisation_'+iis+'/'
     filenamel=dir+'longcl.txt'
     close,1 & openr,1,dir+'longcl.txt',err=error
     if (error eq 0) then begin
        readcol,filenamel,row
        bigarray(*,ii-1)=row
     endif
  endfor
  
  bad=where(bigarray eq -1.)

if (n_elements(bad) gt n_elements(bigarray)/1.2) then goto,out

  fullcov=fltarr(size,size)
  mask=fltarr(size,size)
  pb=fltarr(size)

  for bin1=0,size-1 do begin

     good1=where(bigarray(bin1,*) ne -1)
     pb(bin1)=mean(bigarray(bin1,good1))

;print,n_elements(good)
     for bin2=0,bin1 do begin

        good2=where(bigarray(bin2,*) ne -1)

        good=setintersection(good1,good2)
        x=bigarray(bin1,good)
        y=bigarray(bin2,good)
        c=CORRELATE(X, Y)
        mask(bin1,bin2)=c
        mask(bin2,bin1)=c
        c2=CORRELATE(X, Y,/covariance)
        fullcov(bin1,bin2)=c2
        fullcov(bin2,bin1)=c2

       ; print,bin1,bin2

     endfor

  endfor
  write_cov,fullcov,filename_full

  noise=where(abs(mask) lt thr)
  fullcov(noise)=0.


  write_cov,fullcov,filename

  close,1
  openw,1,filename_pb
  for i=0,size-1 do begin
     printf,1,pb(i)
  endfor
  close,1

  if (total(fullcov) ne 0.) then begin
     close,1
     openw,1,'mathematica_inversion.txt'
     printf,1,'SafeInverse[Matrix_]:=Transpose[Eigenvectors[Matrix]].DiagonalMatrix[1/Eigenvalues[Matrix]].Inverse[Transpose[Eigenvectors[Matrix]]]'
     printf,1,'Cov=Import["'+filename+'","Table"];'
;     printf,1,'InvCov=SafeInverse[Cov];'
     printf,1,'InvCov=Inverse[Cov];'
     printf,1,'Export["'+filename_out+'",InvCov];'
     printf,1,'quantofaschifo=Max[Abs[InvCov.Cov-IdentityMatrix[Length[Cov]]]];'
     printf,1,'Export["'+filename_test+'",quantofaschifo];'

     printf,1,'Pb=Import["'+filename_pb+'","Table"];'
     printf,1,'fom=Transpose[Pb].InvCov.Pb'
     printf,1,'Export["'+filename_fom+'",fom];'
     close,1

     cinv=0
     
     print,'inversion'
     spawn,'/share/apps/Mathematica/9.0/bin/math -script mathematica_inversion.txt'
     print,'done'
     ;readmatrix,cinv,filename_out
     readmatrix,test2,filename_test
     flag=test2(0)

     readmatrix,fom,filename_fom

;  close,1
;     openw,1,'mathematica_inversion.txt'
;     printf,1,'SafeInverse[Matrix_]:=Transpose[Eigenvectors[Matrix]].DiagonalMatrix[1/Eigenvalues[Matrix]].Inverse[Transpose[Eigenvectors[Matrix]]]'
;     printf,1,'Cov=Import["'+filename+'","Table"];'
;     printf,1,'InvCov=SafeInverse[Cov];'
;;     printf,1,'InvCov=Inverse[Cov];'
;     printf,1,'Export["'+filename_out+'",InvCov];'
;     printf,1,'quantofaschifo=Max[Abs[InvCov.Cov-IdentityMatrix[Length[Cov]]]];;'
;     printf,1,'Export["'+filename_test+'",quantofaschifo];';

;     printf,1,'Pb=Import["'+filename_pb+'","Table"];'
;     printf,1,'fom=Transpose[Pb].InvCov.Pb'
;     printf,1,'Export["'+filename_fom+'",fom];'
;     close,1

;     cinv=0
     
 ;    print,'inversion2'
 ;    spawn,'/share/apps/Mathematica/9.0/bin/math -script mathematica_inversion.txt'
 ;    print,'done'
 ;    ;readmatrix,cinv,filename_out
 ;    readmatrix,test2,filename_test
 ;    flag2=test2(0)

 ;    readmatrix,fom2,filename_fom

;     if (flag2 lt flag) then begin
;        print,'better safeinverse'
;        fom=fom2
;        flag=flag2
;     endif
     fom=fom(0)
  endif
out:
n=strlen(dir0)
dir=strmid(dir0,31,n)
;print,dir
;stop
print,fom(0),flag
printf,10,fom(0),flag,'    '+dir
;save,fom,flag,filename=dir0+'/fom.sav'
wait,1
;stop
if (flag gt 1.) then stop
end


pro run_fig,dir,tag             ;,fom

  readcol,'binfile_log.txt',l1,l2
  ;readcol,'/local2/scratch/Bonaldi/lensing/binfile_ian.txt',l1,l2
  nbinl=n_elements(l1)
  lbin=(l1+l2)/2
  filename=dir+'/covariance.txt'
  filename_out=dir+'/covariance_inv.dat'
  restore,filename=dir+'/theory_spectra.sav'
  nl=n_elements(array(0,0,*))
  l=lindgen(nl)
  nbins=n_elements(array(*,0,0))

  results=fltarr(nbins,nbins,nbinl,2) ;mean, stddev
  Pb=fltarr(nbins*nbinl)
  dcov=fltarr(nbins*nbinl)
  ells=fltarr(nbins*nbinl)      ;containing multipoles
  nspecs=intarr(nbins,nbins)
  covariance=fltarr(nbins*nbinl,nbins*nbinl)
  correlation=covariance

  setcolors
;set_plot,'ps'

  str=[textoidl('0\leq z \leq 0.5'),textoidl('0.5\leq z\leq 1'),textoidl('1\leq z\leq 1.6'),textoidl('1.6\leq z\leq 2.2'),textoidl('2.2\leq z\leq 3.5'),textoidl('3.5\leq z\leq 10')]


;device,filename=dir+'compare_spectra_dec.ps',/color

  i=0
  j=0
;,l,etrue,lbin,emean,eerr
  DEVICE, GET_DECOMPOSED=old_decomposed
  DEVICE, DECOMPOSED=0
;setcolors
  LOADCT, 14
; Create an image and display it

;WINDOW, 1, XSIZE=300, YSIZE=300

; Write a PNG file to the temporary directory
; Note the use of the TRUE keyword to TVRD


  WINDOW, /FREE, XSIZE=1000, YSIZE=1000
  trim=0.1
  size=(1.-2.*trim)/nbins

  my_ytitle='!12l!17(!12l!17+1)C!12!Dl!N!17!U!N/2!7p!17';(!7l!17K!U2!N!17)'
  my_xtitle='!12l!17'

  xti_no=[' ',' ',' ',' ',' ']
  yti_no=[' ',' ',' ',' ',' ',' ']

  for i=0,nbins-1 do begin
     for j=0,i do begin
        xti=xti_no
        yti=yti_no
        xtit=' '
        ytit=' '
;if (i eq 0) then 
        if (j eq i) then begin
           if (i eq 0) then begin
              yti=[textoidl('10^-^7'),textoidl('10^-^6'),textoidl('10^-^5'),textoidl('10^-^4'),textoidl('10^-^3')]
              xti=[textoidl('10^1'),textoidl('10^2'),textoidl('10^3'),' ']
;ytit=textoidl('l(l+1)C_l/2\pi')
              ytit=my_ytitle
;              readcol,'/local2/scratch/Bonaldi/lensing/C^k_l-125.dat',lstef,clstef

           endif else if (i eq nbins-1) then begin

              yti=[textoidl('10^-^7'),textoidl('10^-^6'),textoidl('10^-^5'),textoidl('10^-^4'),' ']
              xti=[textoidl('10^1'),textoidl('10^2'),textoidl('10^3'),textoidl('10^4')]
;xtit='l'
              xtit=my_xtitle
           endif else begin
              yti=[textoidl('10^-^7'),textoidl('10^-^6'),textoidl('10^-^5'),textoidl('10^-^4'),' ']
              xti=[textoidl('10^1'),textoidl('10^2'),textoidl('10^3'),' ']
           endelse

;filename = FILEPATH('test.png', /TMP)



        endif


        etrue=array(j,i,*)
        lbin=(l1+l2)/2
;call to read results
        testspec,dir,tag,nbinl,j+1,i+1,l,etrue,ebin,eerr,ns
    
        results(j,i,*,0)=ebin
        results(j,i,*,1)=eerr
        nspecs(j,i)=ns
        lbin_old=lbin

; size of panels
        ylim_lo=1.e-7  
        ylim_up=1.e-3

        lbin_lo=10
        lbin_up=1.e4

;stop

        plot,alog10(l),alog10(etrue),position=[trim+i*size,1-trim-(j+1)*size,trim+(i+1)*size,1-trim-j*size],/noerase,xtickn=xti_no,ytickn=yti_no,xra=[alog10(lbin_lo),alog10(lbin_up)],yra=[alog10(ylim_lo),alog10(ylim_up)],xthick=0,ythick=0



        if (ns ge 30) then begin
           ebin_lo=ebin-eerr
           bad=where(ebin_lo le ylim_lo)
           if (bad(0) ne -1) then ebin_lo(bad)=ylim_lo
           ebin_up=ebin+eerr
           bad=where(ebin_up ge ylim_up)
           if (bad(0) ne -1) then ebin_up(bad)=ylim_up
           ebin_lo=alog10(ebin_lo)
           ebin_up=alog10(ebin_up)
           lbin_up2=3000
           bad=where(lbin le lbin_lo)
           if (bad(0) ne -1) then lbin(bad)=lbin_lo
;bad=where(lbin ge lbin_up2)
;if (bad(0) ne -1) then lbin(bad)=lbin_up2
           lbin=alog10(lbin)

           x_poly=[lbin,reverse(lbin),lbin(0)]
           y_poly=[ebin_lo,reverse(ebin_up),ebin_lo(0)]


           polyfill,x_poly,y_poly,col=5 ;mycolor(3)
        endif
        plot_oo,l,etrue,xra=[lbin_lo,lbin_up],position=[trim+i*size,1-trim-(j+1)*size,trim+(i+1)*size,1-trim-j*size],xtickn=xti,ytickn=yti,/noerase,yra=[ylim_lo,ylim_up],xtitle=xtit,ytitle=ytit,xstyle=1,ystyle=1,charsize=1.5
;        if ((i eq 0) and (j eq 0)) then oplot,lstef,clstef,linestyle=2 ; CHECK!!!
        if (j eq 0) then xyouts,30,0.003,str(i),charsize=1.5
;if (i eq nbins-1) then xyouts,1000,1.e-5,j+1


     endfor
  endfor

  WRITE_PNG, dir+'/spectra.png', TVRD(/TRUE)
  lbin=lbin_old


  save,results,l1,l2,lbin,nspecs,filename=dir+'/simulation_outputs.sav'

  while !D.WINDOW ne -1 do wdelete ;close windows
  
;stop
; cov fig
;stop



end

pro write_cov,matrix,filename
;write covariance in text format for inverting with mathematica!
nrows=n_elements(matrix(*,0))
ncols=n_elements(matrix(0,*))

if (nrows ne ncols) then begin
print,'Error not a square matrix'
stop
endif

close,1
openw,1,filename

for i=0,nrows-1 do begin

row=''
for j=0,ncols-1 do begin
row=row+strcompress(matrix(i,j),/remove_all)+'   '   
endfor
printf,1,row
endfor
close,1


end


;version that computes covariance


pro average_theory_new,dir0
; compute theory power spectra for the big bins
; use catalogues to compute number of sources for each redshift bin

; adding bins 1-5 -> new bin 1
; adding bins 6+7 -> new bin 2
; adding bins 8+9 -> new bin 3
; read maps to find all densities

nbins=11; number of old bins
galaxies=fltarr(nbins)
area=5000.*(!pi/180.)^2 ;-> area sterad
for i=0,nbins-1 do begin
   galaxies(i)=0.
   bin1=strcompress(i+1,/remove_all)

   for ii=1,96 do begin
      file=dir0+'/realisation_'+strcompress(ii,/remove_all)+'/sampler/zbin'+bin1+'_cat.fits'
      spawn,'ls '+file,result
      if (result ne '') then goto,out2
endfor
out2:

   test=headfits(file)
   check=strcompress(test(0),/remove_all)

   if (check eq '-1') then begin ; empty file
      ngals=0
   endif else begin
      
      if (n_elements(test) eq 5) then test=headfits(file,exten=1) ; handle ian's format of the header
      
      ngals = SXPAR(test, 'NAXIS2')
   endelse
   galaxies(i)=ngals
   
endfor

save,galaxies,filename=dir0+'/ngalaxies_11bins.sav'

end


pro read_allspectra,array

nl=3000
nbins=11
array=fltarr(nbins,nbins,nl)

for i=0,nbins-1 do begin
for j=0,i do begin
bin1=strcompress(i+1,/remove_all)
bin2=strcompress(j+1,/remove_all)
if (i+1 lt 10) then bin1='0'+bin1
if (j+1 lt 10) then bin2='0'+bin2
;print,bin1,bin2
readcol,'/home/harrison/clkappa_theory/theory_cls/cl_kk_'+bin2+bin1+'.dat',l,etrue,numline=nl,/silent
array(i,j,*)=etrue
array(j,i,*)=etrue
endfor
endfor

end




pro combine,bin1,bin2,weights,weights_out,array_in,array_out
; specarray: all spectra and crosspectra
;i,j to be combined
;array_in: nbinsXnbins
;array_out: (nbins-1)X(nbins-1)
;weights: nbins 

nbins=n_elements(weights)

array_out=array_in;*0.
weights_out=weights
weights_out(bin1)=weights(bin1)+weights(bin2)
weights_out(bin2)=0.

for i=0,nbins-1 do begin
   if (i eq bin1) then begin
   array_out(bin1,i,*)=weights(bin1)^2*array_in(bin1,bin1,*)+weights(bin2)^2*array_in(bin2,bin2,*)+weights(bin1)*weights(bin2)*array_in(bin1,bin2,*)*2.

   totweight=weights(bin1)^2+weights(bin2)^2+weights(bin1)*weights(bin2)*2.
   array_out(bin1,i,*)=array_out(bin1,i,*)/totweight
   endif else begin
      array_out(bin1,i,*)=weights(bin1)*array_in(i,bin1,*)+weights(bin2)*array_in(i,bin2,*)
      array_out(bin1,i,*)=array_out(bin1,i,*)/weights_out(bin1)
   endelse
endfor
array_out(*,bin1,*)=array_out(bin1,*,*)
array_out(bin2,*)=0.
array_out(*,bin2)=0.
end

pro cancel_row,i,array,vector
  nbins=n_elements(array(*,0,0))
  nl=n_elements(array(0,0,*))
  print,nbins,nl
  array_copy=array
  mask=fltarr(nbins,nbins)
  mask(*,*)=1.
  mask(i,*)=0.
  mask(*,i)=0.

  array=fltarr(nbins-1,nbins-1,nl)

  ii=0
  for i=0,nbins-1 do begin
     good=where(mask(i,*) ne 0)
     ;print,i
     if (good(0) ne -1) then begin
        for l=0,nl-1 do begin
           array(ii,*,l)=array_copy(i,good,l)
        endfor
        ii=ii+1
     endif
  endfor

good=where(vector ne 0.)
vector=vector(good)


end


pro run_combine,dir

restore,filename=dir+'/ngalaxies_11bins.sav'

weights=galaxies/total(galaxies)
read_allspectra,array

;weights_out=weight
;array_out=array
; bin 6 and 7 into bin 7


print,'combining bins 1,2,3,4,5'
combine,0,1,weights,weights_out,array,array_out ; bin 1 combined
cancel_row,1,array_out,weights_out
array=array_out
weights=weights_out

combine,0,1,weights,weights_out,array,array_out ; bin 2 combined
cancel_row,1,array_out,weights_out
array=array_out
weights=weights_out

combine,0,1,weights,weights_out,array,array_out ; bin 3 combined
cancel_row,1,array_out,weights_out
array=array_out
weights=weights_out

combine,0,1,weights,weights_out,array,array_out ; bin 4 combined
cancel_row,1,array_out,weights_out
array=array_out
weights=weights_out

combine,0,1,weights,weights_out,array,array_out ; bin 4 combined
cancel_row,1,array_out,weights_out
array=array_out
weights=weights_out

;; print,'combining bins 6 and 7'
;; combine,1,2,weights,weights_out,array,array_out ; bin 6 and 7 into bin6
;; cancel_row,2,array_out,weights_out
;; array=array_out
;; weights=weights_out


;; print,'combining bins 7 and 8'
;; combine,2,3,weights,weights_out,array,array_out ; bin 7 and 8 into bin7
;; cancel_row,3,array_out,weights_out
;; array=array_out
;; weights=weights_out

save,array,weights,filename=dir+'/theory_spectra.sav'

end

pro plot_newspectra,dir
setcolors
restore,filename=dir+'theory_spectra.sav'

plot_oo,array(0,0,*),xra=[2,3000],yra=[1.e-9,1.e-2],xtitle='l',ytitle='l(l+1)Cl/2pi'
oplot,array(1,1,*),col=mycolor(1)
oplot,array(2,2,*),col=mycolor(2)
oplot,array(3,3,*),col=mycolor(3)
oplot,array(4,4,*),col=mycolor(4)
oplot,array(5,5,*),col=mycolor(5)
;oplot,array(6,6,*),col=mycolor(6)

;oplot,array(0,1,*),linestyle=2,col=mycolor(2)
;oplot,array(0,2,*),linestyle=2,col=mycolor(3)
;oplot,array(1,2,*),linestyle=2,col=mycolor(5)
legend,['11','22','33','44','55','66'],col=[mycolor(0),mycolor(1),mycolor(2),mycolor(3),mycolor(4),mycolor(5)],lines=[0,0,0,0,0,0]
read_allspectra,array
for i=0,10 do begin
oplot,array(i,i,*),linestyle=1
xyouts,3500,array(i,i,2900),strcompress(i,/remove_all)
endfor


end



pro testspec,dir0,tag,nbins,bin1,bin2,l,etrue,meane,erre,nspecs
;print,bin1,bin2

bin1=strcompress(fix(bin1),/remove_all)
bin2=strcompress(fix(bin2),/remove_all)

n_iter=96
e_array=fltarr(nbins,n_iter)
;stop
e_array(*,*)=-1

nspecs=0
for ii=1,n_iter do begin
iis=strcompress(ii,/remove_ALL)
dir=dir0+'/realisation_'+iis+'/specdec/'

;check if file exists

close,1 & OPENR, 1,dir+'cl_'+bin2+bin1+'_'+tag+'_dec.txt',error=err1
close,1 & OPENR, 1,dir+'cl_noise_'+bin2+bin2+'_'+tag+'_dec.txt',error=err2
;stop
if ((err1 eq 0) and (err2 eq 0)) then begin
readcol,dir+'cl_'+bin2+bin1+'_'+tag+'_dec.txt',l1,l2,e,b,eb,/silent
readcol,dir+'cl_noise_'+bin2+bin2+'_'+tag+'_dec.txt',l1,l2,noie,noib,noieb,/silent

if (bin1 ne bin2) then noie=noie*0 & noib=noib*0 & noieb=noieb*0. 
; noise bias subtraction
e=e-noie
b=b-noib
eb=eb-noieb

;print,ii
if (n_elements(e) eq nbins) then e_array(*,nspecs)=e
nspecs=nspecs+1

endif
;stop

endfor

;stop
meane=fltarr(nbins)
erre=fltarr(nbins)

if (nspecs ge 30) then begin
for lbin=0,nbins-1 do begin
good=where(e_array(lbin,*) ne -1)
;print,n_elements(good)
meane(lbin)=mean(e_array(lbin,good))
erre(lbin)=stddev(e_array(lbin,good))
endfor

; write results in txt file for Ian
close,1 & OPENW, 1,dir0+'/cl_'+bin2+bin1+'_'+tag+'.txt'
for lbin=0,nbins-1 do begin
printf,1,l1(lbin),l2(lbin),meane(lbin),meane(lbin)-erre(lbin),meane(lbin)+erre(lbin)
endfor
close,1



endif





end


pro tags4stef
; match folders to numbers for tagging the cov files for stefano

path='/share/c8/bonaldi/skalens_runs/'
;spawn,'ls '+path,dirs

thr=0.4
readcol,'Run_tags.txt',id,dirs,format='a,a'
;close,11
;openw,11,'Run_tags.txt'

close,10
openw,10,'FOMS_'+strcompress(thr,/remove_all)+'.txt'
for i=0,n_elements(dirs)-1 do begin
;is=strcompress(i,/remove_all)
is=id(i)
dir=dirs(i) 
dir2=path+dir
n=strlen(dir)
tag=strmid(dir,n-3,n)
if (tag eq 'png') then goto, next
print,dir
;printf,11,i,'   ',dir
fullcovariance,dir2,tag,thr
com='cp '+dir2+'/covariance_0.300000.txt /home/bonaldi/lensing/covariances/covariance_0.3_'+is+'.txt'
spawn,com

com='cp '+dir2+'/covariance_0.400000.txt /home/bonaldi/lensing/covariances/covariance_0.4_'+is+'.txt'
spawn,com

com='cp '+dir2+'/covariance_0.500000.txt /home/bonaldi/lensing/covariances/covariance_0.5_'+is+'.txt'
spawn,com

com='cp '+dir2+'/covariance_full.txt /home/bonaldi/lensing/covariances/covariance_'+is+'_full.txt'
spawn,com
next:
endfor

;close,11
close,10
end

pro theoryspectra4stef
readcol,'Run_tags.txt',id,name,format='a,a'
path='/share/c8/bonaldi/skalens_runs/'

;excl=['rb_hr_1.00e+00-_1.502015-11-16-152454','rb_hr_1.00e+00-_8.872015-11-10-185243','rb_hr_1.00e+00-_9.502015-11-14-171556','rb_hr_1.17e+00-_7.502015-11-10-185127','rb_hr_4.20e-01-_1.192015-11-14-171656','rb_hr_5.56e-01-_1.052015-11-11-203117']; not ready

;good=setdifference(name,excl)
;stop
n=n_elements(id)


;name=['ska22015-11-27-111226'];
;id='SKA2'
i=where(id eq '21')
;for i=0,n-1 do begin
dir2=path+name(i)+'/realisation_1_copy/sampler/fisher-points/'
spawn,'ls '+dir2,result
if (result(0) ne '') then begin ; only runs with FOM ne 0.
com='tar zcvf /home/bonaldi/lensing/theory_cls/fisherpoints_'+id(i)+'.tgz '+dir2
;test=where(name(i) eq excl)
;if (test(0) eq -1) then begin
print,com
spawn,com
;endif
endif
;endfor

end
