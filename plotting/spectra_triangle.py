import numpy as np
import sys
import ConfigParser

import matplotlib
matplotlib.use('Agg')
from matplotlib import rc
from matplotlib import pyplot as plt
import cPickle as pickle

rc('text', usetex=True)
rc('font', family='serif')
rc('font', size=11)
plt.ioff()

def create_spectra_plot(config, colour='#660099'):

  plt.close('all')

  fig = plt.figure(figsize=(8,8))

  root_dir = config.get('pipeline', 'root_dir')
  run_name = config.get('experiment', 'name')
  run_tag = config.get('pipeline', 'tag')
  root_name = root_dir+run_name[:20]+run_tag+'/cl_{1}{0}_'+run_name[:20]+'.txt'

  bin_edges = config.get('spectra', 'spectra_bin_edges').split(',')
  bin_edges = [float(i) for i in bin_edges]
  #bin_edges = [0.0,0.5,1.0,1.6,2.2,3.5,10.0]

  xticks = [1.e1, 1.e2, 1.e3, 1.e4]
  xlabs = ['$10^{1}$','$10^{2}$','$10^{3}$','$10^{4}$']
  yticks = [1.e-7, 1.e-6, 1.e-5, 1.e-4, 1.e-3]
  ylabs = ['$10^{-7}$','$10^{-6}$','$10^{-5}$','$10^{-4}$','$10^{-3}$']

  plotz = len(bin_edges)
  for i in range(plotz-1):
      for j in range(plotz):
          if(j>i) :
              print(i,j)
              ax = plt.subplot2grid((plotz-1, plotz-1), (i,j-1))
              filename = root_name.format(i+1,j)
              ellmin, ellmax, cen, lb, ub = np.loadtxt(filename, unpack=True)
              lb[lb<0] = 1.e-7
              ell = (ellmin + ellmax)/2
              ell[-1] = xticks[-1]
              ax.set_yscale('log')
              ax.set_xscale('log')
              ax.set_xticks(xticks)
              ax.set_xticklabels([])
              ax.set_yticks(yticks)
              ax.set_yticklabels([])
              ax.set_xlim([1.e1,4096])
              ax.set_ylim([1.e-7,1.e-3])

              ax.fill_between(ell, lb, ub, facecolor=colour, linewidth=0.0, alpha=0.5, edgecolor=None, interpolate=True)
              plt.plot(ell,cen, 'k-')
              
              if (i==0) and (j==1):
                ax.set_ylabel('$\ell(\ell+1)C_{\ell}/2\pi$')
                ax.set_yticklabels(ylabs)
              if (i==plotz-2) and j==(plotz-1):
                ax.set_xlabel('$\ell$')
                ax.set_xticklabels(xlabs)
              if (j==plotz-1):
                ax.yaxis.set_label_position('right')
                ax.set_ylabel('$ %.1f < z \leq %.1f$' % (bin_edges[i], bin_edges[i+1]), fontsize='small')
              if (i==0):
                ax.xaxis.set_label_position('top')
                ax.set_xlabel('$ %.1f < z \leq %.1f$' % (bin_edges[j-1], bin_edges[j]), fontsize='small')

  plt.subplots_adjust(hspace=0.0, wspace=0.0)
  plt.savefig(root_dir+run_name+'_triplot.png', bbox_inches='tight', dpi=160)
  #plt.savefig('triplot.png')

if __name__=='__main__':

  param_filename = sys.argv[1]

  config = ConfigParser.ConfigParser()
  config.read(param_filename) 

  create_spectra_plot(config)
