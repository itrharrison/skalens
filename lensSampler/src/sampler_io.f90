module sampler_io
  use healpix_types
  USE fitstools
  USE utilities
  USE pix_tools
  USE paramfile_io, ONLY : paramfile_handle, parse_init, parse_int, &
       parse_string, parse_double, parse_lgt, concatnl
contains

  subroutine get_info(filename,npixtot,nmaps,ordering,nsmax)
    USE healpix_types
    USE fitstools

    IMPLICIT NONE

    INTEGER(I4B) :: nmaps,ordering,nsmax,nside,mlpol,npixtot
    CHARACTER(LEN=filenamelen) :: filename

    npixtot=getsize_fits(filename,nmaps=nmaps,ordering=ordering,nside=nsmax,mlpol=mlpol)
  end subroutine get_info



  subroutine load_map(nside,ordering,ordering_map,npixtot,nmaps,filein_it,signal)
    implicit none
    !load CMB maps
    CHARACTER(len=filenamelen) :: filein,filein_it
    REAL(SP), ALLOCATABLE        :: map(:,:),map2(:,:)  
    !REAL(DP), ALLOCATABLE        :: signal(:,:)
    REAL(DP):: signal(:,:)
    REAL(KIND=SP) :: fmissval  
    INTEGER(I4B) :: iostat,npixtot,ordering,nmaps,nsims,nside
    INTEGER(I4B) :: it,l,ncmb,ordering_map,ipring,ipnest
    CHARACTER(LEN=5) :: output
    ! fmissval=

    ALLOCATE (map(0:npixtot-1,1:nmaps), map2(0:npixtot-1,1:nmaps),stat=iostat)
    if (iostat /= 0) then
       print*, 'Cannot allocate data maps'
       stop
    endif

    !print*,size(map)

    ! print*,'Number of CMB maps',ncmb


    !print*,filein_it

    CALL input_map(filein_it,map,npixtot,nmaps,fmissval=fmissval)

    !print*,map
    if ((ordering_map==1) .and. (ordering==2)) then 
       print*,'Changing ordering of the map from RING to NESTED'
       do ipring=0,npixtot-1 
          !             call nest2ring(nside,ipring,ipnest)
          call ring2nest(nside,ipring,ipnest)

          map2(ipnest,1)=map(ipring,1)
          map2(ipnest,2)=map(ipring,2)
          map2(ipnest,3)=map(ipring,3)
       enddo
       map=map2
    endif

    if ((ordering_map==2) .and. (ordering==1)) then 
       print*,'Changing ordering of the map from NESTED to RING'
       do ipnest=0,npixtot-1 
          !             call ring2nest(nside,ipnest,ipring)
          call nest2ring(nside,ipnest,ipring)
          map2(ipring,1)=map(ipnest,1)
          map2(ipring,2)=map(ipnest,2)
          map2(ipring,3)=map(ipnest,3)
       enddo
       map=map2
    endif
    signal(1:npixtot,1)=map(0:npixtot-1,1)
    signal(1:npixtot,2)=map(0:npixtot-1,2)
    signal(1:npixtot,3)=map(0:npixtot-1,3)

    DEALLOCATE (map,map2)
  end subroutine load_map

!!$SUBROUTINE output_map(filename,mapIO,npixtot,header,ordering,nside,nmaps)
!!$
!!$  USE healpix_types
!!$  USE alm_tools
!!$  USE fitstools!, ONLY : _dbintab
!!$  USE utilities
!!$  USE ran_tools
!!$  USE pix_tools
!!$
!!$  IMPLICIT NONE
!!$
!!$  REAL(SP) :: mapIO(:,:)
!!$  INTEGER, INTENT(IN) :: npixtot
!!$  CHARACTER(LEN=filenamelen) :: filename
!!$  INTEGER(I4B) :: nmaps,nside,ordering
!!$  CHARACTER(LEN=80), DIMENSION(1:120) :: header
!!$  INTEGER(I4B):: nlheader
!!$ 
!!$  print*,'Writing map ',filename
!!$  call output_map(mapIO, header, filename)
!!$  print*,'Done'
!!$
!!$END SUBROUTINE output_map


  subroutine write_catalogue(filename,redshifts,fluxes,sizes,latitudes,longitudes,e1,e2,e_err1,e_err2,obsredshifts,Ncol)
    real(dp),intent(in)::redshifts(:),obsredshifts(:),fluxes(:),sizes(:),latitudes(:),longitudes(:),e1(:),e2(:),e_err1(:),e_err2(:)
    integer status,unit,readwrite,blocksize,hdutype,tfields,nrows,Ncol
    integer varidat,diameter(6), colnum,frow,felem
    real density(6)
    character filename*40,extname*16
    character*16 ttype(Ncol),tform(Ncol),tunit(Ncol)!,name(6)
    real(sp),allocatable::column(:)
!!$    data ttype/'Name','Diameter','Density'/
!!$    data tform/'8A','1J','1E'/
!!$    data tunit/' ','km','g/cm'/
!!$    !data name/'Mars','Jupiter','Saturn','Uranus','Neptune','Pluto'/
!!$    data diameter/6800,143000,121000,47000,45000,6000/
!!$    data density/3.94,1.33,0.69,1.56,2.27,1.0/

    nrows=size(redshifts)
    allocate(column(nrows))

    status=0
    !C     Name of the FITS file to append the ASCII table to:
    !filename='ATESTFILEZ.FITS'

    !C     Get an unused Logical Unit Number to use to open the FITS file
    call ftgiou(unit,status)

    !C     open the FITS file, with write access
    readwrite=1
    !  call ftopen(unit,filename,readwrite,blocksize,status)
    blocksize=1
    call ftinit(unit,filename,blocksize,status)
    !C     move to the last (2nd) HDU in the file
    call ftmahd(unit,1,hdutype,status)

    !C     append/create a new empty HDU onto the end of the file and move to it
    call ftcrhd(unit,status)

    !C     define parameters for the binary table (see the above data statements)
    tfields=Ncol
    !nrows=6
    extname='Catalogue'
    varidat=0
    
    !modify this for the catalgoue
    ttype(1:10) = (/ 'REDSHIFT     ','FLUX         ',&
         &'SIZE         ','LATITUDE     ','LONGITUDE    ',&
         &'ERR1         ','ERR2         ','ELLIPTICITY1 ','ELLIPTICITY2 ','OBSREDSHIFT  '/)

    tform(:)='1E'

!!$    tform(1) = trim(srepeat)//'J'
!!$    tform(2) = trim(srepeatg)//pform
!!$    tform(3) = trim(srepeatg)//'J'
!!$    tform(4) = trim(srepeatg)//pform

    tunit(1:10)= (/ '        ','Jy      ','arcsecs ','deg     ','deg     ',&
         &'        ','        ','        ','        ','        '/)

    !C     write the required header parameters for the binary table
    call ftphbn(unit,nrows,tfields,ttype,tform,tunit,extname,varidat,status)

    !C     write names to the first column, diameters to 2nd col., and density to 3rd
!!$    frow=1
!!$    felem=1
!!$    colnum=1
!!$    call ftpcls(unit,colnum,frow,felem,nrows,name,status)
!!$    colnum=2
!!$    call ftpclj(unit,colnum,frow,felem,nrows,diameter,status)  
!!$    colnum=3

    frow=1
    felem=1

    column=sngl(redshifts)
    call ftpcle(unit,1,frow,felem,nrows,column,status)  
    if (status .gt. 0)call printerror(status)

    column=sngl(fluxes)
    call ftpcle(unit,2,frow,felem,nrows,column,status)  
    if (status .gt. 0)call printerror(status)

    column=sngl(sizes)
    call ftpcle(unit,3,frow,felem,nrows,column,status)  
    if (status .gt. 0)call printerror(status)

    column=sngl(latitudes)
    call ftpcle(unit,4,frow,felem,nrows,column,status)  
    if (status .gt. 0)call printerror(status)

    column=sngl(longitudes)
    call ftpcle(unit,5,frow,felem,nrows,column,status)  
    if (status .gt. 0)call printerror(status)

    column=sngl(e_err1)
    call ftpcle(unit,6,frow,felem,nrows,column,status)  
    if (status .gt. 0)call printerror(status)

    column=sngl(e_err2)
    call ftpcle(unit,7,frow,felem,nrows,column,status)  
    if (status .gt. 0)call printerror(status)

    column=sngl(e1)
    call ftpcle(unit,8,frow,felem,nrows,column,status)  
    if (status .gt. 0)call printerror(status)

    column=sngl(e2)
    call ftpcle(unit,9,frow,felem,nrows,column,status)  
    if (status .gt. 0)call printerror(status)

    column=sngl(obsredshifts)
    call ftpcle(unit,10,frow,felem,nrows,column,status)  
    if (status .gt. 0)call printerror(status)
    !C     close the FITS file and free the unit number
    call ftclos(unit, status)
    call ftfiou(unit, status)

    !C     check for any error, and if so print out error messages
    if (status .gt. 0)call printerror(status)
    deallocate(column)
  end subroutine write_catalogue



 function rows_number(filename,iunit)
    integer:: reason,iunit,nrows,rows_number
    character (LEN=200)::filename
    real(DP)::col

close(iunit)
    open (UNIT=iunit,file=filename,status='unknown',form="formatted")
    print*,filename
    nrows=1
    DO
       READ(iunit,*,IOSTAT=Reason)  col
       IF (Reason > 0)  THEN 
          print*,'Error in reading file'
          print*,filename
          stop
       ELSE IF (Reason < 0) THEN
          exit
       ELSE
          nrows=nrows+1
       END IF
    END DO
   rows_number=nrows-1
    CLOSE (unit=iunit)
print*,'Nrows=',rows_number
    return
  end function rows_number
  
  subroutine read_columns(filename,iunit,nrows,Ncolumns,data)
    character (LEN=200)::filename
    integer:: reason,iunit,nrows,Ncolumns,i
    real(dp):: col1,col2,col3,col4,col5,col6,col7,col8,col9,col10
    real(dp)::data(:,:)

    data(:,:)=0.
    if (Ncolumns >10) then 
       print*,'Number of columns not supported!'
       stop
    endif
close(iunit)
    open (UNIT=iunit,file=filename,status='unknown',form="formatted")

    SELECT CASE(Ncolumns)

    CASE(1)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1
          data(i,1)=col1
       ENDDO
    CASE(2)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2
          data(i,1)=col1
          data(i,2)=col2
       ENDDO
    CASE(3)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2,col3
          data(i,1)=col1
          data(i,2)=col2
          data(i,3)=col3
       ENDDO

    CASE(4)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2,col3,col4
          data(i,1)=col1
          data(i,2)=col2
          data(i,3)=col3
          data(i,4)=col4
       ENDDO
    CASE(5)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2,col3,col4,col5
          data(i,1)=col1
          data(i,2)=col2
          data(i,3)=col3
          data(i,4)=col4
          data(i,5)=col5
       ENDDO
    CASE(6)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2,col3,col4,col5,col6
          data(i,1)=col1
          data(i,2)=col2
          data(i,3)=col3
          data(i,4)=col4
          data(i,5)=col5
          data(i,6)=col6
       ENDDO

    CASE(7)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2,col3,col4,col5,col7
          data(i,1)=col1
          data(i,2)=col2
          data(i,3)=col3
          data(i,4)=col4
          data(i,5)=col5
          data(i,6)=col6
          data(i,7)=col7
       ENDDO
    CASE(8)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2,col3,col4,col5,col7,col8
          data(i,1)=col1
          data(i,2)=col2
          data(i,3)=col3
          data(i,4)=col4
          data(i,5)=col5
          data(i,6)=col6
          data(i,7)=col7
          data(i,8)=col8
       ENDDO

    CASE(9)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2,col3,col4,col5,col7,col8,col9
          data(i,1)=col1
          data(i,2)=col2
          data(i,3)=col3
          data(i,4)=col4
          data(i,5)=col5
          data(i,6)=col6
          data(i,7)=col7
          data(i,8)=col8
          data(i,9)=col9
       ENDDO

    CASE(10)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2,col3,col4,col5,col7,col8,col9,col10
          data(i,1)=col1
          data(i,2)=col2
          data(i,3)=col3
          data(i,4)=col4
          data(i,5)=col5
          data(i,6)=col6
          data(i,7)=col7
          data(i,8)=col8
          data(i,9)=col9
          data(i,10)=col10
       ENDDO
    END SELECT
  CLOSE (unit=iunit)
 end subroutine read_columns


 subroutine write_columns(filename,iunit,nrows,Ncolumns,data)
    character (LEN=200)::filename
    integer:: reason,iunit,nrows,Ncolumns,i
    real(dp):: col1,col2,col3,col4,col5,col6,col7,col8,col9,col10
    real(dp)::data(:,:)

   
    if (Ncolumns >10) then 
       print*,'Number of columns not supported!'
       stop
    endif

    close(iunit)
    open (UNIT=iunit,file=filename,status='unknown',form="formatted")

    SELECT CASE(Ncolumns)

    CASE(1)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason)  data(i,1)         
       ENDDO
    CASE(2)
       DO i=1,nrows
           write(iunit,*,IOSTAT=Reason)   data(i,1), data(i,2)
        ENDDO
    CASE(3)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason)  data(i,1), data(i,2),data(i,3)
       ENDDO

    CASE(4)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason)  data(i,1), data(i,2),data(i,3),data(i,4)
          
       ENDDO
    CASE(5)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason) data(i,1), data(i,2),data(i,3),data(i,4)&
               &,data(i,5)
          
       ENDDO
    CASE(6)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason) data(i,1), data(i,2),data(i,3),data(i,4)&
               &,data(i,5),data(i,6)
          
       ENDDO

    CASE(7)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason)  data(i,1), data(i,2),data(i,3),data(i,4)&
               &,data(i,5),data(i,6),data(i,7)
          
       ENDDO
    CASE(8)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason)  data(i,1), data(i,2),data(i,3),data(i,4)&
               &,data(i,5),data(i,6),data(i,7),data(i,8)
          
       ENDDO

    CASE(9)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason)  data(i,1), data(i,2),data(i,3),data(i,4)&
               &,data(i,5),data(i,6),data(i,7),data(i,8),data(i,9)
          
       ENDDO

    CASE(10)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason)   data(i,1), data(i,2),data(i,3),data(i,4)&
               &,data(i,5),data(i,6),data(i,7),data(i,8),data(i,9),data(i,10)
          
       ENDDO
    END SELECT
  CLOSE (unit=iunit)
 end subroutine write_columns


 



  end module sampler_io
