program sampler
  use udgrade_nr
  use healpix_types
  USE fitstools
  USE utilities
  USE pix_tools
  USE random_tools
  use sampler_io

  USE paramfile_io, ONLY : paramfile_handle, parse_init, parse_int, &
       parse_string, parse_double, parse_lgt, concatnl
  USE extension, ONLY : getEnvironment, getArgument, nArguments
  implicit none
  character(LEN=filenamelen)::paramfile,description,dummy
  character(LEN=filenamelen)::skads_filename,ellipticities_filename,filename
  character(LEN=filenamelen),allocatable::shearfiles(:),shearfiles_out(:),catfiles(:)
  real(dp),allocatable::data(:,:),minz(:),maxz(:),avg(:),eigenvectors(:,:)
  real(dp),allocatable::sample(:,:),sample_indep(:,:),sample_mean(:,:),sample1d(:),histo(:,:)
  real(dp),allocatable::x(:),px(:),samplex(:),samples(:,:),samples_corr(:,:),sampletot(:,:),samples_store(:,:),samples_tmp(:,:)
  real(dp),allocatable::fluxes(:),sizes(:),egal_err(:,:),redshifts(:),obsredshifts(:),egal(:,:)
  real(dp),allocatable::fluxvector(:),ellvector(:),latitudes(:),longitudes(:)
  real(dp),allocatable::dum(:),histo_arr(:,:)
  real(dp)::mn,mx,binsize,sigma_noise,size_cut,sn_cut,q,skads_size,skads_norm
  real(dp)::survey_area,patch_size,err,skads_area,sig,specz_fraction,photoz_sigma1,photoz_sigma2,z_high
  real(dp)::beta,e,frequency,theta,phi,theta_max,theta_min,phi_max,phi_min,rnd,mt
  integer,allocatable::mapping_e(:),mapping_f(:)
  integer,parameter::Nsub=100000
  integer::nrows,ncolumns,Nbinz,ngal,Nvar,Ndep,Nsample,ncount,Ns1,Ns2,jj,pixel,pixel_in
  integer::subsample(Nsub),size_alloc
  integer::i,ii,j,iostat,l,nbin,l2,p(1),count_f,row
  integer::ini,fini,Ngen,nloop,k
  integer::iseed
  TYPE(paramfile_handle) :: handle
  CHARACTER(LEN=5) :: output,output2,tag
  CHARACTER(LEN=10) ::output3
  CHARACTER(LEN=80), DIMENSION(1:120) :: header
  CHARACTER(LEN=80)::line
  real(dp),parameter:: fullsky_area=41253._dp
  integer(4) :: ic4, crate4, cmax4,ni,sum_plus
  INTEGER, DIMENSION(8,2) :: values_time
  REAL(SP) :: clock_time
  CHARACTER(LEN=*), PARAMETER :: code ="LensSampler"
  !double precision ispline
  save iseed

  !getting seed from the clock
  call system_clock(count=ic4, count_rate=crate4, count_max=cmax4)
  iseed=ic4

  call date_and_time(values = values_time(:,1))

  !1)input file:
  !  -bins in redshift
  !  -number of objects


  if (nArguments() == 0) then
     paramfile=''
  else if (nArguments() == 1) then
     call getArgument(1,paramfile)
  else 
     print '("Usage: QML: [parameter file name]")'
     stop 1
  endif

  handle = parse_init(paramfile)
  description = concatnl( &
       & " Enter number of redshift slices ")
  Nbinz = parse_int(handle, 'Nbinz', default=5, vmin=1, descr=description)

  allocate(minz(Nbinz),maxz(Nbinz),shearfiles(Nbinz),shearfiles_out(Nbinz),catfiles(Nbinz),stat=iostat)
  if (iostat /=0) then
     print*,'Error allocating filenames'
     stop
  endif

  do i=1,Nbinz
     write(output,"(i3)")i
     output=ADJUSTL(output)
     l=LEN_TRIM(output)
     dummy = 'minz'//output(:l)
     description = concatnl( &
          & " Enter minimum redshift of the # "//output(:l)// "slice:")
     minz(i) = parse_double(handle, dummy, default=0.d0, descr=description)
     dummy = 'maxz'//output(:l)
     description = concatnl( &
          & " Enter maximum redshift of the # "//output(:l)// "slice:")
     maxz(i) = parse_double(handle, dummy, default=0.d0, descr=description)

     dummy = 'catout'//output(:l)
     description = concatnl( &
          & " Enter the name of the output catalogue")
     catfiles(i)=parse_string(handle,dummy, default='', descr=description)
  enddo

  description = concatnl( &
       & " Enter the name of the input file with skads simulations")
  skads_filename=parse_string(handle,'skads', default='', descr=description)

  description = concatnl( &
       & " Enter area of skads query:")
  skads_area = parse_double(handle, 'skads_area', default=400.d0, descr=description)
  
  description = concatnl( &
       & " Enter the name of the input file with ellipticity vs SNR")
  ellipticities_filename=parse_string(handle,'ellipticities', default='', descr=description)

  description = concatnl( &
       & "Enter noise level [muJy]:")
  sigma_noise = parse_double(handle, 'sigma_noise', default=0.d0, descr=description)
  description = concatnl( &
       & "Enter frequency of observation [GHz]:")
  frequency = parse_double(handle, 'frequency', default=1.4d0, descr=description)
  description = concatnl( &
       & " Enter size threshold (arcseconds):")
  size_cut = parse_double(handle, 'size_cut', default=0.d0, descr=description)

  description = concatnl( &
       & " Enter S/N threshold:")
  sn_cut = parse_double(handle, 'sn_cut', default=0.d0, descr=description)


  description = concatnl( &
       & " Enter survey area:")
  survey_area = parse_double(handle, 'survey_area', default=fullsky_area, descr=description)

  description = concatnl( &
       & " Enter survey area:")
  specz_fraction = parse_double(handle, 'specz_fraction', default=0.15d0, descr=description)

  description = concatnl( &
       & " Enter survey area:")
  photoz_sigma1 = parse_double(handle, 'photoz_sigma1', default=0.04d0, descr=description)

  description = concatnl( &
       & " Enter survey area:")
  photoz_sigma2 = parse_double(handle, 'photoz_sigma2', default=0.3d0, descr=description)

  description = concatnl( &
       & " Enter survey area:")
  z_high = parse_double(handle, 'z_high', default=2.d0, descr=description)


  if(survey_area < fullsky_area) then
     
   ! define latitude and longitude range for sky patch
     !this only works for limited survey area 

patch_size=sqrt(survey_area)*pi/180.
!long_min=360.-patch_size/2.
!long_max=patch_size/2.
!lat_max=-patch_size/2.
!long_max=patch_size/2.
theta_max=pi/2.+patch_size/2.
theta_min=pi/2.-patch_size/2.
phi_min=pi-patch_size/2.
phi_max=pi+patch_size/2.
endif

! fudge factors for SKADS simulations (to agree better with data)
  description = concatnl( &
       & " Enter skads size calibration factor:")
  skads_size = parse_double(handle, 'skads_size', default=1.d0, descr=description)

  description = concatnl( &
       & " Enter skads numbercounts calibration factor:")
  skads_norm = parse_double(handle, 'skads_counts', default=1.d0, descr=description)

  !2)INPUTS:
  !#1 load relevant columns of skads simulation (with bad data already excluded)
  !#2 load file with gamma_int+gamma_err as function of S/N
  !#3 load shear maps (bin in redshift should correspond!... write to header?)


  !reading ellipticities vs SNR 
  Ncolumns=3 !log10(sn),ellipticities,N
  nrows=rows_number(ellipticities_filename,1)
  allocate(data(nrows,ncolumns),dum(nrows),mapping_e(nrows),mapping_f(nrows),stat=iostat)
  if (iostat /=0) then
     print*,'Error allocating skads data'
     stop
  endif

  call read_columns(ellipticities_filename,2,nrows,Ncolumns,data)

  !extract values of sn vector
  call getsample(data(:,1),dum,mapping_f,Ns1)
  allocate(fluxvector(Ns1))
  fluxvector=dum(1:Ns1)
  print*,'sn values in file',Ns1,minval(fluxvector),maxval(fluxvector)
  !extract values of ellipticity vector
  call getsample(data(:,2),dum,mapping_e,Ns2)
  allocate(ellvector(Ns2))
  ellvector=dum(1:Ns2)
  deallocate(dum)
  print*,'ellipticity values in file',Ns2,minval(ellvector),maxval(ellvector)
  allocate(histo_arr(Ns1,Ns2))

  do i=1,nrows
     ii=mapping_f(i)
     jj=mapping_e(i)
     histo_arr(ii,jj)=data(i,3)!number of samples
  enddo

  deallocate(data)

  !reading in skads simulations
  Ncolumns=3 !flux,size,redshift
  Nvar=Ncolumns !flux,size,redshift
  Ndep=Ncolumns-1 !dependent variables (flux,size)
  nrows=rows_number(skads_filename,1)
  allocate(data(nrows,ncolumns),stat=iostat)
  if (iostat /=0) then
     print*,'Error allocating skads data'
     stop
  endif


  call read_columns(skads_filename,2,nrows,Ncolumns,data)
  allocate(eigenvectors(Ndep,Ndep),avg(Ndep))


  !3)split sample for redshift bins

  !FOR EACH REDSHIFT BIN:
  !4)PCA -> decorrelate variables

  !FOR EACH VARIABLE:
  !5)call randomgen -> SAMPLE

  !6) put variables together
  !7) inverse PCA -> sample of fluxes



  do i=1,Nbinz
     !    do i=2,Nbinz
     print*,'*** Redshift slice',i
     !read shear map from file  

     !count number of galaxies 
     ngal=0
     do ii=1,nrows
        if ((data(ii,1) >= minz(i)) .and.  (data(ii,1)< maxz(i))) ngal=ngal+1
     enddo
     print*,'Slice',i
     print*,'Number of objects in slice',ngal

     allocate(sample(ngal,Nvar),sample_mean(ngal,Ndep),sample_indep(Nvar,ngal),sample1d(ngal),stat=iostat)
     if (iostat /=0) then
        print*,'Error allocating skads samples'
        stop
     endif

     !flux,dimension 
     ngal=0
     do ii=1,nrows
        if ((data(ii,1) >= minz(i)) .and.  (data(ii,1)< maxz(i))) then
           ngal=ngal+1
           sample(ngal,1)=data(ii,2)
           sample(ngal,2)=data(ii,3)
           sample(ngal,3)=data(ii,1)
!!$           do j=1,Nvar
!!$              sample(ngal,j)=data(ii,j)
!!$           enddo
        endif
     enddo

     sample(:,2)=sample(:,2)/skads_size ! recalibration of skads sizes
     sample(:,2)=dlog10(sample(:,2)) !log of dimension
!!$     dummy=trim('samples_ini.txt')
!!$     call write_columns(dummy,10,ngal,Nvar,sample)
     !4)PCA -> decorrelate variables

     !subtract mean
     do j=1,Ndep
        avg(j)=sum(sample(:,j))/dble(ngal)
        sample_mean(:,j)=sample(:,j)-avg(j) !mean-subtracted variable
     enddo

     !performs pca
     print*,'Performing PCA...'

     if (ngal > Nsub) then 
        !extract a subsample of Nsub objects to perform PCA
        do j=1,Nsub
           rnd=int(ran_mwc(iseed)*ngal)
           subsample(j)=rnd
        enddo
        call pca(sample_mean(subsample,:),Ndep,Nsub,eigenvectors)
     else 
        call pca(sample_mean,Ndep,ngal,eigenvectors)        
     endif


     !sample_mean(ngal,Ndep),sample_indep(Nvar,ngal)
     print*,'Eigenvectors:'
     print*,eigenvectors(1,:)
     print*,eigenvectors(2,:)

     print*,'Computing independent variables'
     !sample_indep(1:Ndep,:)=matmul(eigenvectors,transpose(sample_mean))
     !done manually to prevent crashes for long vectors
     do j=1,ngal 
        do jj=1,Ndep
           mt=0.
           do ii=1,Ndep
              mt=mt+eigenvectors(ii,jj)*sample_mean(j,ii)
           enddo
           sample_indep(jj,j)=mt
        enddo
     enddo
     print*,'done'

     !     deallocate(sample_mean_tr)

     do j=Ndep+1,Nvar !add indep variables
        sample_indep(j,:)=sample(:,j)
     enddo

! fix for bug when survey area < skads_area 
     if (survey_area/skads_area*skads_norm > 1.) then  
        Ngen=int(ngal/100)
        nloop=int(survey_area/skads_area*skads_norm)*100
        Nsample=nloop*Ngen
     else 
        Ngen=int(ngal*survey_area/skads_area*skads_norm/100.)
        nloop=100
        Nsample=nloop*Ngen
     endif

     print*,'generating number of galaxies',nsample

     allocate(samplex(Ngen),samples(Ngen,Nvar),samples_corr(Ngen,Nvar),samples_store(Ngen*2,Nvar),stat=iostat)
     if (iostat /=0) then
        print*,'Error allocating my samples'
        stop
     endif
     size_alloc=Ngen*2
     print*,'starting loop',nloop
     print*,'Ngen=',Ngen

     samples_corr(:,:)=0.

     ncount=0
     do k=1,nloop
        do j=1,Nvar
           sample1d(:)=sample_indep(j,:)
           mn=minval(sample1d)
           mx=maxval(sample1d)
           !print*,'call histogram'
           nbin=-1 
           call histogram(sample1d,nbin,binsize,histo)!find nbin and binsize
           !print*,'number of bins',nbin

           allocate(histo(nbin,2),x(nbin),px(nbin),stat=iostat)
           if (iostat /=0) then
              print*,'Error histogram'
              stop
           endif
           !print*,'call histogram'
           histo(:,1)=0.
           call histogram(sample1d,nbin,binsize,histo)!compute histogram 

           !5)call randomgen -> SAMPLE
           x=histo(:,1)
           px=histo(:,2)

!!$        ini=1
!!$        fini=Ngen
!!$        print*,'starting loop',nloop
!!$        do k=1,nloop
           call randomgen(x,px,mn,mx,Ngen,samplex)
           call adjust(x,px,Ngen,samplex)
           samples(:,j)=samplex(:)
           !           ini=ini+Ngen
           !           fini=fini+Ngen
           deallocate(histo,x,px)
        enddo !  do j=1,Nvar

        !        print*,'inverse PCA'
        do j=1,Ngen
           do jj=1,Ndep
              mt=0.
              do ii=1,Ndep
                 mt=mt+eigenvectors(ii,jj)*samples(j,ii)              
              enddo
              samples_corr(j,jj)=mt
           enddo
        enddo

        do j=1,Ndep
           samples_corr(:,j)=samples_corr(:,j)+avg(j) !non mean-subtracted
        enddo
        do j=Ndep+1,Nvar
           samples_corr(:,j)=samples(:,j)
        enddo

        !correct redshifts (field N 3)
        do j=1,Ngen
           if (samples_corr(j,3) < minz(i)) samples_corr(j,3)=minz(i)
           if (samples_corr(j,3) > maxz(i)) samples_corr(j,3)=maxz(i)
        enddo

!!$     do j=1,Nvar
!!$           print*,minval(samples_corr(:,j)),maxval(samples_corr(:,j))
!!$        enddo
!!$        stop


        samples(:,2)=10.**(samples_corr(:,2))! dimension (arcseconds)
        samples(:,1)=(10.**(samples_corr(:,1)))*(frequency/1.4)**(-0.7)/sigma_noise*1.e6 !sn !check units!!! Jy at frequency 
        samples(:,3)=samples_corr(:,3)!redshift


        !ncount=0
        !print*,'starting',ncount
        do j=1,Ngen
           if ((samples(j,1) >= sn_cut) .and. (samples(j,2) >= size_cut)) then
              ncount=ncount+1
              samples_store(ncount,:)=samples(j,:)
           endif
        enddo
        !print*,'ending',ncount


        if (ncount+Ngen > size_alloc) then
           !expand size of sample_store
           !   print*,'Expand memory'
           allocate(samples_tmp(size_alloc,Nvar))
           samples_tmp=samples_store
           deallocate(samples_store)        
           allocate(samples_store(2*size_alloc,Nvar))
           samples_store(:,:)=0.
           samples_store(1:size_alloc,:)=samples_tmp
           size_alloc=size_alloc*2
           deallocate(samples_tmp)      
        endif

        !store observed galaxies in samples_corr
!!$     deallocate(samples_corr,samplex,stat=iostat)
!!$     if (iostat /=0) then
!!$        print*,'Deallocation Error'
!!$        stop
!!$     endif

     enddo ! k loop
     print*,'loop done'
     !print*,'inverse PCA'

     !inverse PCA -> sample of fluxes
     !samples_corr=
     deallocate(samples_corr,samples,samplex,sample,sample_mean,sample1d,sample_indep,stat=iostat)
     if (iostat /=0) then
        print*,'Deallocation Error'
        stop
     endif

  
     print*,'Number of galaxies above size and S/N threshold=',ncount

        allocate(fluxes(ncount),redshifts(ncount),sizes(ncount),&
          &egal_err(ncount,2),egal(ncount,2),sampletot(ncount,2),&
          &latitudes(ncount),longitudes(ncount),obsredshifts(ncount),stat=iostat)
     if (iostat /=0) then
        print*,'Error allocating arrays'
        stop
     endif



!!$     ncount=0
!!$     do j=1,Nsample
!!$        if ((samples(j,1) >= sn_cut) .and. (samples(j,2) >= size_cut)) then
!!$           ncount=ncount+1
!!$           fluxes(ncount)=log10(samples(j,1))  !to be compared with log10(SN) from file
!!$           sizes(ncount)=samples(j,2)
!!$           redshifts(ncount)=samples(j,3)
!!$        endif
!!$     enddo
     do j=1,ncount
        fluxes(j)=log10(samples_store(j,1))  !to be compared with log10(SN) from file
        sizes(j)=samples_store(j,2)
        redshifts(j)=samples_store(j,3)


        !photoz
123 continue
        obsredshifts(j)=redshifts(j)
        
        if (redshifts(j)>z_high) then
           sig=photoz_sigma2*(1.+redshifts(j))
           rnd=randgauss_boxmuller(iseed)
           obsredshifts(j)=obsredshifts(j)+rnd*sig
        else
           rnd=ran_mwc(iseed)
           sig=photoz_sigma1*(1.+redshifts(j))
           if (rnd > specz_fraction) then
              rnd=randgauss_boxmuller(iseed)
              obsredshifts(j)=obsredshifts(j)+rnd*sig
           endif
        endif
        if (obsredshifts(j)<0.) goto 123


        !photoz done
        
     enddo

     print*,'Parameter ranges'
     print*,'size',minval(sizes),maxval(sizes)
     print*,'sn',minval(10.**fluxes),maxval(10.**fluxes)
     print*,'z',minval(redshifts),maxval(redshifts)
     print*,'obsz',minval(obsredshifts),maxval(obsredshifts)
     ! data2: ellipticities,log10(sn),N
     !generate e_err using the distribution depending on S/N

     deallocate(samples_store)

     mn=minval(fluxes)  !min q of the sample
     mx=maxval(fluxes)  !max q of the sample
  
     print*,'Starting ellipticities'

     
     do ii=1,Ns1
        if ((fluxvector(ii)<mn) .or.  (fluxvector(ii)>mx)) go to 100
        !print*,ii,'of 200'
        count_f=0
        do j=1,ncount
           q=fluxes(j)
           p=minloc(abs(q-fluxvector))
           if (p(1)==ii) count_f=count_f+1
        enddo
       Ngen=count_f
        if (Ngen /=0) then
           allocate(samplex(Ngen),stat=iostat)
           if (iostat /=0) then
              print*,'Error allocating samplex'
              stop
           endif
           
           call randomgen(ellvector,histo_arr(ii,:),mn,mx,Ngen,samplex)


           !check samplex values
           do j=1,Ngen
              if (samplex(j) > 1.) samplex(j)=1.
              if (samplex(j) < 0.) samplex(j)=0.
           enddo
           err=rms(sngl(samplex)) ! error on ellipticity

           !place samplex values in the right place in the catalogue
           row=1
           do j=1,ncount
              q=fluxes(j)
              p=minloc(abs(q-fluxvector))
              if (p(1)==ii) then 
                 if (row >Ngen) then
                    print*,'PROBLEM!!!',j,row,Ngen
                    stop
                 endif
                 sampletot(j,1)=samplex(row)
                 sampletot(j,2)=err ! error on ellipticity
                 row=row+1
              endif
           enddo
     
           deallocate(samplex,stat=iostat)
           if (iostat /=0) then
              print*,'Error deallocating samplex'
              stop
           endif
        endif

100     continue
     enddo
     print*,'Ellipticities done'


     !generating ellipticities and sky position

     egal_err(:,:)=1.

     do j=1,ncount
        beta=ran_mwc(iseed)*2.*PI
        e=sampletot(j,1)
        if (e>1.) then 
           print*,'e>1',e
           e=1.
        endif
        if (e<0.) then 
           print*,'e<0',e
           e=0.
        endif
        egal(j,1)=e*cos(2.*beta)
        egal(j,2)=e*sin(2.*beta)
        egal_err(j,1)=sampletot(j,2)
        egal_err(j,2)=sampletot(j,2)
        !egal_err as width of the distribution (for inverse variance weighting)

        if (survey_area <fullsky_area) then
           theta=ran_mwc(iseed)*(theta_max-theta_min)+theta_min
           phi=ran_mwc(iseed)*(phi_max-phi_min)+phi_min
           phi=phi-pi
        else 
           theta=ran_mwc(iseed)*pi   !sky coordinates in healpix convention
           phi=ran_mwc(iseed)*2.*pi
        endif
        latitudes(j)=(theta-pi/2.)*180./pi  !sky coordinates 
        longitudes(j)=phi*180./pi
     enddo

     print*,'loop done'
     !map normalizations

     
     filename=trim(catfiles(i))

     fluxes=(10.**(fluxes))*(sigma_noise/1.e6) ! fluxes in Jy

!remove old file
open(unit=1234, iostat=iostat, file=filename, status='old')
        if (iostat.eq.0) close(1234, status='delete')

     print*,'writing catalogue ',filename
     call write_catalogue(filename,redshifts,fluxes,sizes,latitudes,&
          &longitudes,egal(:,1),egal(:,2),egal_err(:,1),egal_err(:,2),obsredshifts,10)
     print*,'done'


     deallocate(fluxes,sizes,egal_err,sampletot,latitudes,longitudes,redshifts,obsredshifts,egal,stat=iostat)
     if (iostat /=0) then
        print*,'Deallocation error'
        stop
     endif


     print*,'*** End redshift slice',i

  enddo ! loop on redshift bin


  !free all memory
  deallocate(minz,maxz,shearfiles,shearfiles_out,catfiles,data,&
       &mapping_e,mapping_f,fluxvector,ellvector,histo_arr,eigenvectors,stat=iostat)
  if (iostat /=0) then
     print*,'Deallocation error'
     stop
  endif

  call date_and_time(values = values_time(:,2))

  values_time(:,1) = values_time(:,2) - values_time(:,1)
  clock_time =  (  (values_time(3,1)*24 &
       &           + values_time(5,1))*60. &
       &           + values_time(6,1))*60. &
       &           + values_time(7,1) &
       &           + values_time(8,1)/1000.
  PRINT*,"Total clock time [m]:",clock_time/60.
  PRINT*,"               "//code//" > Normal completion"
end program sampler
