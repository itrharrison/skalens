

module interpolations
use healpix_types
contains

subroutine quadratic(x0,x1,x2,y0,y1,y2,A,B,C)
implicit none
!Computes quadratic coefficients 
real(dp),intent(IN)::x0,x1,x2,y0,y1,y2
real(dp),intent(OUT)::A,B,C
real(dp):: a0,a1,a2

a0=y0/(x0-x1)/(x0-x2)
a1=y1/(x1-x0)/(x1-x2)
a2=y2/(x2-x0)/(x2-x1)

A=a0+a1+a2
B=-(a0*(x1+x2)+a1*(x0+x2)+a2*(x0+x1))
C=a0*x1*x2+a1*x0*x2+a2*x0*x1

end subroutine quadratic

subroutine quadratic_iterpolation(x,y,xnew,ynew,N,N2)
  implicit none
  !interpolate y quadratically y->ynew 
  real(dp),intent(in)::x(:),y(:),xnew(:)
  real(dp),intent(out)::ynew(:)
  integer::N,N2,i,ii,p(1),ny
  real(dp)::dif(N),dist(1)
  real(dp)::x0,x1,x2,y0,y1,y2,A,B,C,tiny,p0,p1,p2,yold

  tiny=1.e-3

  do i=1,N2 
     dif=abs(xnew(i)-x)
     p=minloc(dif)
     dist=minval(dif)
     if (dist(1)<tiny) then
        ynew(i)=y(p(1))
        !no interpolatin needed
     else 
        !quadratic interpolation. find closest three points
        if (p(1)==1) then 
           ii=p(1)+1
        else if (p(1)==N2) then
           ii=p(1)-1
        else
           ii=p(1)
        endif
        x0=x(ii-1) 
        x1=x(ii) 
        x2=x(ii+1) 
        y0=y(ii-1) 
        y1=y(ii) 
        y2=y(ii+1) 

        call quadratic(x0,x1,x2,y0,y1,y2,A,B,C)

        ynew(i)=A*xnew(i)**2.+B*xnew(i)+C

     endif
     enddo

end subroutine quadratic_iterpolation


   subroutine spline (x, y, b, c, d, n)
!======================================================================
!  Calculate the coefficients b(i), c(i), and d(i), i=1,2,...,n
!  for cubic spline interpolation
!  s(x) = y(i) + b(i)*(x-x(i)) + c(i)*(x-x(i))**2 + d(i)*(x-x(i))**3
!  for  x(i) <= x <= x(i+1)
!  Alex G: January 2010
!----------------------------------------------------------------------
!  input..
!  x = the arrays of data abscissas (in strictly increasing order)
!  y = the arrays of data ordinates
!  n = size of the arrays xi() and yi() (n>=2)
!  output..
!  b, c, d  = arrays of spline coefficients
!  comments ...
!  spline.f90 program is based on fortran version of program spline.f
!  the accompanying function fspline can be used for interpolation
!======================================================================
implicit none
integer n
double precision x(n), y(n), b(n), c(n), d(n)
integer i, j, gap
double precision h

gap = n-1
! check input
if ( n < 2 ) return
if ( n < 3 ) then
  b(1) = (y(2)-y(1))/(x(2)-x(1))   ! linear interpolation
  c(1) = 0.
  d(1) = 0.
  b(2) = b(1)
  c(2) = 0.
  d(2) = 0.
  return
end if
!
! step 1: preparation
!
d(1) = x(2) - x(1)
c(2) = (y(2) - y(1))/d(1)
do i = 2, gap
  d(i) = x(i+1) - x(i)
  b(i) = 2.0*(d(i-1) + d(i))
  c(i+1) = (y(i+1) - y(i))/d(i)
  c(i) = c(i+1) - c(i)
end do
!
! step 2: end conditions 
!
b(1) = -d(1)
b(n) = -d(n-1)
c(1) = 0.0
c(n) = 0.0
if(n /= 3) then
  c(1) = c(3)/(x(4)-x(2)) - c(2)/(x(3)-x(1))
  c(n) = c(n-1)/(x(n)-x(n-2)) - c(n-2)/(x(n-1)-x(n-3))
  c(1) = c(1)*d(1)**2/(x(4)-x(1))
  c(n) = -c(n)*d(n-1)**2/(x(n)-x(n-3))
end if
!
! step 3: forward elimination 
!
do i = 2, n
  h = d(i-1)/b(i-1)
  b(i) = b(i) - h*d(i-1)
  c(i) = c(i) - h*c(i-1)
end do
!
! step 4: back substitution
!
c(n) = c(n)/b(n)
do j = 1, gap
  i = n-j
  c(i) = (c(i) - d(i)*c(i+1))/b(i)
end do
!
! step 5: compute spline coefficients
!
b(n) = (y(n) - y(gap))/d(gap) + d(gap)*(c(gap) + 2.0*c(n))
do i = 1, gap
  b(i) = (y(i+1) - y(i))/d(i) - d(i)*(c(i+1) + 2.0*c(i))
  d(i) = (c(i+1) - c(i))/d(i)
  c(i) = 3.*c(i)
end do
c(n) = 3.0*c(n)
d(n) = d(n-1)
end subroutine spline

  function ispline(u, x, y, b, c, d, n)
!======================================================================
! function ispline evaluates the cubic spline interpolation at point z
! ispline = y(i)+b(i)*(u-x(i))+c(i)*(u-x(i))**2+d(i)*(u-x(i))**3
! where  x(i) <= u <= x(i+1)
!----------------------------------------------------------------------
! input..
! u       = the abscissa at which the spline is to be evaluated
! x, y    = the arrays of given data points
! b, c, d = arrays of spline coefficients computed by spline
! n       = the number of data points
! output:
! ispline = interpolated value at point u
!=======================================================================
implicit none
double precision ispline
integer n
double precision  u, x(n), y(n), b(n), c(n), d(n)
integer i, j, k
double precision dx

! if u is ouside the x() interval take a boundary value (left or right)
if(u <= x(1)) then
  ispline = y(1)
  return
end if
if(u >= x(n)) then
  ispline = y(n)
  return
end if

!*
!  binary search for for i, such that x(i) <= u <= x(i+1)
!*
i = 1
j = n+1
do while (j > i+1)
  k = (i+j)/2
  if(u < x(k)) then
    j=k
    else
    i=k
   end if
end do
!*
!  evaluate spline interpolation
!*
dx = u - x(i)
ispline = y(i) + dx*(b(i) + dx*(c(i) + dx*d(i)))
end function ispline



end module interpolations



module random_tools
use healpix_types
use interpolations
contains

  !=======================================================================
  function randgauss_boxmuller(iseed)
    !=======================================================================
    !     Box-Muller method for converting uniform into Gaussian deviates 
    !=======================================================================
    integer(I4B), intent(inout) :: iseed
    real(SP) :: randgauss_boxmuller
    logical(LGT), save :: empty=.true.
    real(SP) :: fac,rsq,v1,v2
    real(SP),save :: gset

    if (empty .or. iseed < 0) then ! bug correction, EH, March 13, 2003
1      v1=2.*ran_mwc(iseed)-1.
       v2=2.*ran_mwc(iseed)-1.
!!$1      v1=2.*ran1(iseed)-1.
!!$       v2=2.*ran1(iseed)-1.
       rsq=v1**2+v2**2
       if(rsq.ge.1.or.rsq.eq.0.) goto 1
       fac=sqrt(-2.*log(rsq)/rsq)
       gset=v1*fac
       randgauss_boxmuller=v2*fac
       empty=.false.
    else
       randgauss_boxmuller=gset
       empty=.true.
    endif
    return
  end function randgauss_boxmuller


 !=======================================================================
  function rms(vector)
   
    real(SP),intent(in):: vector(:)
    real(SP)::rms,avg
    integer(I4B)::n,i
  
    n=size(vector)

    avg=0.
    do i=1,N
       avg=avg+vector(i)
    enddo
    avg=avg/real(N)

    rms=0.
    do i=1,N
       rms=rms+(vector(i)-avg)**2
    enddo
    rms=rms/real(N)
    rms=sqrt(rms)
    return
  end function rms

  !=======================================================================
  function ran_mwc(iseed)
    !=======================================================================
    !     This routine implements the Marsaglia "multiply with carry method"
    !     and adds a Marsaglia shift sequence to it.
    !     (cf. references at http://www.cs.hku.hk/)
    !     You are welcome to replace this with your preferred generator
    !     of uniform variates in the interval ]0,1[ (i.e. excluding 0 and 1)

    !     (Re-)initialise with setting iseed to a negative number.
    !     Note that iseed=0 gives the same sequence as iseed=-1
    !     After initialisation iseed becomes abs(iseed) (or 1 if it was 0).

    !     B. D. Wandelt, May 1999
    !=======================================================================
    implicit none
    integer(I4B), intent(inout):: iseed
    real(SP) :: ran_mwc

    integer(I4B) :: i,iseedl,iseedu,mwc,combined
    integer(I4B),save :: upper,lower,shifter
    integer(I4B),parameter :: mask16=65535,mask30=2147483647
    real(SP),save :: small
    logical(lgt), save :: first=.true.
    
    if (first.or.iseed<=0) then
       if(iseed==0) iseed=-1
       iseed=abs(iseed)
       small=nearest(1.0_sp,-1.0_sp)/mask30

       ! Users often enter small seeds - I spread them out using the
       ! Marsaglia shifter a few times.
       shifter=iseed
       do i=1,9
          shifter=ieor(shifter,ishft(shifter,13))
          shifter=ieor(shifter,ishft(shifter,-17))
          shifter=ieor(shifter,ishft(shifter,5))
       enddo

       iseedu=ishft(shifter,-16)
       upper=ishft(iseedu+8765,16)+iseedu !This avoids the fixed points.
       iseedl=iand(shifter,mask16)
       lower=ishft(iseedl+4321,16)+iseedl !This avoids the fixed points.

       first=.false.
    endif

100 continue

    shifter=ieor(shifter,ishft(shifter,13))
    shifter=ieor(shifter,ishft(shifter,-17))
    shifter=ieor(shifter,ishft(shifter,5))
    
    upper=36969*iand(upper,mask16)+ishft(upper,-16)
    lower=18000*iand(lower,mask16)+ishft(lower,-16)

    mwc=ishft(upper,16)+iand(lower,mask16)

    combined=iand(mwc,mask30)+iand(shifter,mask30)

    ran_mwc=small*iand(combined,mask30)
    if(ran_mwc==0._sp) goto 100

    return
  end function ran_mwc


  subroutine randomgen(x,px,mn,mx,Nsample,samplex)
    implicit none
    ! sample from a generic 1D distribution
    ! using transformation of variable to one having uniform distribution
    ! inputs: x,px = random variable and pdf
    ! input: Nsample =number of elements to be sampled
    ! output: samplex=sample

    real(dp),intent(in)::x(:),mn,mx,px(:)
    integer,intent(in)::Nsample
    real(dp),intent(out)::samplex(:)
    integer,parameter::resolution=50
    integer,allocatable::numbers(:)
    integer::iostat,N2,N,iseed,i,P(1),l(1),nl,j,jj,ii
    real(dp),allocatable::xnew(:),pnew(:),cumul(:),y(:),dif(:),ynew(:)!,px(:)
    real(dp),allocatable::histo(:,:),sampleplus(:)
    real(dp)::sampley(Nsample),avg,test,st,binsize
    real(dp)::p_old,p_new,deltaPcum,dx,min_i,max_i
    real(dp),allocatable::b(:),c(:),d(:)
    integer(4) :: ic4, crate4, cmax4,ni,sum_plus
    !double precision ispline
    save iseed

    !getting seed from the clock
    call system_clock(count=ic4, count_rate=crate4, count_max=cmax4)
    iseed=ic4


    N=size(x)
    N2=N*resolution

! smooth px

    allocate(b(N),c(N),d(N))!spline
    allocate(xnew(N2),pnew(N2),cumul(N2),dif(N2),y(N),ynew(N2),stat=iostat)
    if (iostat/=0) then
       print*,'RANDOMGEN: Allocation error'
    endif

    ! linearly interpolate x->xnew (regular grid)
    dx=abs(maxval(x)-minval(x))/dble(N2-1)

    xnew(1)=x(1)
    do i=2,N2
       xnew(i)=xnew(i-1)+dx
    enddo


    ! interpolate px->pxnew (spline)
    call spline (x, px, b, c, d,N) 
    do i=1,N2
       pnew(i)=ispline(xnew(i), x, px, b, c, d, n)
    enddo



    !quadratically interpolate px->pnew
    !call quadratic_iterpolation(x,px,xnew,pnew,N,N2)

    !check for negative numbers in pnew and substitute with 0.

!!$    do i=1,N
!!$       print*,px(i)
!!$    enddo
!!$stop
    do i=1,N2
       if (pnew(i) <0.) pnew(i)=0.
    enddo

    pnew=pnew/sum(pnew)

    !compute cumulative distribution
    cumul(1)=pnew(1)
    do i=2,N2
       cumul(i)=cumul(i-1)+pnew(i)
    enddo

!STOP


    !find y = mapping to give uniform distribution 
    deltaPcum=1./dble(N-1)
    p_old=0.-deltaPcum
    do i=1,N
       p_new=p_old+deltaPcum
       dif=abs(cumul-p_new)
       l=minloc(dif)
       y(i)=xnew(l(1))
       p_old=p_new
    enddo

    ! increase resolution of y->ynew
    call spline (x, y, b, c, d,N) 
    do i=1,N2
       ynew(i)=ispline(xnew(i), x, y, b, c, d, n)
    enddo

    !sample with uniform distribution
    do i=1,Nsample
       sampley(i)=ran_mwc(iseed)
    enddo

    !new range for xnew from 0 to 1 (to match range of sampley)
    dx=1./dble(N2-1)
    xnew(1)=0.
    do i=2,N2
       xnew(i)=xnew(i-1)+dx
    enddo

    !convert sampley ->samplex
    do i=1,Nsample
       dif=abs(xnew-sampley(i))
       l=minloc(dif)
       samplex(i)=ynew(l(1))
       samplex(i)=samplex(i)*(1.+randgauss_boxmuller(iseed)/100.) !1% random fluctuations
   enddo

   deallocate(b,c,d,xnew,pnew,cumul,dif,y,ynew,stat=iostat)
   if (iostat/=0) then
      print*,'RANDOMGEN: Deallocation Error '
   endif
   
  end subroutine randomgen

  subroutine adjust(x,px,Nsample,samplex)
    real(dp)::x(:),px(:)
    integer,intent(in)::Nsample
    real(dp)::samplex(:)
    integer,allocatable::numbers(:)
    integer::iostat,N2,N,iseed,i,P(1),l(1),nl,j,jj,ii
    real(dp),allocatable::histo(:,:),sampleplus(:)
    real(dp)::sampley(Nsample),avg,test,st,binsize
    real(dp)::p_old,p_new,deltaPcum,dx,min_i,max_i
    real(dp),allocatable::b(:),c(:),d(:)
    integer(4) :: ic4, crate4, cmax4,ni,sum_plus
    !double precision ispline
    save iseed
    call system_clock(count=ic4, count_rate=crate4, count_max=cmax4)
    iseed=ic4
    

    N=size(x)

    !check samplex with px and optimize
    allocate(histo(N,2),numbers(N))
    binsize=x(2)-x(1)
    histo(:,1)=x(:)


    call histogram(samplex,N,binsize,histo)
    px=px/sum(px)*sum(histo(:,2)) !both distributions to the same total to be compared

    sum_plus=0
    do i=1,N
       numbers(i)=px(i)-histo(i,2) !number of samples to be added/removed
       if (numbers(i) >0.) sum_plus=sum_plus+numbers(i)
    enddo
    !print*,'da cambiare:',sum_plus

    allocate(sampleplus(sum_plus),stat=iostat)
    if (iostat /=0) then
       print*,'randomgen: Error allocating samples'
       stop
    endif


    !sample to add:
    ii=1
    do i=1,N
       if (numbers(i)>0.) then
          ni=numbers(i)
          min_i=x(i)-binsize/2.
          max_i=x(i)+binsize/2.
          !print*, 'limits:',min_i,max_i,ni
          do j=1,ni
             sampleplus(ii)=ran_mwc(iseed)*(max_i-min_i)+min_i
             !         print*,sampleplus(ii)
             ii=ii+1
          enddo
       endif
    enddo

    !print*,'sampleplus done',size(sampleplus)


    !sample to remove:
    jj=1
    do i=1,N
       if (numbers(i)<0.) then
          ni=abs(numbers(i))
          !print*,i,ni
          min_i=x(i)-binsize/2.
          max_i=x(i)+binsize/2.
          do j=1,ni
             do ii=1,Nsample
                if ((samplex(ii) > min_i) .and. (samplex(ii) < max_i)) then
                   samplex(ii)=-1.e30!sampleplus(jj)
                   go to 100 
                endif
             enddo
100          continue
          enddo
       endif
    enddo
    jj=0
    do ii=1,Nsample
       if (samplex(ii)==-1.e30) then
          jj=jj+1
          samplex(ii)=sampleplus(jj)
       endif
    enddo

    deallocate(sampleplus,histo,numbers,stat=iostat)
    if (iostat/=0) then
       print*,'ADJUST: Deallocation Error '
    endif
   
  end subroutine adjust

  subroutine pca(variables,Nvar,Nsample,eigenvectors)
    implicit none
    !Principal component analysis to get uncorrelated variables
    real(dp),intent(in)::variables(:,:)
    integer,intent(in)::Nvar,Nsample
    real(dp),intent(out)::eigenvectors(:,:)
    integer::i,j,ii
    real(dp)::covmat(Nvar,Nvar),avg(nvar),variables2(Nsample,Nvar),xx
    integer::LDA,LWORK,INFO
    integer,parameter::LWMAX=100
    real(dp)::A(Nvar,Nvar),W(Nvar),work(LWMAX)
    !real(dp),allocatable::work(:)

    variables2=variables
    do i=1,Nvar
       avg(i)=sum(variables(:,i))/dble(Nsample)
       variables2(:,i)=variables2(:,i)-avg(i) !mean-subtracted variable
    enddo

    covmat(:,:)=0.

    do i=1,Nvar
       do j=1,i
          xx=0.
          do ii=1,Nsample
             xx=xx+variables2(ii,i)*variables2(ii,j)
          enddo
          xx=xx/dble(Nsample)
          covmat(i,j)=xx
          covmat(j,i)=xx
       enddo
    enddo

    A=covmat
    LDA=Nvar  !leading dimension of A
    LWORK=-1
    !query optimal workspace

    call dsyev ('V', 'U', Nvar, A, LDA, W, WORK, LWORK, INFO)
    LWORK = MIN( LWMAX, INT( WORK( 1 ) ) )
    !solve eigenproblem

    call dsyev ('V', 'U', Nvar, A, LDA, W, WORK, LWORK, INFO)
    if (INFO /=0) then 
       print*,'PCA: matrix diagonalization failed'
       stop
    endif
    !on output A contains eigenvectors columnwise
    !( A( I, J ), J = 1, N )

    eigenvectors=A

  end subroutine pca

  subroutine histogram(data,nbins,binsize,histo)
    implicit none
    real(dp)::histo(:,:)
    real(dp)::data(:)
    integer::ndata,i,nbins,ii,nbins_ini
    real(dp)::avg,st,binsize,test,tot,mn,mx

    ndata=size(data)
    if (nbins==-1) then
!determine bin size
       avg=sum(data)/dble(ndata)

       test=0.
       do i=1,ndata
          test=test+(data(i)-avg)**2 ! mean is 0.
       enddo
       st=sqrt(test/dble(ndata))
       mn=minval(data)!-st !extrapolate values
       mx=maxval(data)!+st !extrapolate values

       binsize=st/3. ! three bins inside 1 sigma
       !       binsize=st/5. ! three bins inside 1 sigma
       nbins=0
       test=mn
       do while (test <=mx)
          nbins=nbins+1
          test=test+binsize
       enddo
       nbins=nbins+2 ! 0 elements on first and last bin 
    else 
       if (sum(histo(:,1))==0.) then !compute binning if not provided
          mn=minval(data)!-st !extrapolate values
          mx=maxval(data)!+st !extrapolate values

          ! if number of bins is set proceed with histogram
          test=mn+binsize/2.-binsize !centre of bin, one bin more added at the beginning
          do i=1,nbins
             histo(i,1)=test
             test=test+binsize
          enddo
       endif
       do i=1,nbins
          tot=0.
          do ii=1,ndata
             if ((data(ii) >=histo(i,1)-binsize/2.) .and. (data(ii) <histo(i,1)+binsize/2.)) tot=tot+1.
          enddo
          histo(i,2)=tot
       enddo

    endif
  end subroutine histogram

subroutine getsample(array,sample,mapping,N)


  implicit none

  real(dp),intent(in)::array(:)
  real(dp),intent(out)::sample(:)
  integer, intent(out)::mapping(:),N
  real(sp)::element
  integer*8::i,j,k,N1,N2,test
  INTEGER omp_get_thread_num 
  sample(:)=-100.
  N1=size(array(:))

  N=1

     do i=1,N1
        test=0
        element=sngl(array(i))
        do k=1,N
           if (sample(k) == element) then
              test=1
              mapping(i)=k
           endif
        enddo
        if (test/=1) then
           sample(N)=element
           mapping(i)=N
           N=N+1
        endif
        !print*,i,j,mapping(i,j),element
     enddo
  N=N-1
end subroutine getsample


  end module random_tools


