
subroutine bandpass_window_function(M_lmax,lowl,highl,M_eb,M_ee_bb,K_eb,K_ee_bb,W_eb,W_ee,W_bb)
	! lmax is read from the input file directly
	implicit none
	real(8):: W_eb(:,:), W_ee(:,:), W_bb(:,:)
	real(8):: K_eb(:,:), K_ee_bb(:,:)
	real(4):: M_eb(:,:), M_ee_bb(:,:) 
	integer(4),intent(in):: lowl(:), highl(:)
	real(8), allocatable :: P_bl(:,:), Q_lb(:,:)
	integer(4) :: i, j, l, k, kk
	integer(4) :: lmax, lmin, nbin, binflag, binsize,M_lmax
	real(8) :: ellmin, ellmax, dum
	integer(4) :: nl, flag, ltest
	real(8) :: tot_eb,tot_ee,tot_bb


	nbin=size(highl)
	lmax = highl(nbin)
	nl = lmax - 1  
	allocate(P_bl(1:nbin, 1:nl))
	allocate(Q_lb(1:nl, 1:nbin))

	do i = 1, nl
	   do j = 1, nbin
		  P_bl(j,i) = 0.0
		  Q_lb(i,j) = 0.0 
	   enddo
	   do j = 1, nbin
		  ltest = i + 1
		  if (ltest.ge.lowl(j).and.ltest.le.highl(j)) then
			 P_bl(j,i) = real(i+1) * real(i+2) / (real(highl(j)) - real(lowl(j)) + 1.) / 2. / pi
			 Q_lb(i,j) = 2. * pi / real(i+1) / real(i+2)
		  endif
	   enddo
	enddo

	do i=1 , nbin ! this is b
		do j=1, nl ! this is l
			! set elements of the W matrices to 0
			W_eb(i,j) = 0.0
			W_ee(i,j) = 0.0
			W_bb(i,j) = 0.0
			
			do k=1, nbin ! this is b'
				
				tot_eb=0.0
				tot_ee=0.0
				tot_bb=0.0
				
				do kk=1, nl ! this is l'
					
					! if i run after deconvolve spectrum, the K matrices will be already inverted
					tot_eb = tot_eb + P_bl(k,kk) * M_eb(kk,j)
					tot_ee = tot_ee + P_bl(k,kk) * M_ee_bb(kk,j)
					tot_bb = tot_bb + P_bl(k,kk) * M_ee_bb(kk+nl,j+nl)
					
									
					!W_eb(i,j) = W_eb(i,j) + (2. * pi) / real(i+2) * K_eb(i,k) * P_bl(k,kk) * M_eb(kk,j)
					!W_eb(i,j) = W_eb(i,j) + (2. * pi) / (real(i+1)*real(i+2)) * K_eb(i,k) * P_bl(k,kk) * M_eb(kk,j)
					
					!W_ee(i,j) = W_ee(i,j) + (2. * pi) / real(i+2) * K_ee_bb(i,k) * P_bl(k,kk) * M_ee_bb(kk,j) 
					!W_ee(i,j) = W_ee(i,j) + (2. * pi) / (real(i+1)*real(i+2)) * K_ee_bb(i,k) * P_bl(k,kk) * M_ee_bb(kk,j) 
	!				
					!W_bb(i,j) = W_bb(i,j) + (2. * pi) / real(i+2) * K_ee_bb(i+nbin,k+nbin) * P_bl(k,kk) * M_ee_bb(kk+nl,j+nl)
					!W_bb(i,j) = W_bb(i,j) + (2. * pi) / (real(i+1)*real(i+2)) * K_ee_bb(i+nbin,k+nbin) * P_bl(k,kk) * M_ee_bb(kk+nl,j+nl)

				enddo
				
				W_eb(i,j) = W_eb(i,j) + (2. * pi) / real(i+2) * K_eb(i,k) 				* tot_eb
				W_ee(i,j) = W_ee(i,j) + (2. * pi) / real(i+2) * K_ee_bb(i,k) 			* tot_ee
				W_bb(i,j) = W_bb(i,j) + (2. * pi) / real(i+2) * K_ee_bb(i+nbin,k+nbin) 	* tot_bb


			enddo
		enddo
	enddo

	deallocate(P_bl)
	deallocate(Q_lb)

end subroutine
