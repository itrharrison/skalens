module master_io
  use healpix_types
  USE fitstools
  USE utilities
  USE pix_tools
  USE paramfile_io, ONLY : paramfile_handle, parse_init, parse_int, &
       parse_string, parse_double, parse_lgt, concatnl
contains

  subroutine get_info(filename,npixtot,nmaps,ordering,nsmax)
    USE healpix_types
    USE fitstools

    IMPLICIT NONE

    INTEGER(I4B) :: nmaps,ordering,nsmax,nside,mlpol,npixtot
    CHARACTER(LEN=filenamelen) :: filename

    npixtot=getsize_fits(filename,nmaps=nmaps,ordering=ordering,nside=nsmax,mlpol=mlpol)
  end subroutine get_info


 function rows_number(filename,iunit)
    integer:: reason,iunit,nrows,rows_number
    character (LEN=200)::filename
    real(DP)::col

close(iunit)
    open (UNIT=iunit,file=filename,status='unknown',form="formatted")
    print*,filename
    nrows=1
    DO
       READ(iunit,*,IOSTAT=Reason)  col
       IF (Reason > 0)  THEN 
          print*,'Error in reading file'
          print*,filename
          stop
       ELSE IF (Reason < 0) THEN
          exit
       ELSE
          nrows=nrows+1
       END IF
    END DO
   rows_number=nrows-1
    CLOSE (unit=iunit)
print*,'Nrows=',rows_number
    return
  end function rows_number
  
  subroutine read_columns(filename,iunit,nrows,Ncolumns,data)
    character (LEN=200)::filename
    integer:: reason,iunit,nrows,Ncolumns,i
    real(dp):: col1,col2,col3,col4,col5,col6,col7,col8,col9,col10
    real(dp)::data(:,:)

    data(:,:)=0.
    if (Ncolumns >10) then 
       print*,'Number of columns not supported!'
       stop
    endif
close(iunit)
    open (UNIT=iunit,file=filename,status='unknown',form="formatted")

    SELECT CASE(Ncolumns)

    CASE(1)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1
          data(i,1)=col1
       ENDDO
    CASE(2)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2
          data(i,1)=col1
          data(i,2)=col2
       ENDDO
    CASE(3)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2,col3
          data(i,1)=col1
          data(i,2)=col2
          data(i,3)=col3
       ENDDO

    CASE(4)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2,col3,col4
          data(i,1)=col1
          data(i,2)=col2
          data(i,3)=col3
          data(i,4)=col4
       ENDDO
    CASE(5)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2,col3,col4,col5
          data(i,1)=col1
          data(i,2)=col2
          data(i,3)=col3
          data(i,4)=col4
          data(i,5)=col5
       ENDDO
    CASE(6)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2,col3,col4,col5,col6
          data(i,1)=col1
          data(i,2)=col2
          data(i,3)=col3
          data(i,4)=col4
          data(i,5)=col5
          data(i,6)=col6
       ENDDO

    CASE(7)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2,col3,col4,col5,col7
          data(i,1)=col1
          data(i,2)=col2
          data(i,3)=col3
          data(i,4)=col4
          data(i,5)=col5
          data(i,6)=col6
          data(i,7)=col7
       ENDDO
    CASE(8)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2,col3,col4,col5,col7,col8
          data(i,1)=col1
          data(i,2)=col2
          data(i,3)=col3
          data(i,4)=col4
          data(i,5)=col5
          data(i,6)=col6
          data(i,7)=col7
          data(i,8)=col8
       ENDDO

    CASE(9)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2,col3,col4,col5,col7,col8,col9
          data(i,1)=col1
          data(i,2)=col2
          data(i,3)=col3
          data(i,4)=col4
          data(i,5)=col5
          data(i,6)=col6
          data(i,7)=col7
          data(i,8)=col8
          data(i,9)=col9
       ENDDO

    CASE(10)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2,col3,col4,col5,col7,col8,col9,col10
          data(i,1)=col1
          data(i,2)=col2
          data(i,3)=col3
          data(i,4)=col4
          data(i,5)=col5
          data(i,6)=col6
          data(i,7)=col7
          data(i,8)=col8
          data(i,9)=col9
          data(i,10)=col10
       ENDDO
    END SELECT
  CLOSE (unit=iunit)
 end subroutine read_columns


 subroutine write_columns(filename,iunit,nrows,Ncolumns,data)
    character (LEN=200)::filename
    integer:: reason,iunit,nrows,Ncolumns,i
    real(dp):: col1,col2,col3,col4,col5,col6,col7,col8,col9,col10
    real(dp)::data(:,:)

   
    if (Ncolumns >10) then 
       print*,'Number of columns not supported!'
       stop
    endif

    close(iunit)
    open (UNIT=iunit,file=filename,status='unknown',form="formatted")

    SELECT CASE(Ncolumns)

    CASE(1)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason)  data(i,1)         
       ENDDO
    CASE(2)
       DO i=1,nrows
           write(iunit,*,IOSTAT=Reason)   data(i,1), data(i,2)
        ENDDO
    CASE(3)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason)  data(i,1), data(i,2),data(i,3)
       ENDDO

    CASE(4)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason)  data(i,1), data(i,2),data(i,3),data(i,4)
          
       ENDDO
    CASE(5)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason) data(i,1), data(i,2),data(i,3),data(i,4)&
               &,data(i,5)
          
       ENDDO
    CASE(6)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason) data(i,1), data(i,2),data(i,3),data(i,4)&
               &,data(i,5),data(i,6)
          
       ENDDO

    CASE(7)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason)  data(i,1), data(i,2),data(i,3),data(i,4)&
               &,data(i,5),data(i,6),data(i,7)
          
       ENDDO
    CASE(8)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason)  data(i,1), data(i,2),data(i,3),data(i,4)&
               &,data(i,5),data(i,6),data(i,7),data(i,8)
          
       ENDDO

    CASE(9)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason)  data(i,1), data(i,2),data(i,3),data(i,4)&
               &,data(i,5),data(i,6),data(i,7),data(i,8),data(i,9)
          
       ENDDO

    CASE(10)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason)   data(i,1), data(i,2),data(i,3),data(i,4)&
               &,data(i,5),data(i,6),data(i,7),data(i,8),data(i,9),data(i,10)
          
       ENDDO
    END SELECT
  CLOSE (unit=iunit)
 end subroutine write_columns

  end module master_io
