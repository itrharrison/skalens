
module coupling
! contains the subroutine and main part of the code coupling.f90 by ML Brown 
real(8), parameter :: pi = 3.141592653589793238462643383279502884197
contains


  subroutine compute_coupling(w_tt,w_pp,w_tp,M_tt,M_te,M_eb,M_ee_bb,ell,nl,nmask)
    implicit none
    real(8),intent(in)::ell(:),w_tt(:,:),w_pp(:,:),w_tp(:,:)
    real(4)::M_tt(:,:,:), M_te(:,:,:), M_eb(:,:,:), M_ee_bb(:,:,:) 
    integer(4),intent(in)::nl,nmask
    real(8), allocatable :: wig3j_000(:)
    real(8), allocatable :: result1(:), result2(:), result3(:)
    integer(4)::i,j,iostat,imask,k,kk
    ! wigner 3-j stuff
    integer(4) :: l3min, l3max
    
    allocate(wig3j_000(1:nl),result1(1:2*nl+10),result2(1:2*nl+10),result3(1:2*nl+10),stat=iostat)
    if (iostat /=0) then
       print*,'Compute Coupling: Error allocating arrays'
       stop
    endif

    do i = 1, nl
       !print*, 'multipoles', i
       do j = 1, nl  
          do imask = 1, nmask
             M_tt(imask, i, j) = 0.
             M_te(imask, i, j) = 0.
             M_eb(imask, i, j) = 0.
             M_ee_bb(imask, i, j) = 0.
             M_ee_bb(imask, i + nl, j) = 0.
             M_ee_bb(imask, i, j + nl) = 0.
             M_ee_bb(imask, i + nl, j + nl) = 0.
          end do
          l3min = int(abs(ell(j) - ell(i))) 
          l3max = int(ell(i) + ell(j))
          ! using SLATEC routine for 3-j symbols:
          call rec3jj(result1, ell(i), ell(j), 0.d0, 0.d0)
          do k = 1, nl
             wig3j_000(k) = 0.e0
          enddo
          do k = 1, l3max - l3min + 1
             kk = l3min + k
             if (kk.le.nl) then 
                wig3j_000(kk) = result1(k)
                do imask = 1, nmask
                   M_tt(imask, i, j) = M_tt(imask, i, j) + w_tt(imask, kk) * result1(k)**2. ! TT-TT
                end do
             endif
          enddo
          if (ell(i).ge.2.d0.and.ell(j).ge.2.d0) then
             call rec3jj(result2, ell(i), ell(j), -2.d0, 2.d0)                
             call rec3jj(result3, ell(i), ell(j), 2.d0, -2.d0)
             do k = 1, l3max - l3min + 1
                kk = l3min + k
                if (kk.le.nl) then
                   do imask = 1, nmask
                      M_te(imask, i, j) = M_te(imask, i, j) + w_tp(imask, kk) * &
                           wig3j_000(kk) * (result2(k) + result3(k))     ! TE-TE (and TB-TB)
                      M_eb(imask, i, j) = M_eb(imask, i, j) + w_pp(imask, kk) * &
                           (result2(k)**2. + result3(k)**2.)             ! EB-EB
                      M_ee_bb(imask, i, j) = M_ee_bb(imask, i, j) + w_pp(imask, kk) * &
                           (result2(k) + result3(k))**2.                 ! EE-EE
                      M_ee_bb(imask, i, j+nl) = M_ee_bb(imask, i, j+nl) + w_pp(imask, kk) * &
                           (result2(k) - result3(k))**2.                 ! EE-BB
                   end do
                endif
             enddo
          endif
          do imask = 1, nmask
             M_ee_bb(imask, i+nl, j+nl) = M_ee_bb(imask, i, j)   ! BB-BB
             M_ee_bb(imask, i+nl, j) = M_ee_bb(imask, i, j+nl)   ! BB-EE
             M_tt(imask, i, j) = (2. * ell(j) + 1.) / 4. / pi * M_tt(imask, i, j)
             M_te(imask, i, j) = (2. * ell(j) + 1.) / 8. / pi * M_te(imask, i, j)
             M_eb(imask, i, j) = (2. * ell(j) + 1.) / 8. / pi * M_eb(imask, i, j)
             M_ee_bb(imask, i, j) = (2. * ell(j) + 1.) / 16. / pi * M_ee_bb(imask, i, j)
             M_ee_bb(imask, i+nl, j+nl) = (2. * ell(j) + 1.) / 16. / pi * M_ee_bb(imask, i+nl, j+nl)
             M_ee_bb(imask, i+nl, j) = (2. * ell(j) + 1.) / 16. / pi * M_ee_bb(imask, i+nl, j)
             M_ee_bb(imask, i, j+nl) = (2. * ell(j) + 1.) / 16. / pi * M_ee_bb(imask, i, j+nl)
          end do
       enddo
    enddo

    deallocate(wig3j_000,result1,result2,result3,stat=iostat) 
    if (iostat /=0) then
       print*,'Compute Coupling: Error deallocating arrays'
       stop
    endif

  end subroutine compute_coupling


subroutine compute_coupling_pol(w_pp,M_eb,M_ee_bb,ell,nl,nmask)
!same as compute_coupling but compute just the polarization parts
    implicit none
    real(8),intent(in)::ell(:),w_pp(:,:)
    real(4)::M_eb(:,:,:), M_ee_bb(:,:,:) 
    integer(4),intent(in)::nl,nmask
    real(8), allocatable :: wig3j_000(:)
    real(8), allocatable :: result1(:), result2(:), result3(:)
    integer(4)::i,j,iostat,imask,k,kk
    ! wigner 3-j stuff
    integer(4) :: l3min, l3max
    
    allocate(wig3j_000(1:nl),result1(1:2*nl+10),result2(1:2*nl+10),result3(1:2*nl+10),stat=iostat)
    if (iostat /=0) then
       print*,'Compute Coupling: Error allocating arrays'
       stop
    endif

    do i = 1, nl
       !print*, 'multipoles', i
       do j = 1, nl  
          do imask = 1, nmask
!!$             M_tt(imask, i, j) = 0.
!!$             M_te(imask, i, j) = 0.
             M_eb(imask, i, j) = 0.
             M_ee_bb(imask, i, j) = 0.
             M_ee_bb(imask, i + nl, j) = 0.
             M_ee_bb(imask, i, j + nl) = 0.
             M_ee_bb(imask, i + nl, j + nl) = 0.
          end do
          l3min = int(abs(ell(j) - ell(i))) 
          l3max = int(ell(i) + ell(j))
          ! using SLATEC routine for 3-j symbols:
          call rec3jj(result1, ell(i), ell(j), 0.d0, 0.d0)
          do k = 1, nl
             wig3j_000(k) = 0.e0
          enddo
!!$          do k = 1, l3max - l3min + 1
!!$             kk = l3min + k
!!$             if (kk.le.nl) then 
!!$                wig3j_000(kk) = result1(k)
!!$                do imask = 1, nmask
!!$                   M_tt(imask, i, j) = M_tt(imask, i, j) + w_tt(imask, kk) * result1(k)**2. ! TT-TT
!!$                end do
!!$             endif
!!$          enddo
          if (ell(i).ge.2.d0.and.ell(j).ge.2.d0) then
             call rec3jj(result2, ell(i), ell(j), -2.d0, 2.d0)                
             call rec3jj(result3, ell(i), ell(j), 2.d0, -2.d0)
             do k = 1, l3max - l3min + 1
                kk = l3min + k
                if (kk.le.nl) then
                   do imask = 1, nmask
                      M_eb(imask, i, j) = M_eb(imask, i, j) + w_pp(imask, kk) * &
                           (result2(k)**2. + result3(k)**2.)             ! EB-EB
                      M_ee_bb(imask, i, j) = M_ee_bb(imask, i, j) + w_pp(imask, kk) * &
                           (result2(k) + result3(k))**2.                 ! EE-EE
                      M_ee_bb(imask, i, j+nl) = M_ee_bb(imask, i, j+nl) + w_pp(imask, kk) * &
                           (result2(k) - result3(k))**2.                 ! EE-BB
                   end do
                endif
             enddo
          endif
          do imask = 1, nmask
             M_ee_bb(imask, i+nl, j+nl) = M_ee_bb(imask, i, j)   ! BB-BB
             M_ee_bb(imask, i+nl, j) = M_ee_bb(imask, i, j+nl)   ! BB-EE
             M_eb(imask, i, j) = (2. * ell(j) + 1.) / 8. / pi * M_eb(imask, i, j)
             M_ee_bb(imask, i, j) = (2. * ell(j) + 1.) / 16. / pi * M_ee_bb(imask, i, j)
             M_ee_bb(imask, i+nl, j+nl) = (2. * ell(j) + 1.) / 16. / pi * M_ee_bb(imask, i+nl, j+nl)
             M_ee_bb(imask, i+nl, j) = (2. * ell(j) + 1.) / 16. / pi * M_ee_bb(imask, i+nl, j)
             M_ee_bb(imask, i, j+nl) = (2. * ell(j) + 1.) / 16. / pi * M_ee_bb(imask, i, j+nl)
          end do
       enddo
    enddo

    deallocate(wig3j_000,result1,result2,result3,stat=iostat) 
    if (iostat /=0) then
       print*,'Compute Coupling: Error deallocating arrays'
       stop
    endif

  end subroutine compute_coupling_pol



subroutine rec3jj(thrcof,l2,l3,m2,m3)
  ! Recursive evaluation of 3j symbols. Does minimal error checking on input parameters.
  ! MLB: borrowed from CAMB...

  implicit none
  real(8), intent(in) :: l2,l3, m2,m3
  real(8) :: l1, m1, l1min,l1max, newfac,lmatch
  real(8), dimension(*) :: thrcof
  integer ier
  real(8), parameter :: zero=0._8, eps=0.01_8, one=1._8
  real(8) :: srtiny, sum1, tiny, oldfac, a1, a2, c1, dv, denom, c1old, &
              x, sumuni, c2, sumfor, srhuge, huge, x1, x2, x3, sum2, &
              a1s, a2s, y, sumbac, y1, y2, y3, ratio, cnorm, sign1, &
              sign2, thresh
  integer :: l1cmin, l1cmax, nfin, lstep, i, nstep2, nfinp1, nfinp2, &
             nfinp3, index, nlim, n


! routine to generate set of 3j-coeffs (l1,l2,l3\\ m1,m2,m3)

! by recursion from l1min = max(abs(l2-l3),abs(m1)) 
!                to l1max = l2+l3
! the resulting 3j-coeffs are stored as thrcof(l1-l1min+1)

! to achieve the numerical stability, the recursion will proceed
! simultaneously forwards and backwards, starting from l1min and l1max
! respectively.
!
! lmatch is the l1-value at which forward and backward recursion are matched.
!
! ndim is the length of the array thrcof
!
! ier = -1 for all 3j vanish(l2-abs(m2)<0, l3-abs(m3)<0 or not integer)
! ier = -2 if possible 3j's exceed ndim
! ier >= 0 otherwise
!
  data tiny,srtiny /1.0d-30,1.0d-15/
  data huge,srhuge /1.0d30,1.0d15/

  lmatch = zero
  m1 = -(m2+m3)

! check relative magnitude of l and m values
  ier = 0
 
  if (l2 < abs(m2) .or. l3 < m3) then
  ier = -1
  return
  end if

! limits for l1
  l1min = max(abs(l2-l3),abs(m1))
  l1max = l2+l3

  if (l1min >= l1max-eps) then
   if (l1min/=l1max) then
   ier = -1
   return
   end if

! reached if l1 can take only one value, i.e.l1min=l1max
  thrcof(1) = (-1)**nint(abs(l2+m2-l3+m3))/sqrt(l1min+l2+l3+one)
  l1cmin = l1min
  l1cmax = l1max
  return

  end if

  nfin = int(l1max-l1min+one)
 

! starting forward recursion from l1min taking nstep1 steps
  l1 = l1min
  thrcof(1) = srtiny
  sum1 = (2*l1 + 1)*tiny

  lstep = 1

30  lstep = lstep+1
  l1 = l1+1

  oldfac = newfac
  a1 = (l1+l2+l3+1)*(l1-l2+l3)*(l1+l2-l3)*(-l1+l2+l3+1)
  a2 = (l1+m1)*(l1-m1)
  newfac = sqrt(a1*a2)
  if (l1 < one+eps) then
     !IF L1 = 1  (L1-1) HAS TO BE FACTORED OUT OF DV, HENCE
     c1 = -(2*l1-1)*l1*(m3-m2)/newfac
  else

   dv = -l2*(l2+1)*m1 + l3*(l3+1)*m1 + l1*(l1-1)*(m3-m2)
   denom = (l1-1)*newfac

   if (lstep > 2) c1old = abs(c1)
   c1 = -(2*l1-1)*dv/denom

  end if

  if (lstep<= 2) then

! if l1=l1min+1 the third term in the recursion eqn vanishes, hence
   x = srtiny*c1
   thrcof(2) = x
   sum1 = sum1+tiny*(2*l1+1)*c1*c1
   if(lstep==nfin) then
      sumuni=sum1
      go to 230
   end if
   goto 30

  end if

  c2 = -l1*oldfac/denom

! recursion to the next 3j-coeff x  
  x = c1*thrcof(lstep-1) + c2*thrcof(lstep-2)
  thrcof(lstep) = x
  sumfor = sum1
  sum1 = sum1 + (2*l1+1)*x*x
  if (lstep/=nfin) then

! see if last unnormalised 3j-coeff exceeds srhuge
  if (abs(x) >= srhuge) then
     
     ! REACHED IF LAST 3J-COEFFICIENT LARGER THAN SRHUGE
     ! SO THAT THE RECURSION SERIES THRCOF(1), ... , THRCOF(LSTEP)
     ! HAS TO BE RESCALED TO PREVENT OVERFLOW
     
     ier = ier+1
     do i = 1, lstep
        if (abs(thrcof(i)) < srtiny) thrcof(i)= zero
        thrcof(i) = thrcof(i)/srhuge
     end do

     sum1 = sum1/huge
     sumfor = sumfor/huge
     x = x/srhuge

  end if

! as long as abs(c1) is decreasing, the recursion proceeds towards increasing
! 3j-valuse and so is numerically stable. Once an increase of abs(c1) is 
! detected, the recursion direction is reversed.

 if (c1old > abs(c1)) goto 30

 end if !lstep/=nfin

! keep three 3j-coeffs around lmatch for comparison with backward recursion

  lmatch = l1-1
  x1 = x
  x2 = thrcof(lstep-1)
  x3 = thrcof(lstep-2)
  nstep2 = nfin-lstep+3

! --------------------------------------------------------------------------
!
! starting backward recursion from l1max taking nstep2 stpes, so that
! forward and backward recursion overlap at 3 points 
! l1 = lmatch-1, lmatch, lmatch+1

  nfinp1 = nfin+1
  nfinp2 = nfin+2
  nfinp3 = nfin+3
  l1 = l1max
  thrcof(nfin) = srtiny
  sum2 = tiny*(2*l1+1)
 
  l1 = l1+2
  lstep=1

  do
  lstep = lstep + 1
  l1= l1-1

  oldfac = newfac
  a1s = (l1+l2+l3)*(l1-l2+l3-1)*(l1+l2-l3-1)*(-l1+l2+l3+2)
  a2s = (l1+m1-1)*(l1-m1-1)
  newfac = sqrt(a1s*a2s)

  dv = -l2*(l2+1)*m1 + l3*(l3+1)*m1 +l1*(l1-1)*(m3-m2)

  denom = l1*newfac
  c1 = -(2*l1-1)*dv/denom
  if (lstep <= 2) then

     ! if l2=l2max+1, the third term in the recursion vanishes
     
     y = srtiny*c1
     thrcof(nfin-1) = y
     sumbac = sum2
     sum2 = sum2 + tiny*(2*l1-3)*c1*c1

     cycle

  end if

  c2 = -(l1-1)*oldfac/denom

! recursion to the next 3j-coeff y
  y = c1*thrcof(nfinp2-lstep)+c2*thrcof(nfinp3-lstep)

  if (lstep==nstep2) exit
  
  thrcof(nfinp1-lstep) = y
  sumbac = sum2
  sum2 = sum2+(2*l1-3)*y*y

! see if last unnormalised 3j-coeff exceeds srhuge
  if (abs(y) >= srhuge) then
     
     ! reached if 3j-coeff larger than srhuge so that the recursion series
     ! thrcof(nfin),..., thrcof(nfin-lstep+1) has to be rescaled to prevent overflow
     
     ier=ier+1
     do i = 1, lstep
        index=nfin-i+1
        if (abs(thrcof(index)) < srtiny) thrcof(index)=zero
        thrcof(index) = thrcof(index)/srhuge
     end do

     sum2=sum2/huge
     sumbac=sumbac/huge

  end if

  end do

! the forward recursion 3j-coeffs x1, x2, x3 are to be matched with the 
! corresponding backward recursion vals y1, y2, y3

  y3 = y
  y2 = thrcof(nfinp2-lstep)
  y1 = thrcof(nfinp3-lstep)

! determine now ratio such that yi=ratio*xi (i=1,2,3) holds with minimal error

  ratio = (x1*y1+x2*y2+x3*y3)/(x1*x1+x2*x2+x3*x3)
  nlim = nfin-nstep2+1

  if (abs(ratio) >= 1) then

   thrcof(1:nlim) = ratio*thrcof(1:nlim) 
   sumuni = ratio*ratio*sumfor + sumbac

  else

  nlim = nlim+1
  ratio = 1/ratio
  do n = nlim, nfin
     thrcof(n) = ratio*thrcof(n)
  end do
  sumuni = sumfor + ratio*ratio*sumbac

  end if
! normalise 3j-coeffs

230 cnorm = 1/sqrt(sumuni)

! sign convention for last 3j-coeff determines overall phase

  sign1 = sign(one,thrcof(nfin))
  sign2 = (-1)**nint(abs(l2+m2-l3+m3))
  if (sign1*sign2 <= 0) then
    cnorm = -cnorm
  end if
  if (abs(cnorm) >= one) then
     thrcof(1:nfin) = cnorm*thrcof(1:nfin)
     return
  end if

  thresh = tiny/abs(cnorm)

  do n = 1, nfin
     if (abs(thrcof(n)) < thresh) thrcof(n) = zero
     thrcof(n) = cnorm*thrcof(n)
  end do
  return 

end subroutine rec3jj

!#################################################################!


end module coupling



module binned_coupling
! contains the subroutine and main part of the code binned_coupling.f90 by ML Brown 
real(8), parameter :: pi = 3.141592653589793238462643383279502884197

contains

subroutine bin_coupling_pol(nl,W_pp,lowl,highl,M_eb,M_ee_bb,K_eb,K_ee_bb)
implicit none

real(8), allocatable :: K_eb_cp(:,:),K_ee_bb_cp(:,:)
real(8):: tot_ee,tot_bb,tot_eb
real(8):: W_pp(:),K_eb(:,:), K_ee_bb(:,:)
real(4):: M_eb(:,:), M_ee_bb(:,:) 
integer(4),intent(in):: lowl(:), highl(:)
real(8), allocatable :: P_bl(:,:), Q_lb(:,:)
integer(4) :: i, j, l, k, kk
integer(4) :: lmax, lmin, nbin, binflag, binsize,M_lmax
real(8) :: ellmin, ellmax, dum
integer(4) :: nl, flag, ltest, fl


nbin=size(highl)

allocate(P_bl(1:nbin, 1:nl))
allocate(Q_lb(1:nl, 1:nbin))
P_bl(:,:) = 0.0
Q_lb(:,:) = 0.0
do i = 3, nl
   do j = 1, nbin
      P_bl(j,i) = 0.0
      Q_lb(i,j) = 0.0
   enddo
   do j = 1, nbin
      ltest = i -1
      if (ltest.ge.lowl(j).and.ltest.le.highl(j)) then
         P_bl(j,i) = real(ltest) * real(ltest+1) / (real(highl(j)) - real(lowl(j)) + 1.) / 2. / pi
         Q_lb(i,j) = 2. * pi / real(ltest) / real(ltest+1)
      endif
   enddo
enddo


! get the bandpower-bandpower coupling matrix
do j = 1, nbin
   !print*, 'bins', j
   do i = 1, nbin
      K_eb(i,j) = 0.0
      K_ee_bb(i,j) = 0.0
      K_ee_bb(i,j + nbin) = 0.0
      K_ee_bb(i + nbin,j) = 0.0
      K_ee_bb(i + nbin,j + nbin) = 0.0

      do l = 3, nl 
         do k = 3, nl
            K_eb(i,j) = K_eb(i,j) + P_bl(i,k) * M_eb(k,l) * W_pp(l) * Q_lb(l,j) ! EB-EB
            K_ee_bb(i,j) = K_ee_bb(i,j) + P_bl(i,k) * M_ee_bb(k,l) * W_pp(l) * Q_lb(l,j) ! EE-EE
            K_ee_bb(i + nbin,j) = K_ee_bb(i + nbin,j) + P_bl(i,k)                      &
                 * M_ee_bb(k + nl,l) * W_pp(l) * Q_lb(l,j)                      ! EE-BB
            K_ee_bb(i,j + nbin) = K_ee_bb(i,j + nbin) + P_bl(i,k)                      &     
                 * M_ee_bb(k,l + nl) * W_pp(l) * Q_lb(l,j)                      ! BB-EE
            K_ee_bb(i + nbin,j + nbin) =  K_ee_bb(i + nbin,j + nbin) +  P_bl(i,k)      &
                 * M_ee_bb(k + nl,l + nl) * W_pp(l) * Q_lb(l,j)                 ! BB-BB
         enddo
      enddo
   enddo
enddo

end subroutine bin_coupling_pol

end module binned_coupling


module pcl2cl
! contains the subroutines and main part of the code pcl2cl.f90 by ML Brown 
real(8), parameter :: pi = 3.141592653589793238462643383279502884197

contains

  subroutine deconvolve_spectrum(ncl,nl,nbin,K_eb,K_eebb,ell,lowl,highl,ee_bb,eb,ee_true,bb_true,eb_true,inv_mode)
    


    implicit none
    integer(4) :: ii, i, j, k
    ! signal pseudo-cl and cl spectra:
    integer(4) :: nl, lmax
    real(8) :: dum, ttin, eein, bbin, tein, tbin, ebin, fudge_factor
    real(8) :: ell(:),eb(:), eb_true(:) ,ee_bb(:), ee_true(:), bb_true(:) 
    ! coupling matrices:
    integer(4) :: fl, nbin, ltest,ncl,inv_mode
    integer(4) :: lowl(:),highl(:)
    real(8) ::K_eb(:,:), K_eebb(:,:)  
    real(8), allocatable :: P_bl(:,:)



    ! invert binned coupling matrices:
    call minvert(K_eb, nbin, nbin, fl,inv_mode)     
    !print*, fl
    if (fl /= 0) then
       print*,'Matrix inversion error'
       stop
    endif
    call minvert(K_eebb, 2*nbin, 2*nbin, fl,inv_mode)
    !print*, fl
    if (fl /= 0) then
       print*,'Matrix inversion error'
       stop
    endif

    ! calculate binning matrix:



    allocate(P_bl(1:nbin, 1:nl))
    P_bl(:,:)=0.

    do i = 3, nl
       do j = 1, nbin
          P_bl(j,i) = 0.0
       enddo
       do j = 1, nbin
          ltest=ell(i)
          if (ltest.ge.lowl(j).and.ltest.le.highl(j)) then
             P_bl(j,i) = ell(i) * (ell(i) + 1) /                     &
                  (real(highl(j)) - real(lowl(j)) + 1.) / 2. / pi
          endif
       enddo
    enddo

    ! calculate full-sky cl bandpowers 
    ee_true = 0.0
    bb_true = 0.0
    eb_true = 0.0
    do i = 1, nbin
       do j = 1, nbin
          do k = 3, nl
             eb_true(i) = eb_true(i) + K_eb(i,j) * P_bl(j,k) * eb(k) 
             ee_true(i) = ee_true(i) + K_eebb(i,j) * P_bl(j,k) * ee_bb(k) &
                  + K_eebb(i,j + nbin) * P_bl(j,k) * ee_bb(k+ncl) 
             bb_true(i) = bb_true(i) + K_eebb(i + nbin,j) * P_bl(j,k) * ee_bb(k) &
                  + K_eebb(i + nbin,j + nbin) * P_bl(j,k) * ee_bb(k+ncl)  
          enddo
       enddo
    enddo

    deallocate(P_bl)

  end subroutine deconvolve_spectrum

!############################################################!
! Subroutine to invert a matrix                              !
!############################################################!                     
  subroutine minvert(matrix,n,np,info,inv_mode)
    INTEGER n,np,info,flag,inv_mode
    REAL(8) matrix(np,np),jcol(n),d
    INTEGER indx(n),i,j,n2
    real(8),allocatable::VT(:,:),U(:,:),S(:),UT(:,:),V(:,:),Sinv(:,:),work(:),mat2(:,:)
    INTEGER(8),allocatable,dimension(:) :: ipiv,iwork
    INTEGER(8)::iostat,lwork
    INTEGER(8)::LDA,LDU,LDVT
    real(8),parameter::precision=1.e-1
    N2=N
    LDA=N
    LDU=N
    LDVT=N2
    SELECT CASE(INV_MODE)

    CASE(1)  
       ! inversion with LU factorization
       PRINT*,'INVERSION WITH LU FACTORIZATION'
       ALLOCATE (ipiv(N),work(N),stat=iostat)
       if (iostat /= 0) then
          print*,'Failed to allocate work arrays for matrix inversion'
          stop
       endif

       lwork=-1

       print*,'calling DGETRF'
       call DGETRF(N ,N , matrix, N, IPIV, INFO )
       print*,'Done',info
       call DGETRI(N,matrix,N,IPIV,WORK,lwork,INFO)
       Lwork=int(work(1))
       print*,info,lwork
       deallocate(work)
       allocate(work(lwork),stat=iostat)
       if (iostat /= 0) then
          print*,'Failed to allocate work arrays for matrix inversion'
          stop
       endif
       print*,'Calling  DGETRI'
       call DGETRI(N,matrix,N,IPIV,WORK,lwork,INFO)
       print*,'Done',info
       deallocate(ipiv,work)
       !end inversion with LU factorization

    CASE(2)
       !inversion with SVD
       PRINT*,'INVERSION WITH SVD'
       Lwork=-1
       ALLOCATE (mat2(N,N),work(N),U(LDU,N2),S(N2),VT(LDVT,N2),iwork(8*N),stat=iostat)
       if (iostat /= 0) then
          print*,'Failed to allocate work arrays for matrix inversion'
          stop
       endif

       mat2=matrix


       call  DGESDD ('A', N, N2, mat2, LDA, S, U, LDU, VT, LDVT, WORK, LWORK, IWORK, INFO)


       Lwork=int(work(1))
       deallocate(work)
       allocate(work(lwork),stat=iostat)
       if (iostat /= 0) then
          print*,'Failed to allocate work arrays for matrix inversion'
          stop
       endif


       print*,'calling DGESVD'

       call  DGESDD ('A', N, N2, mat2, LDA, S, U, LDU, VT, LDVT, WORK, LWORK, IWORK, INFO)

       print*,'Done',info

       UT=transpose(U)
       deallocate(work)
       allocate(Sinv(N,N),V(N,N),UT(N,N),stat=iostat)
       if (iostat /=0) then
          print*,'Error Sinvm V, U matrices'
          stop
       endif
       !test SVD decomposition

       Sinv(:,:)=0.

       do i=1,N
          Sinv(i,i)=S(i)
       enddo

       mat2(:,:)=0.
       mat2=matmul(Sinv,VT)
       !call my_matmul_p(Sinv,VT,mat2)
       Sinv=mat2
       mat2=matmul(U,Sinv)
       !call my_matmul_p(U,Sinv,mat2)
       mat2=abs(mat2-matrix)
       prec=maxval(mat2)
       print*,'SVD precision=',prec
       if (prec > precision) stop
       !end test SVD decomposition

       V=transpose(VT)
       UT=transpose(U)
       deallocate(VT,U)

       Sinv(:,:)=0.

       do i=1,N
          Sinv(i,i)=1./S(i)
       enddo

       !        call my_matmul_p(Sinv,UT,matrix)
       matrix=matmul(Sinv,UT)
       UT=matrix
       !        call my_matmul_p(V,UT,matrix)
       matrix=matmul(V,UT)
       deallocate(mat2,V,UT,S,Sinv)
       ! end inversion with SVD
    END SELECT

  end subroutine minvert




subroutine minvert_old(matrix,n,np,fl)
! Inverts a matrix bu LU decompostion (see Num Rec., pp 34-40) 

      IMPLICIT none
      INTEGER n,np,fl,flag
      REAL(8) matrix(np,np),mat2(n,n),jcol(n),d
      INTEGER indx(n),i,j
      EXTERNAL ludcmp,lubksb

      fl = 0
      flag = 0
      Do i=1,n
         indx(i) = 0
         Do j=1,n
            mat2(i,j)=0.
         Enddo
         mat2(i,i)=1.
      Enddo

      Call ludcmpmlb(matrix,n,np,indx,d,flag)

      if (flag .eq. 0) then 
         Do j=1,n
            Do i=1,n
               jcol(i)=mat2(i,j)
            Enddo
            Call lubksbmlb(matrix,n,np,indx,jcol)
            Do i=1,n
               mat2(i,j)=jcol(i)
            Enddo
         Enddo
         Do i=1,n
            Do j=1,n
               matrix(i,j)=mat2(i,j) 
            Enddo
         Enddo
      else 
         fl = 1
      endif
      Return
    
end subroutine

!############################################################!                     

subroutine ludcmpmlb(a,n,np,indx,d,flag)

      INTEGER n,np,indx(n),NMAX
      REAL(8) d,a(np,np),TINY
      PARAMETER (NMAX=5000,TINY=1.0e-20)
      INTEGER i,imax,j,k,flag
      REAL(8) aamax,dum,sum,vv(NMAX)

      d=1.
      flag = 0  
      do 12 i=1,n
        aamax=0.
        do 11 j=1,n
           if (abs(a(i,j)).gt.aamax) aamax=abs(a(i,j))
11      continue
        if (aamax.eq.0.0) then
           flag = 1
           return
        endif
        vv(i)=1./aamax
12      continue
      do 19 j=1,n
        do 14 i=1,j-1
          sum=a(i,j)
          do 13 k=1,i-1
            sum=sum-a(i,k)*a(k,j)
13        continue
          a(i,j)=sum
14      continue
        aamax=0.
        do 16 i=j,n
          sum=a(i,j)
          do 15 k=1,j-1
            sum=sum-a(i,k)*a(k,j)
15        continue
          a(i,j)=sum
          dum=vv(i)*abs(sum)
          if (dum.ge.aamax) then
            imax=i
            aamax=dum
          endif
16      continue
        if (j.ne.imax)then
          do 17 k=1,n
            dum=a(imax,k)
            a(imax,k)=a(j,k)
            a(j,k)=dum
17        continue
          d=-d
          vv(imax)=vv(j)
        endif
        indx(j)=imax
        if (abs(a(j,j)) .eq. 0.d0) a(j,j) = TINY
        if(j.ne.n)then
          dum=1./a(j,j)
          do 18 i=j+1,n
            a(i,j)=a(i,j)*dum
18        continue
        endif
19    continue        
      return

end subroutine

!############################################################!                     

subroutine lubksbmlb(a,n,np,indx,b)

      INTEGER n,np,indx(n)
      REAL(8) a(np,np),b(n)
      INTEGER i,ii,j,ll
      REAL(8) sum
      ii=0
      do 12 i=1,n
        ll=indx(i)
        sum=b(ll)
        b(ll)=b(i)
        if (ii.ne.0)then
          do 11 j=ii,i-1
            sum=sum-a(i,j)*b(j)
11        continue
        else if (sum.ne.0.) then
          ii=i
        endif
        b(i)=sum
12    continue
      do 14 i=n,1,-1
        sum=b(i)
        do 13 j=i+1,n
          sum=sum-a(i,j)*b(j)
13      continue
        b(i)=sum/a(i,i)
14    continue
      return

end subroutine

!############################################################!                     

subroutine bandpass_window_function(nl,lowl,highl,M_eb,M_ee_bb,K_eb,K_ee_bb,W_eb,W_ee,W_bb,W_eebb,W_bbee,Fl_pp)
	! lmax is read from the input file directly
	implicit none
	real(8):: Fl_pp(:)
	real(8):: W_eb(:,:), W_ee(:,:), W_bb(:,:), W_eebb(:,:), W_bbee(:,:) 
	real(8):: K_eb(:,:), K_ee_bb(:,:)
	real(4):: M_eb(:,:), M_ee_bb(:,:) 
	integer(4),intent(in):: lowl(:), highl(:)
	real(8), allocatable :: P_bl(:,:)
	integer(4) :: i, j, l, k, kk
	integer(4) :: lmax, lmin, nbin, binflag, binsize,M_lmax
	real(8) :: ellmin, ellmax, dum
	integer(4) :: nl, flag, ltest, fl
	real(8) :: tot_eb,tot_ee,tot_bb,tot_eebb,tot_bbee

	nbin=size(highl)
	lmax = highl(nbin)

 allocate(P_bl(1:nbin, 1:nl))
 P_bl(:,:) = 0.0

	do i = 3, nl
	   do j = 1, nbin
		  P_bl(j,i) = 0.0
	   enddo
	   do j = 1, nbin
		  ltest = i - 1
		  if (ltest.ge.lowl(j).and.ltest.le.highl(j)) then
			 P_bl(j,i) = real(ltest) * (real(ltest)+1.d0) / (real(highl(j)) - real(lowl(j)) + 1.) / 2. / pi
		  endif
	   enddo
	enddo

	do i=1 , nbin ! this is b
		do j=3, nl ! this is l
			! set elements of the W matrices to 0
			W_eb(i,j) 	= 0.0
			W_ee(i,j) 	= 0.0
			W_bb(i,j) 	= 0.0
			W_eebb(i,j)	= 0.0
			W_bbee(i,j)	= 0.0
			
			do k=1, nbin ! this is b'
				
				tot_eb		=0.0
				tot_ee		=0.0
				tot_bb		=0.0
				tot_eebb	=0.0
				tot_bbee	=0.0
				
				do kk=3, nl ! this is l'
					
					! if i run after deconvolve spectrum, the K matrices will be already inverted
					tot_eb 		= tot_eb + P_bl(k,kk) * M_eb(kk,j)
					tot_ee 		= tot_ee + P_bl(k,kk) * M_ee_bb(kk,j)
					tot_bb 		= tot_bb + P_bl(k,kk) * M_ee_bb(kk+nl,j+nl)
					tot_eebb	= tot_eebb + P_bl(k,kk) * M_ee_bb(kk,j+nl)
					tot_bbee	= tot_bbee + P_bl(k,kk) * M_ee_bb(kk+nl,j)
									
				enddo
				
				W_eb(i,j) 	= W_eb(i,j) + (2. * pi) / (real(j-1)*real(j)) * Fl_pp(j) * K_eb(i,k) 			* tot_eb
				W_ee(i,j) 	= W_ee(i,j) + (2. * pi) / (real(j-1)*real(j)) * Fl_pp(j) * K_ee_bb(i,k) 			* tot_ee
				W_bb(i,j) 	= W_bb(i,j) + (2. * pi) / (real(j-1)*real(j)) * Fl_pp(j) * K_ee_bb(i+nbin,k+nbin)* tot_bb
				W_eebb(i,j)	= W_eebb(i,j) + (2. * pi) / (real(j-1)*real(j)) * Fl_pp(j) * K_ee_bb(i,k+nbin) 	* tot_eebb
				W_bbee(i,j)	= W_bbee(i,j) + (2. * pi) / (real(j-1)*real(j)) * Fl_pp(j) * K_ee_bb(i+nbin,k) 	* tot_bbee
				
			enddo
		enddo
	enddo
	deallocate(P_bl)
end subroutine

end module pcl2cl


