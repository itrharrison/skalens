pro plot_spectra

!p.multi = [0,2,2]
red = fsc_color("Red", !D.Table_Size-1)
!p.charsize = 1.5
 
; pixel window function
readcol, 'pixel_window_n0512.dat', dum, w_l_t, w_l_p

; predicted convolved theory spectra 
readcol, 'cl_predicted.dat', l_p, tt_p, ee_p, bb_p, te_p
tt_p = l_p * (l_p + 1.0) * tt_p / 2. / !pi * w_l_t^2.d0
ee_p = l_p * (l_p + 1.0) * ee_p / 2. / !pi * w_l_p^2.d0
bb_p = l_p * (l_p + 1.0) * bb_p / 2. / !pi * w_l_p^2.d0
te_p = l_p * (l_p + 1.0) * te_p / 2. / !pi * w_l_t * w_l_p

; spectra measured from the masked simulation
readcol, 'cl_masked_cmb.dat', tt_d, ee_d, bb_d, te_d, tb_d, eb_d
l_d = findgen(n_elements(tt_d))
tt_d = 1.e12 * l_d * (l_d + 1.0) * tt_d / 2. / !pi
ee_d = 1.e12 * l_d * (l_d + 1.0) * ee_d / 2. / !pi
bb_d = 1.e12 * l_d * (l_d + 1.0) * bb_d / 2. / !pi
te_d = 1.e12 * l_d * (l_d + 1.0) * te_d / 2. / !pi
tb_d = 1.e12 * l_d * (l_d + 1.0) * tb_d / 2. / !pi
eb_d = 1.e12 * l_d * (l_d + 1.0) * eb_d / 2. / !pi

plot_oo, l_d, tt_d, xtitle = 'multipole, l', ytitle = 'TT spectrum (l^2Cl / 2pi)', xrange=[2.,1000.], yrange=[400., 6000.], xsty=1., ysty=1.
oplot, l_p, tt_p, col=red, thick=2.

plot, /xlog, l_d, te_d, xtitle = 'multipole, l', ytitle = 'TE spectrum (l^2Cl / 2pi)', xrange=[2.,1000.], yrange=[-100., 120.], xsty=1., ysty=1.
oplot, l_p, te_p, col=red, thick=2.

plot_oo, l_d, ee_d, xtitle = 'multipole, l', ytitle = 'EE spectrum (l^2Cl / 2pi)', xrange=[2.,1000.], yrange=[1.e-3, 50.], xsty=1., ysty=1.
oplot, l_p, ee_p, col=red, thick=2.

plot_oo, l_d, bb_d, xtitle = 'multipole, l', ytitle = 'BB spectrum (l^2Cl / 2pi)', xrange=[2.,1000.], yrange=[1.e-3, 0.10], xsty=1., ysty=1.
oplot, l_p, bb_p, col=red, thick=2.

print, 'Enter 1 to view recovered true sky EE, BB and EB bandpowers' 
read, dum2

; input theory spectra
readcol, 'input_model/wmap5yr_r0.026_lensed_total_Cls.dat', l_th, tt_th, ee_th, bb_th, te_th
eb_th = fltarr(n_elements(tt_th))
eb_th(*) = 0.d0

; recovered bandpowers
readcol, 'cl_recovered_cmb.dat', lmin, lmax, ee_band, bb_band, eb_band
lval = (lmin + lmax) / 2.
ee_band = 1.e12 * ee_band
bb_band = 1.e12 * bb_band
eb_band = 1.e12 * eb_band

plot, l_th, ee_th, xtitle = 'multipole, l', ytitle = 'EE spectrum (l^2Cl /2pi)', xrange=[2.,1000.], yrange=[-3., 50.], xsty=1., ysty=1.
oplot, lval, ee_band, col=red, thick=2., psym=5.
plot, l_th, bb_th, xtitle = 'multipole, l', ytitle = 'BB spectrum (l^2Cl /2pi)', xrange=[2.,1000.], yrange=[-0.01, 0.1], xsty=1., ysty=1.
oplot, lval, bb_band, col=red, thick=2., psym=5.
plot, l_th, eb_th, xtitle = 'multipole, l', ytitle = 'EB spectrum (l^2Cl /2pi)', xrange=[2.,1000.], yrange=[-1.e-2, 1.e-2], xsty=1., ysty=1.
oplot, lval, eb_band, col=red, thick=2., psym=5.
stop
end
