!######################################################################!
!#                                                                    #!
!#  Mask deconvolution pipeline for lensing                           #!
!#  A Bonaldi 2015                                                    #!
!#  written using codes from M Brown                                  #!
!#                                                                    #! 
!#                                                                    #! 
!#                                                                    #!
!#######################i###############################################!

program specdec
  use coupling
  use binned_coupling
  use pcl2cl
  use master_io
  use healpix_types
  USE paramfile_io, ONLY : paramfile_handle, parse_init, parse_int, &
       parse_string, parse_double, parse_lgt, concatnl
  USE extension, ONLY : getEnvironment, getArgument, nArguments
  USE fitstools

  !###############################################S#######################!

  implicit none

  ! filenames
  !character(LEN=filenamelen) :: cl_file
  character(LEN=filenamelen),allocatable::mask_filenames(:),cl_filenames(:),pcl_filenames(:)
  character(LEN=filenamelen)::paramfile,description,dummy
  character(LEN=filenamelen)::filename,bin_filename,pixel_filename!,pcl_filename
  CHARACTER(LEN=5) :: output
  CHARACTER(LEN=80), DIMENSION(1:120) :: header
  character(25) :: fmt
  ! timing
  integer :: iexec, system
  character(100) :: command
  REAL(SP) :: clock_time
  INTEGER, DIMENSION(8,2) :: values_time

  ! parameters
  integer(4)::nmask,ncl,lmax,mask_choice,nel ! now set by the user

  ! binning params, window fns etc. 
  integer(4) :: i, j, l, k, kk, jj
  integer(4) :: imask, nl,iostat,nbin,ncolumns,nfiles,nmasks
  integer(4), allocatable :: lowl(:), highl(:)
  integer(4) ::ordering,nside,nmaps,max_lpol,npixtot,inv_mode ! healpix I/O 

  ! window fn pcls, transfer functions, coupling matrices
  real(8),parameter ::p=3.14159265358979323846264338327950288
  real(8) :: w_tt_in, w_pp_in, w_tp_in, l_in, fwhm,sigma
  real(8), allocatable :: ell(:), beam(:),w_tt(:,:), w_pp(:,:), w_tp(:,:)
  real(4), allocatable :: M_tt(:,:,:), M_te(:,:,:), M_eb(:,:,:), M_ee_bb(:,:,:)
  real(4), allocatable ::clin(:,:)
  real(8), allocatable :: ee_bb(:),eb(:),ee_true(:),bb_true(:),eb_true(:) 
  real(4), allocatable :: M_tt_out(:,:), M_te_out(:,:), M_eb_out(:,:), M_ee_bb_out(:,:) 
  real(8),allocatable :: data(:,:),pw_tt(:),pw_pp(:),pw_tp(:),Fl_pp(:)
  ! bandpower coupling matrices
  real(8), allocatable :: K_tt(:,:), K_te(:,:), K_eb(:,:), K_ee_bb(:,:),K_eb_save(:,:), K_ee_bb_save(:,:)
  ! bandpower windows functions
  real(8), allocatable :: W_eb(:,:), W_ee(:,:), W_bb(:,:), W_eebb(:,:), W_bbee(:,:)

  !user interface stuff
  CHARACTER(LEN=*), PARAMETER :: code ="SpecDec"

  TYPE(paramfile_handle) :: handle


  !######################################################################!

  if (nArguments() == 0) then
     paramfile=''
  else if (nArguments() == 1) then
     call getArgument(1,paramfile)
  else 
     print*, "Usage:  "//code//": [parameter file name]"
     stop
  endif

  ! timing
  command='date'
  call date_and_time(values = values_time(:,1))

  handle = parse_init(paramfile)


  description = concatnl( &
       & "Set number of spectra")
  nfiles = parse_int(handle, 'nfiles', default=1, vmin=1, descr=description)

  description = concatnl( &
       & "Choice for the mask: 0) same mask 1) different masks")
  mask_choice = parse_int(handle, 'mask_choice', default=1, vmin=0, vmax=1,descr=description)
  !if mask_choice is 0 the same mask is used for all spectra -> no need to recompute copuling kernel

  ! number of input mask cls
  description = concatnl( &
       & "Set ncl")
  ncl = parse_int(handle, 'ncl', default=1024, vmin=0, descr=description)

  ! maximum ell in coupling matrices
  description = concatnl( &
       & "Set lmax")
  lmax = parse_int(handle, 'lmax', default=1024, vmin=0, vmax=ncl,descr=description)

!!$  ! number of masks 
!!$  description = concatnl( &
!!$       & "nmask")
!!$  nmask = parse_int(handle, 'nmask', default=1, vmin=0, descr=description)
  nmask=1 !check if this will be always the case!

  allocate(mask_filenames(nfiles), cl_filenames(nfiles),pcl_filenames(nfiles))

  !if the mask is the same:

  do i=1,nfiles
     write(output,"(i3)")i
     output=ADJUSTL(output)
     l=LEN_TRIM(output)

     dummy = 'mask'//output(:l)
     description = concatnl( &
          & " Enter the name of the input file with mask cls")
     mask_filenames(i)=parse_string(handle,dummy, default='', descr=description)

     dummy = 'pcl_file'//output(:l)
     description = concatnl( &
          & " Enter the name of the input file with map cls")
     pcl_filenames(i)=parse_string(handle,dummy, default='', descr=description)

     dummy = 'cl_file'//output(:l)
     description = concatnl( &
          & " Enter the name of the ouput file with map cls")
     cl_filenames(i)=parse_string(handle,dummy, default='', descr=description)
  enddo

  description = concatnl( &
       & " Enter the name of the input file with binning scheme")
  bin_filename=parse_string(handle,'bin_file', default='', descr=description)

  description = concatnl( &
       & " Enter the beam FWHM in arcmin")
  fwhm=parse_double(handle,'fwhm', default=0.d0, descr=description)

  description = concatnl( &
       & " Enter the name of the input file with pixel window function")
  pixel_filename=parse_string(handle,'pixel_file', default='', descr=description)

  !######################################################################!

  !reading inputs

  ! get window fn cls (power spectra of mask):
!!$  allocate(w_tt(1:nmask, 1:ncl),w_tp(1:nmask, 1:ncl),stat=iostat)
!!$  if (iostat /=0) then
!!$     print*,'Error allocating maps'
!!$     stop
!!$  endif

  ! allocations and I/O to be done only once (for all pcl files):

  nl = lmax + 1
  allocate(M_eb(1:nmask, 1:nl, 1:nl),M_ee_bb(1:nmask, 1:2*nl, 1:2*nl),&
       &M_eb_out(1:nl, 1:nl),M_ee_bb_out(1:2*nl, 1:2*nl),stat=iostat)
  if (iostat /=0) then
     print*,'Error allocating coupling matrices'
     stop
  endif

  ncl=ncl+1 !from 0 to ncl
  if (nl .gt. ncl) then 
     nl=ncl
     print*,'reconstructiong only up to',nl
  endif

  allocate(ell(1:ncl),beam(1:ncl),w_pp(1:nmask, 1:ncl),eb(1:ncl),ee_bb(1:2*ncl),stat=iostat)
  if (iostat /=0) then
     print*,'Error allocating maps'
     stop
  endif



  
  !reading file with binning scheme
  nbin=rows_number(bin_filename,10)
  ncolumns=2
  allocate(data(nbin,ncolumns),lowl(1:nbin),highl(1:nbin),ee_true(1:nbin),&
       &bb_true(1:nbin),eb_true(1:nbin),stat=iostat)
  if (iostat /=0) then
     print*,'Error allocating maps'
     stop
  endif

  ! ...and for k-matrices
  allocate(K_eb(1:nbin, 1:nbin),K_eb_save(1:nbin, 1:nbin))
  allocate(K_ee_bb(1:2*nbin, 1:2*nbin),K_ee_bb_save(1:2*nbin, 1:2*nbin))
    
  ! allocate for BPWF matrices
  allocate(W_eb(1:nbin, 1:nl), W_ee(1:nbin, 1:nl) , W_bb(1:nbin, 1:nl) , W_eebb(1:nbin, 1:nl) , W_bbee(1:nbin, 1:nl))

  call read_columns(bin_filename,2,nbin,Ncolumns,data)
  lowl=int(data(:,1))
  highl=int(data(:,2))
  deallocate(data)

  ! reading pixel window function
  call get_info(pixel_filename,npixtot,nmaps,ordering,nside)


  allocate(data(npixtot,nmaps),pw_pp(1:ncl),Fl_pp(1:ncl),stat=iostat)
  if (iostat /=0) then
     print*,'Error allocating maps'
     stop
  endif

  call input_map(pixel_filename,data,npixtot,nmaps)
  !  pw_tt(:)=data(3:npixtot,1)!remove l=0 and l=1 elements:
  pw_pp(:) = 0.0
  nel=minval((/npixtot,ncl/))
  pw_pp(1:nel)=data(1:nel,2)
  deallocate(data)

  !  pw_tp=sqrt(pw_tt+pw_pp)
  sigma=fwhm/60.d0*p/180.d0/sqrt(8.*ALOG(2.))
  do i=1,ncl
     ell(i)=real(i)-1
     beam(i)=exp(-0.5*ell(i)*(ell(i)+1.)*sigma**2.)
  enddo
  if (fwhm ==0.) beam(:)=1.d0
  ! I define a new variable to not recycle the pixel window fn
  Fl_pp = pw_pp**2 * beam**2
  
  !here i will output the Fl function
!!$  OPEN(UNIT=3333, FILE="Fl.dat", ACTION="write", STATUS="replace") 
!!$  DO jj=1,SIZE(Fl_pp,1)
!!$		write(3333, '(1000F20.7)')( real(Fl_pp(jj)))
!!$  END DO 
!!$  close(unit=3333)
 
  if (mask_choice==0) then !coupling kernel computed only ince and stored
     allocate(clin(ncl,1))
     ! get the window fns
     ! do imask = 1, nmask
     !        filename=mask_filenames(imask)

     call get_info(mask_filenames(1),npixtot,nmaps,ordering,nside)
     if ((npixtot /=ncl) .or. (nmaps /= 1)) then 
        print*,'wrong format mask cl file'
        print*,'expected',ncl,1
        print*,'found',npixtot,nmaps
        stop
     endif
     
     call fits2cl(mask_filenames(1), clin, ncl-1, 1, header)

     do i=1,ncl
        w_pp(1, i) = (2 * ell(i) + 1) *  clin(i,1)        
     enddo

     ! I dont have w_tt and w_tb but I dont use them
     !end do

     deallocate(clin)
     print*, ' '
     write (*,'(a,i5,a)') ' Calculating coupling matrix up to lmax =', lmax, '...' 

     ! calculate coupling matrices:
     call compute_coupling_pol(w_pp,M_eb,M_ee_bb,ell,nl,nmask)
     print*,'Done'


     !this is just for one mask at the moment!
     do imask = 1, nmask
        M_eb_out(:, :) = M_eb(imask, :, :) 
        M_ee_bb_out(:, :) = M_ee_bb(imask, :, :) 
     end do

     Print*,'Binning coupling matrices...'
     call bin_coupling_pol(nl,Fl_pp,lowl,highl,M_eb_out,M_ee_bb_out,&
          &K_eb,K_ee_bb)
     print*,'Done'
     K_eb_save=K_eb
     K_ee_bb_save=K_ee_bb
     
     ! here i will output the K_ee_bb matrix
!!$     OPEN(UNIT=3333, FILE="Kbb_EE_BB.dat", ACTION="write", STATUS="replace") 
!!$     DO jj=1,SIZE(K_ee_bb,2)
!!$!		write(3333, '(1000F20.7)')( real(K_ee_bb(i,jj)) ,i=1,Size(K_ee_bb,1))
!!$        write(3333, *)( real(K_ee_bb(i,jj)) ,i=1,Size(K_ee_bb,1))
!!$
!!$	 END DO 
!!$	 close(unit=3333)
     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  endif

  do j=1,nfiles
     print*,'analysing spectrum N',j

     call get_info(pcl_filenames(j),npixtot,nmaps,ordering,nside)

     if ((npixtot /=ncl) .or. (nmaps /=6)) then
        print*,'error pcl input format!'
        print*,'expected',ncl,6
        print*,'found',npixtot,nmaps
        stop
     endif

     allocate(clin(1:ncl,1:6))

     call fits2cl(pcl_filenames(j), clin, ncl-1, 6, header)
 
     eb=clin(1:ncl,6)
     ee_bb(1:ncl)=clin(1:ncl,2)
     ee_bb(ncl+1:2*ncl)=clin(1:ncl,3)
     

     deallocate(clin)

     
     K_eb=K_eb_save  !initialization for mask_choice==0
     K_ee_bb=K_ee_bb_save

     if (mask_choice/=0) then !need to recompute couplig kernel each time
        allocate(clin(ncl,1))
    
        ! get the window fns
        ! do imask = 1, nmask
        !        filename=mask_filenames(imask)

        call get_info(mask_filenames(j),npixtot,nmaps,ordering,nside)
        if ((npixtot /=ncl) .or. (nmaps /= 1)) then
           print*,'wrong format mask cl file'
           print*,'expected',ncl,1
           print*,'found',npixtot,nmaps
           stop
        endif
        

        call fits2cl(mask_filenames(j), clin, ncl-1, 1, header)

        do i=1,ncl
           w_pp(1, i) = (2 * ell(i) + 1) *  clin(i,1)        
        enddo

        ! I dont have w_tt and w_tb but I dont use them
        !end do

        deallocate(clin)
        print*, ' '
        write (*,'(a,i5,a)') ' Calculating coupling matrix up to lmax =', lmax, '...' 

        ! calculate coupling matrices:
        call compute_coupling_pol(w_pp,M_eb,M_ee_bb,ell,nl,nmask)
        print*,'Done'

!carlos: M_eb and M_ee_bb and M_ll' for  for EB and EE,BB
!P_bl computed inside bin_coupling_pol but easy to recompute

        !100  continue
        !this is just for one mask at the moment!
        do imask = 1, nmask
           M_eb_out(:, :) = M_eb(imask, :, :) 
           M_ee_bb_out(:, :) = M_ee_bb(imask, :, :) 
        end do

        Print*,'Binning coupling matrices...'
        call bin_coupling_pol(nl,Fl_pp,lowl,highl,M_eb_out,M_ee_bb_out,&
             &K_eb,K_ee_bb)
        print*,'Done'
     endif
     print*,'qui'
     Print*,'Deconvolving spectrum...'
	 
inv_mode=1 !LU
!inv_mode=2 !SVD

	 call deconvolve_spectrum(ncl,nl,nbin,K_eb,K_ee_bb,ell,lowl,highl,ee_bb,&
          &eb,ee_true,bb_true,eb_true,inv_mode)

	!carlos: K_eb and K_ee_bb are K^-1 for EB and EE,BB

	! write deconvolved spectrum to file

     Print*,'Writing to file....'
     fmt = '(i4, i5, 3e16.5e2)'
     open(10, file = cl_filenames(j), status = 'unknown')
     do i = 1, nbin
        write (10,fmt) lowl(i), highl(i), ee_true(i), bb_true(i), eb_true(i)
     enddo
     close(10)   

  enddo
  
!!$  call bandpass_window_function(lmax,lowl,highl,M_eb_out,M_ee_bb_out,K_eb,K_ee_bb,W_eb,W_ee,W_bb,W_eebb,W_bbee,Fl_pp)
!!$  print *,'Bandpass window functions calculated'
!!$  
!!$  ! write bandpass window functions to files
!!$  print*,'writing BWF to files'
!!$  
!!$  OPEN(UNIT=3333, FILE="BPWF_EB.dat", ACTION="write", STATUS="replace") 
!!$  DO jj=1,SIZE(W_eb,2)
!!$		write(3333, '(1000F20.7)')( real(W_eb(i,jj)) ,i=1,Size(W_eb,1))
!!$  END DO 
!!$  close(unit=3333)
!!$ 
!!$  OPEN(UNIT=3333, FILE="BPWF_EE.dat", ACTION="write", STATUS="replace") 
!!$  DO jj=1,SIZE(W_ee,2)
!!$		write(3333, '(1000F20.7)')( real(W_ee(i,jj)) ,i=1,Size(W_ee,1))
!!$  END DO
!!$  close(unit=3333)
!!$ 
!!$  OPEN(UNIT=3333, FILE="BPWF_BB.dat", ACTION="write", STATUS="replace") 
!!$  DO jj=1,SIZE(W_bb,2)
!!$		write(3333, '(1000F20.7)')( real(W_bb(i,jj)) ,i=1,Size(W_bb,1))
!!$  END DO
!!$  close(unit=3333)
!!$  
!!$  OPEN(UNIT=3333, FILE="BPWF_EEBB.dat", ACTION="write", STATUS="replace") 
!!$  DO jj=1,SIZE(W_eebb,2)
!!$		write(3333, '(1000F20.7)')( real(W_eebb(i,jj)) ,i=1,Size(W_eebb,1))
!!$  END DO
!!$  close(unit=3333)
!!$  
!!$  OPEN(UNIT=3333, FILE="BPWF_BBEE.dat", ACTION="write", STATUS="replace") 
!!$  DO jj=1,SIZE(W_bbee,2)
!!$		write(3333, '(1000F20.7)')( real(W_bbee(i,jj)) ,i=1,Size(W_bbee,1))
!!$  END DO
!!$  close(unit=3333)

  deallocate(M_eb_out)
  deallocate(M_ee_bb_out)
  deallocate(ell,lowl,highl) 
  deallocate(K_eb)
  deallocate(K_eb_save)
  deallocate(K_ee_bb)
  deallocate(K_ee_bb_save)
  deallocate(pw_pp,beam,Fl_pp)   !  deallocate(pw_tt,pw_tp,pw_pp)
  deallocate(ee_true,bb_true,eb_true,ee_bb,eb)
  deallocate(M_eb)
  deallocate(M_ee_bb)
  deallocate(W_eb,W_ee,W_bb,W_eebb,W_bbee)

  !  deallocate(M_tt)
  !  deallocate(M_te)
  !  deallocate(M_tt_out)
  !  deallocate(M_te_out)
  call date_and_time(values = values_time(:,2))

  values_time(:,1) = values_time(:,2) - values_time(:,1)
  clock_time =  (  (values_time(3,1)*24 &
       &           + values_time(5,1))*60. &
       &           + values_time(6,1))*60. &
       &           + values_time(7,1) &
       &           + values_time(8,1)/1000.
  PRINT*,"Total clock time [m]:",clock_time/60.
  PRINT*,"               "//code//" > Normal completion"
end program specdec

!#################################################################!

