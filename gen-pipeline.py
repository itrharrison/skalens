import numpy as np
import pdb
import sys
import os
import ConfigParser
from time import gmtime, strftime

import pipeline_tools as pt

param_filename = sys.argv[1]

config = ConfigParser.ConfigParser()
config.read(param_filename)

if config.get('pipeline', 'tag') == 'time':
  subtime = strftime('%Y-%m-%d-%H%M%S', gmtime())
else:
  subtime = config.get('pipeline', 'tag')

expname = config.get('experiment', 'name')
if len(expname) > 20:
  expname = expname[:20]
  config.set('experiment', 'name', value=expname)
  print('WARNING:\n experiment name truncated to (20 chars): {0}'.format(expname))

template = open('launch_experiment.template').read()

root_dir = config.get('pipeline', 'root_dir')
run_name = config.get('experiment', 'name')+subtime
submission_filename = run_name+'.launch'
run_dir = root_dir+run_name+'/'
if not os.path.exists(run_dir):
  os.makedirs(run_dir)

sampler_template = open('lensSampler/samplerparams.template').read()
phoz_template = open('PhozSim/phozparams.template').read()
spectra_template = open('lensSpectra/spectraparams.template').read()
specdec_template = open('SpecDec/specdecparams.template').read()

sampler_input_filename = run_dir+'sampler_input.txt'
phoz_input_filename = run_dir+'phoz_input.p'
spectra_input_filename = run_dir+'spectra_input.txt'
specdec_input_filename = run_dir+'specdec_input.txt'

sampler_exe_filename = config.get('sampler', 'file')
phoz_exe_filename = config.get('phoz', 'file')
spectra_exe_filename = config.get('spectra', 'file')
specdec_exe_filename = config.get('specdec', 'file')

if config.getboolean('pipeline', 'dosampler'):
  sampler_cmd = './{0} {1}'.format(sampler_exe_filename.split('/')[-1],
                                   sampler_input_filename.split('/')[-1])
else:
  sampler_cmd = ''

if config.getboolean('pipeline', 'dophoz'):
  phoz_cmd = './{0} {1}'.format(phoz_exe_filename.split('/')[-1],
                                phoz_input_filename.split('/')[-1])
else:
  phoz_cmd = ''

if config.getboolean('pipeline', 'dospectra'):
  spectra_cmd = './{0} {1}'.format(spectra_exe_filename.split('/')[-1],
                                   spectra_input_filename.split('/')[-1])
else:
  spectra_cmd = ''

if config.getboolean('pipeline', 'dospecdec'):
  specdec_cmd = './{0} {1}'.format(specdec_exe_filename.split('/')[-1],
                                   specdec_input_filename.split('/')[-1])
else:
  specdec_cmd = ''

pt.genSamplerInput(config, sampler_template, sampler_input_filename)
pt.genPhozInput(config, phoz_template, phoz_input_filename)
pt.genSpectraInput(config, spectra_template, spectra_input_filename)
pt.genSpecDecInput(config, specdec_template, specdec_input_filename)

submission_file = template.format(ppn=config.getint('pipeline', 'processors_per_node'),
                                 wall_hours=config.getint('pipeline', 'wall_hours'),
                                 name=config.get('experiment', 'name'),
                                 n_realisations=config.getint('pipeline', 'n_realisations'),
                                 email=config.get('pipeline', 'email'),
                                 run_dir=run_dir,
                                 nzbins=config.getint('sampler', 'n_zbins'),
                                 map_dir=config.get('input', 'shear_map_dir'),
                                 sampler_input_filename=sampler_input_filename,
                                 phoz_input_filename=phoz_input_filename,
                                 spectra_input_filename=spectra_input_filename,
                                 specdec_input_filename=specdec_input_filename,
                                 sampler_exe_filename=sampler_exe_filename,
                                 phoz_exe_filename=phoz_exe_filename,
                                 spectra_exe_filename=spectra_exe_filename,
                                 specdec_exe_filename=specdec_exe_filename,
                                 sampler_cmd=sampler_cmd,
                                 phoz_cmd=phoz_cmd,
                                 spectra_cmd=spectra_cmd,
                                 specdec_cmd=specdec_cmd
                                 )

open(submission_filename, 'w').write(submission_file)
print(submission_filename)
