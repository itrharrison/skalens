import numpy as np

def kolmogorov_fwhm(lam, fried):
  '''Kolomogorov PSF FWHM
  '''
  retVar = 0.976*lam/fried
  return retVar

def fried(nu_2, r0=10.e3, nu_1=150.e6):
  '''Propogate Fried parameter between two different frequencies
  '''
  retVar = (pow(nu_2/nu_1, 6./5.)*r0)
  return retVar