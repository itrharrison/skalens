import numpy as np
import cPickle as pickle
import pdb
import ConfigParser
import itertools

def genPhozInput(config, template, filename):
  '''Generate a PhozSim input file.
  '''

  '''
  bins = config.getint('sampler', 'n_zbins')
  specz_fraction = config.getfloat('experiment', 'specz_fraction')
  photoz_sigma = config.getfloat('experiment', 'photoz_sigma')
  clobber = config.get('phoz', 'clobber')
  max_photo_z = config.get('experiment', 'max_photo_z')
  high_photoz_sigma = config.get('experiment', 'high_photoz_sigma')

  input_file = template.format(bins=bins,
                               specz_fraction=specz_fraction,
                               photoz_sigma=photoz_sigma,
                               max_photo_z=max_photo_z,
                               high_photoz_sigma=high_photoz_sigma,
                               clobber=clobber)

  open(filename, 'w').write(input_file)
  '''
  
  pickle.dump(config, open(filename, 'wb'))



def genSpecDecInput(config, template, filename):
  '''Generate a SpecDec input file.
  '''
  #mask_root = config.get('specdec', 'mask_dir')
  #cl_root = config.get('spectra', 'spectra_dir')
  #out_root = config.get('specdec', 'output_dir')
  
  n_spectra_bins = config.getint('spectra', 'n_spectra_bins')
  bins = range(1,n_spectra_bins+1)
  spectra_combs = list(itertools.combinations_with_replacement(bins, 2))
  n_files = len(spectra_combs)
  
  mask_list = ''
  pcl_list = ''
  spectra_list = ''
  
  for i,bins in enumerate(spectra_combs):
    mask_list+='mask{0}=cl_mask_{1}{2}_{3}.fits\n'.format(i+1, bins[1], bins[0], config.get('experiment', 'name'))
    pcl_list+='pcl_file{0}=cl_{1}{2}_{3}.fits\n'.format(i+1, bins[1], bins[0], config.get('experiment', 'name'))
    spectra_list+='cl_file{0}=cl_{1}{2}_{3}_dec.txt\n'.format(i+1, bins[1], bins[0], config.get('experiment', 'name'))
  
  if config.getboolean('spectra', 'mc'):
    for i in np.arange(n_spectra_bins):
      mask_list+='mask{0}=cl_mask_{1}{1}_{2}.fits\n'.format(n_files+i+1, i+1, config.get('experiment', 'name'))
      pcl_list+='pcl_file{0}=cl_noise_{1}{1}_{2}.fits\n'.format(n_files+i+1, i+1, config.get('experiment', 'name'))
      spectra_list+='cl_file{0}=cl_noise_{1}{1}_{2}_dec.txt\n'.format(n_files+i+1, i+1, config.get('experiment', 'name'))
    n_files = n_files+n_spectra_bins
  
  input_file = template.format(ncl=2*config.getint('spectra', 'nside_out'),
                              lmax=2*config.getint('spectra', 'nside_out'),
                              binfile=config.get('specdec', 'binfile'),
                              pixel_file=config.get('specdec', 'pixel_window_file'),
                              nfiles=n_files,
                              spectra_masks=mask_list,
                              spectra_pcl=pcl_list,
                              spectra_cl=spectra_list
                              )
  
  open(filename, 'w').write(input_file)

def genSpectraInput(config, template, filename):
  '''Generate a lensSpectra input file.
  '''
  
  # parse the spectra binning choices
  #spectra_bin_choices = config.get('spectra', 'binning_choices')
  #spectra_bin_choices = spectra_bin_choices.split(',')
  #spectra_bin_choices = np.asarray(spectra_bin_choices, dtype=int)

  spectra_bin_edges = config.get('spectra', 'spectra_bin_edges')
  spectra_bin_edges = spectra_bin_edges.split(',')
  spectra_bin_edges = np.asarray(spectra_bin_edges, dtype=float)

  nzbins_out = config.getint('spectra', 'n_spectra_bins')
  nzbins_in = config.getint('sampler', 'n_zbins') # ToDo: some error checking

  # parse the sampler locations
  #shear_map_root = config.get('spectra', 'shear_map_dir')
  #inp_cat_root = config.get('spectra', 'input_catalogue_dir')
  
  # create the list of bin choices, maps, catalogues
  #bin_choice_list = ''
  shear_map_list = ''
  cat_out_list = ''
  redshift_bin_list = ''

  for i in np.arange(1,nzbins_in+1):
    shear_map_list+='map{0}=zbin{0}_map.fits\n'.format(i)
    cat_out_list+='cat{0}=zbin{0}_cat.fits\n'.format(i)
  
  for i in np.arange(1,nzbins_out+1):
    redshift_bin_list+='minz{0}={1}\n'.format(i, spectra_bin_edges[i-1])
    redshift_bin_list+='maxz{0}={1}\n'.format(i, spectra_bin_edges[i])
    
  input_file = template.format(nzbins_in=config.getint('sampler', 'n_zbins'),
                              nzbins_out=config.getint('spectra', 'n_spectra_bins'),
                              nside_out=config.getint('spectra', 'nside_out'),
                              mc_choice=int(config.getboolean('spectra', 'mc')),
                              output_choice=config.getint('spectra', 'output'),
                              run_name=config.get('experiment', 'name'),
                              shear_maps=shear_map_list,
                              spectra_bin_edges=redshift_bin_list,
                              catalogues=cat_out_list
                              )
  
  # write the input file
  open(filename, 'w').write(input_file)
  
def genSamplerInput(config, template, filename):
  '''Generate a lensSampler input file.
  '''
  # parse the sampler bin edges
  sampler_bin_edges = config.get('sampler', 'sampler_bin_edges')
  sampler_bin_edges = sampler_bin_edges.split(',')
  sampler_bin_edges = np.asarray(sampler_bin_edges, dtype=float)
  nzbins = config.getint('sampler', 'n_zbins') # ToDo: some error checking
  
  # parse the sampler locations
  #out_cat_root = config.get('sampler', 'output_catalogue_dir')
  
  # create the list of bin edges, shear maps and output catalogues
  redshift_bin_list = ''
  shear_map_list = ''
  cat_out_list = ''
  for i in np.arange(1,nzbins+1):
    redshift_bin_list+='minz{0}={1}\n'.format(i, sampler_bin_edges[i-1])
    redshift_bin_list+='maxz{0}={1}\n'.format(i, sampler_bin_edges[i])
    shear_map_list+='map{0}=zbin_{0}.fits\n'.format(i)
    cat_out_list+='catout{0}=zbin{0}_cat.fits\n'.format(i)
  
  # work out the size cut from the psf size and cut
  size_cut = (config.getfloat('experiment', 'psf_size')*
              config.getfloat('experiment', 'size_cut'))
  
  # put into the template
  input_file = template.format(skads_file=config.get('input','skads_file'),
                               e_snr_file=config.get('input','e_snr_file'),
                               skads_size=config.getfloat('fudge','size'),
                               skads_count=config.getfloat('fudge','count'),
                               area=config.getfloat('experiment', 'area'),
                               sn_cut=config.getfloat('experiment', 'sn_cut'),
                               size_cut=size_cut,
                               srms=config.getfloat('experiment', 'sigma_noise'),
                               freq=config.getfloat('experiment', 'frequency'),
                               n_redshift_bins=nzbins,
                               nside_out=config.getint('spectra', 'nside_out'),
                               shear_maps=shear_map_list,
                               redshift_bin_edges=redshift_bin_list,
                               ellipticity_catalogues=cat_out_list
                               )

  # write the input file
  open(filename, 'w').write(input_file)
