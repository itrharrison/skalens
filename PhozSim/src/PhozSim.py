#!/usr/bin/env python
import numpy as np
import sys
import glob
import pdb
import cPickle as pickle
from astropy.io import fits
import ConfigParser

def make_nofzs_from_bins(config, obs=False, normed=True, nhistbins=20):

  nzbinsin = config.getint('sampler', 'n_zbins')
  nzbinsout = config.getint('spectra', 'n_spectra_bins')

  bin_zs = np.array([])
  
  if obs:
    fname = 'zbin{0}_cat_obs.fits'
  else:
    fname = 'zbin{0}_cat.fits'

  for i in np.arange(1,nzbinsin+1):
      dat = fits.open('zbin{0}_cat.fits'.format(i))
      z = dat[1].data['OBSREDSHIFT']
      #if i < 7:
      #    first_bin_zs = np.append(first_bin_zs, z)
      #else:
      bin_zs = np.append(bin_zs, z)

  #bin_zs = [first_bin_zs] + bin_zs

  out_bin_edges = config.get('spectra', 'spectra_bin_edges')
  out_bin_edges = out_bin_edges.split(',')
  out_bin_edges = np.asarray(out_bin_edges, dtype=float)

  bindex = np.digitize(bin_zs, out_bin_edges)

  counts = np.ones([nzbinsout, nhistbins])
  bins = np.ones([nzbinsout, nhistbins+1])
  med_arr = np.zeros(nhistbins+1)

  for i in np.arange(1,nzbinsout+1):

    '''
    counts[i], bins[i], _ = hist(bin_zs[i], normed=normed, bins=nhistbins)
    print counts.sum()
    np.savetxt('nofz_bin{0}.txt'.format(i), np.column_stack([bins[i], counts[i]))
    '''
    med_arr[i-1] = np.median(bin_zs[bindex==i])
    counts[i-1], bins[i-1] = np.histogram(bin_zs[bindex==i], normed=normed, bins=nhistbins)
    np.savetxt('nofz_bin{0}.txt'.format(i),
               np.column_stack([bins[i-1], np.append(0, counts[i-1])]),
               header='{0}'.format(nhistbins+1),
               comments='')

  np.savetxt('nofz_medians_allbins.txt', med_arr)

def add_photo_zs(config):

  bins = np.arange(1,config.getint('sampler', 'n_zbins')+1)
  overwrite = config.get('phoz', 'clobber')
  high_z = config.getfloat('experiment', 'max_photo_z')

  for i in bins:
    catfile_root = 'zbin{0}_cat*.fits'.format(i)
    parts = glob.glob(catfile_root)
    for catfile in parts:
    #catfile = 'zbin{0}_cat.fits'.format(i)
      tb = fits.open(catfile)
      header = tb[-1].header
      data = tb[-1].data
      nrows = header['NAXIS2']
      z_arr = data['REDSHIFT']
      z_obs = np.ones_like(z_arr)*-1

      no_spec_z = np.random.uniform(size=nrows) > config.getfloat('experiment', 'specz_fraction')
      photoz_err = np.random.normal(scale=(config.getfloat('experiment', 'photoz_sigma')*(1.e0 + z_arr)))
      high_photoz_err = np.random.normal(scale=(config.getfloat('experiment', 'high_photoz_sigma')*(1.e0 + z_arr)))
      
      lowz_sample = z_arr<high_z
      highz_sample = ~lowz_sample
      
      z_obs[lowz_sample] = z_arr[lowz_sample] + no_spec_z[lowz_sample]*photoz_err[lowz_sample]
      
      z_obs[highz_sample] = z_arr[highz_sample] + high_photoz_err[highz_sample]
      
      #pdb.set_trace()

      while ((z_obs < 0).sum() > 0):
        unphys_z = z_obs < 0
        
        z_obs[lowz_sample*unphys_z] = z_arr[lowz_sample*unphys_z] + np.random.normal(scale=(config.getfloat('experiment', 'photoz_sigma')*(1.e0 + z_arr[lowz_sample*unphys_z])))
        z_obs[highz_sample*unphys_z] = z_arr[highz_sample*unphys_z] + np.random.normal(scale=(config.getfloat('experiment', 'high_photoz_sigma')*(1.e0 + z_arr[highz_sample*unphys_z])))

      while ((z_obs > 10).sum() > 0):
        unphys_z = z_obs > 10
        
        z_obs[lowz_sample*unphys_z] = z_arr[lowz_sample*unphys_z] + np.random.normal(scale=(config.getfloat('experiment', 'photoz_sigma')*(1.e0 + z_arr[lowz_sample*unphys_z])))
        z_obs[highz_sample*unphys_z] = z_arr[highz_sample*unphys_z] + np.random.normal(scale=(config.getfloat('experiment', 'high_photoz_sigma')*(1.e0 + z_arr[highz_sample*unphys_z])))
        
      #pdb.set_trace()

      cols = []
      cols.append(fits.Column(name='OBSREDSHIFT', format='E', array=z_obs))
      cols.append(fits.Column(name='SPECZFLAG', format='L', array=~no_spec_z))
      orig_cols = data.columns
      new_cols = fits.ColDefs(cols)
      hdu = fits.BinTableHDU.from_columns(orig_cols[:9] + new_cols)
      if overwrite:
        #hdu.writeto('zbin{0}_cat.fits'.format(i), clobber=True)
        hdu.writeto(catfile, clobber=True)
      else:
        #hdu.writeto('zbin{0}_cat_obs.fits'.format(i), clobber=True)
        hdu.writeto(obs+catfile, clobber=True)

if __name__ == '__main__':

  param_filename = sys.argv[1]

  '''
  config = ConfigParser.ConfigParser()
  config.read(param_filename)
  '''

  config = pickle.load(open(param_filename, 'rb'))

  add_photo_zs(config)
  #make_nofzs_from_bins(config)