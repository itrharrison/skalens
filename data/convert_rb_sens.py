import numpy as np

f = open('2dcs.txt')

freq_arr = np.array([], dtype=float)
fwhm_arr = np.array([], dtype=float)
srms_arr = np.array([], dtype=float)

for line in f:
  line = line.replace('*', '1.e35')
  line = line.split()
  
  # skip over comments
  if line[0] == '#':
    continue

  # add the frequency if we are on a new one
  if len(line) == 3:
    freq_arr = np.append(freq_arr, float(line[0]))
  else:
    freq_arr = np.append(freq_arr, freq_arr[-1])

  # add the fwhm and srms
  fwhm_arr = np.append(fwhm_arr, float(line[-2]))
  srms_arr = np.append(srms_arr, float(line[-1]))

f.close()

# save in the same format as the old one -- note swapped columns!
savdata = np.column_stack([fwhm_arr, freq_arr, srms_arr])
np.savetxt('rebaselined_SKA1_MID_Cont_Survey_Sensitivity.txt', savdata)