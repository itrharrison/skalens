#!/usr/bin/python
"""
SKA1-MID sensitivity plot, based on 'sensaswbx.mon'.
 -- Phil Bull, Sept. 2015
"""
import numpy as np
import pylab as P
import matplotlib.ticker
import scipy.interpolate

kb = 1.38e-16/1.e4/1.e-23 # Boltzmann constant

# Frequency coverage
frq = np.linspace(0.35, 20., 4000)
lfrq = np.log10(frq)

# SKA1-MID parameters
sdeg = 1. # FOV in sq. deg.? (PJB: Not sure...)
nant = 1. # No. of antennae (1 == A_eff/T_sys per antenna)
stad = 15. # Dish diameter [m]

# Define T_recv piecewise, in each band (bands 1,5 are defined in two pieces)
trcvb1a = 17. + 3.*(frq-0.35)/(1.05-0.35)
trcvb1b = 17. + 3.*(frq-0.35)/(1.05-0.35)
trcvb2 = 8.2 + 0.7*(frq-0.95)/(1.76-0.95)
trcvb3 = 10.6 + 1.5*(frq-1.65)/(3.05-1.65)
trcvb4 = 14.3 + 2.4*(frq-2.8)/(5.18-2.8)
trcvb5 = 16.7 + 6.1*(frq-4.6)/(13.8-4.6)
trcvb5b = 17. + 6.*(frq-4.6)/(24.-4.6)
tsky = 20. * (0.408/frq)**2.75 + 2.73 \
     + 288. * ( 0.005 + 0.1314 * np.exp((lfrq-np.log10(22.23))*8.) ) # T_sky
tspl = 4.0 # T_spillover?
tgnd = 300. # T_ground?

# Aperture efficiency as a fn. of frequency
etaa0 = 0.92
etaa = etaa0 - 70.*((3.e8/(frq*1.e9))/stad)**2.
etaa = etaa - 0.36*(np.abs(frq-1.6)/(24.-1.6))**0.6

# Band boundaries (GHz)
frb1alo = 0.35
frb1ahi = 0.58
frb1blo = 0.58
frb1bhi = 1.05
frb2lo = 0.95
frb2hi = 1.76
frb3lo = 1.65
frb3hi = 3.05
frb4lo = 2.8
frb4hi = 4.6
frb5lo = 4.6
frbd5hi = 13.8
frb5hi = 26.

# Initialise SEFD, FOV, T_recv arrays
sefd = 1e6 * np.ones(lfrq.shape)
fov = np.ones(lfrq.shape)
trcv = np.ones(lfrq.shape)

# Calculate piecewise A_eff / T_sys curve
# (N.B. Ordering seems to have been chosen to take the largest value of A/T when 
# two bands overlap)
bands = [
  (frb1ahi, frb1alo, trcvb1a),
  (frb1bhi, frb1blo, trcvb1b),
  (frb5hi, frb5lo, trcvb5b),
  (frb4hi, frb4lo, trcvb4),
  (frb3hi, frb3lo, trcvb3),
  (frb2hi, frb2lo, trcvb2),
]
trcv_bands = []
for fhi, flo, _trcv in bands:
    idx = np.where(np.logical_and(frq < fhi, frq >= flo))
    trcv[idx] = _trcv[idx] # Overall T_recv
    
    # Get per-band T_recv curve
    trcv_band = np.nan * np.ones(trcv.shape) # Should make A/T -> 0 out of band
    trcv_band[idx] = _trcv[idx]
    trcv_bands.append(trcv_band)
trcv_bands = np.array(trcv_bands) # Array containing T_rcv separately, for each band

# Calculate T_sys, A_eff, SEFD across all bands
tsys = trcv + tsky + tspl
aeff = nant * etaa * np.pi * stad**2./4.
sefd = 2.*kb*tsys/aeff

# Calculate FOV, A/T, survey speed
fovd = 2340. * ((3.e8/(frq*1.e9))/stad)**2.
aont = 2.*kb/sefd/sdeg
surv = aont**2*fovd

# Do the same for the separate bands
aot_bands = []
for i in range(trcv_bands.shape[0]):
    _tsys = trcv_bands[i] + tsky + tspl
    _aeff = nant * etaa * np.pi * stad**2./4.
    aot_bands.append( _aeff / _tsys )
aot_bands = np.array(aot_bands) # per-band A_eff / T_sys

# Construct interpolation function for A/T (takes freq. argument in MHz)
interp_aont = scipy.interpolate.interp1d(frq*1e3, aont, kind='linear', 
                                         bounds_error=False)

print interp_aont(1000.)

# Plot results
ff = np.logspace(np.log10(350.), np.log10(20e3), 1000)
P.subplot(111)

# Combined curve
P.plot(ff, interp_aont(ff), 'k-', lw=1.5)

# Per-band
for i in range(trcv_bands.shape[0]):
    P.plot(frq*1e3, aot_bands[i], 'y--', lw=1.5)

P.xlabel("Freq. [MHz]", fontsize=18)
P.ylabel(r"$A_{\rm eff} / T_{\rm sys}$", fontsize=18)

P.xscale('log')
P.xlim((300., 30e3))
P.tight_layout()
P.show()
