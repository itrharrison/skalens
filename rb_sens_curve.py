import numpy as np
from scipy import interpolate

def find_nearest(array,value):
    idx = (np.abs(array-value)).argmin()
    return array[idx], idx

def rescaleSrms(srms, survey_area):
  '''Rescale Robert Braun's sensitivity curve from a 3pi sky survey to same
  hours on given survey area.
  '''

  retVar = np.sqrt(survey_area/30939.78668)*srms

  return retVar

def createSensitivityCurve(filename='SKA1_MID_Cont_Survey_Sensitivity.txt'):
  '''Create a function to interpolate Srms values from the tabulated values
  provided by Robert Braun.

  Parameters
  ----------
  filename : string
    File containing tabulated values of fwhm, nu, Srms

  Returns
  -------
  f : function
    Interpolating function, to be called as
    f(ln(fwhm in arcsec), ln(frequency in GHz))
    which returns ln(ln(ln(Srms)))

  '''

  fwhm, nu, sens = np.loadtxt(filename, unpack=True)

  xl = np.log(fwhm)
  x = np.unique(xl)

  yl = np.log(nu)
  y = np.unique(yl)

  xx, yy = np.meshgrid(x, y)

  z = np.log(np.log(np.log(sens)))
  z = z.reshape(xx.shape)

  retFun= interpolate.interp2d(x, y, z, kind='cubic')

  return retFun, x, y, z