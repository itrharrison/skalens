import numpy as np
import cPickle as pickle
import os
from matplotlib import pyplot as plt
plt.close('all')

from astropy.constants import c
from astropy import units as uns

import kolmogorov_psf as kpsf
import rb_sens_curve as scurve

lowpsf = np.array([  1.76870694e+00,   1.76870694e+00,   1.76870694e+00,\
         2.75000000e+00,   4.00000000e+00,   1.17996416e+00,\
         1.17996416e+00,   1.50000000e+00,   2.75000000e+00,\
         4.00000000e+00,   5.91221384e-01,   5.91221384e-01,\
         1.50000000e+00,   2.75000000e+00,   4.00000000e+00,\
         2.47860794e-03,   2.50000000e-01,   1.50000000e+00,\
         2.75000000e+00,   4.00000000e+00,  -5.86264168e-01,\
         2.50000000e-01,   1.50000000e+00,   2.75000000e+00,\
         4.00000000e+00,  -3.66080560e-01,   2.50000000e-01,\
         1.50000000e+00,   2.75000000e+00,   4.00000000e+00,\
        -7.05214467e-01,   2.50000000e-01,   1.50000000e+00,\
         2.75000000e+00,   4.00000000e+00,  -1.00000000e+00,\
         2.50000000e-01,   1.50000000e+00,   2.75000000e+00,\
         4.00000000e+00,  -1.00000000e+00,   2.50000000e-01,\
         1.50000000e+00,   2.75000000e+00,   4.00000000e+00,\
        -1.00000000e+00,   2.50000000e-01,   1.50000000e+00,\
         2.75000000e+00,   4.00000000e+00])

lowfeqs = np.array([-1.02165125, -1.02165125, -1.02165125, -1.02165125, -1.02165125,\
       -0.75404089, -0.75404089, -0.75404089, -0.75404089, -0.75404089,\
       -0.48643054, -0.48643054, -0.48643054, -0.48643054, -0.48643054,\
       -0.21882019, -0.21882019, -0.21882019, -0.21882019, -0.21882019,\
        0.04879016,  0.04879016,  0.04879016,  0.04879016,  0.04879016,\
       -0.05129329, -0.05129329, -0.05129329, -0.05129329, -0.05129329,\
        0.10285848,  0.10285848,  0.10285848,  0.10285848,  0.10285848,\
        0.25701026,  0.25701026,  0.25701026,  0.25701026,  0.25701026,\
        0.41116203,  0.41116203,  0.41116203,  0.41116203,  0.41116203,\
        0.56531381,  0.56531381,  0.56531381,  0.56531381,  0.56531381])

lownoise = np.array([ 2.11070559,  2.11070559,  2.11070559,  2.09606581,  1.84793191,\
        1.97529633,  1.97529633,  1.97285426,  1.91543681,  1.78165963,\
        1.80809271,  1.80809271,  1.77985029,  1.67062872,  1.60299715,\
        2.06550114,  2.11941555,  2.09160214,  1.90596182,  1.87800433,\
        1.4814666 ,  1.44069443,  1.43893669,  1.29755049,  1.30293906,\
        1.36292395,  1.41396662,  1.40686503,  1.26334431,  1.25992382,\
        1.62425754,  1.47759632,  1.47645944,  1.33254986,  1.34393173,\
        2.3304029 ,  1.58697778,  1.57675073,  1.4343279 ,  1.47071057,\
        1.86768249,  1.71635667,  1.68251319,  1.55097675,  1.63355629,\
        2.22805587,  2.27459993,  2.18964979,  2.05186767,  2.25354726])

koopmans_r0 = 10.e3
koopmans_nu = 150.e6
nfreq = 5
npsf = 5
f_sky = 5000.e0

# define the frequency bands
band1 = [0.360, 1.050]
band2 = [0.950, 1.760]

b1_width = abs(band1[1] - band1[0])
b2_width = abs(band2[1] - band2[0])
b1_centre = np.log(np.mean(band1))
b2_centre = np.log(np.mean(band2))

# span the bands with a grid
#b1_nus = np.linspace(band1[0], band1[1], nfreq)*1.e9
#b2_nus = np.linspace(band2[0], band2[1], nfreq)*1.e9
b1_nus = np.logspace(np.log(band1[0]), np.log(band1[1]), nfreq, base=np.e)*1.e9
b2_nus = np.logspace(np.log(band2[0]), np.log(band2[1]), nfreq, base=np.e)*1.e9
b1_lam = c.value/b1_nus
b2_lam = c.value/b2_nus

# find the fried parameter at these frequencies
b1_fried = kpsf.fried(b1_nus, r0=koopmans_r0, nu_1=koopmans_nu)
b2_fried = kpsf.fried(b2_nus, r0=koopmans_r0, nu_1=koopmans_nu)

# find the kolmogorov psf fwhm at these frequencies
b1_fwhm = kpsf.kolmogorov_fwhm(b1_lam, b1_fried)*uns.rad
b2_fwhm = kpsf.kolmogorov_fwhm(b2_lam, b2_fried)*uns.rad
b1_min_fwhm = np.log(b1_fwhm.to(uns.arcsec).value)
b2_min_fwhm = np.log(b2_fwhm.to(uns.arcsec).value)

# span a 'reasonable' PSF range with a grid
b1_psf_arr = np.linspace(-1, 4, npsf)
b2_psf_arr = np.linspace(-1, 4, npsf)

b1_psf, b1_min = np.meshgrid(b1_psf_arr, b1_min_fwhm)
b2_psf, b2_min = np.meshgrid(b2_psf_arr, b2_min_fwhm)

b1_pp, b1_ff = np.meshgrid(b1_psf_arr, np.log(b1_nus/1.e9))
b2_pp, b2_ff = np.meshgrid(b2_psf_arr, np.log(b2_nus/1.e9))

plt.figure(1)
plt.plot(b1_pp.flatten(), b1_ff.flatten(), 'bo')
plt.plot(b2_pp.flatten(), b2_ff.flatten(), 'bo')

# if psf size is below ionosphere psf, set to ionosphere psf
for i in np.arange(npsf):
  for j in np.arange(nfreq):
    if b1_psf[i,j] < b1_min[i,j]:
      b1_pp[i,j] = b1_min[i,j]
    if b2_psf[i,j] < b2_min[i,j]:
      b2_pp[i,j] = b2_min[i,j]

plt.figure(1)
plt.plot(b1_pp.flatten(), b1_ff.flatten(), 'r+')
plt.plot(b2_pp.flatten(), b2_ff.flatten(), 'r+')
plt.xlabel('$\ln{\\theta_{\mathrm{PSF}}} \, \mathrm{[arcsec]}$')
plt.ylabel('$\ln{\\nu} \, \mathrm{[GHz]}$')
#plt.show()

# generate a RB sensitivity curve
rb_curve_prere, x_prere, y_prere, z_prere = scurve.createSensitivityCurve('data/SKA1_MID_Cont_Survey_Sensitivity.txt')
rb_curve, x, y, z = scurve.createSensitivityCurve('data/rebaselined_SKA1_MID_Cont_Survey_Sensitivity.txt')

b1_srms = np.ones_like(b1_pp.flatten())
b2_srms = np.ones_like(b2_pp.flatten())

b1_psfs = b1_pp.flatten()
b2_psfs = b2_pp.flatten()
b1_fs = b1_ff.flatten()
b2_fs = b2_ff.flatten()

# find the noise levels for each experiment and rescale them to the 5000deg2 area
for i in np.arange(b1_srms.shape[0]):
  a = np.exp(np.exp(np.exp(rb_curve(b1_psfs[i], b1_fs[i]))))
  b1_srms[i] = scurve.rescaleSrms(a, f_sky)
  b = np.exp(np.exp(np.exp(rb_curve(b2_psfs[i], b2_fs[i]))))
  b2_srms[i] = scurve.rescaleSrms(b, f_sky)


psf_sizes = np.exp(b1_pp).flatten()
psf_sizes = np.append(psf_sizes, np.exp(b2_pp).flatten())
obs_frequencies = np.exp(b1_ff).flatten()
obs_frequencies = np.append(obs_frequencies, np.exp(b2_ff).flatten())
noise_levels = b1_srms.flatten()
noise_levels = np.append(noise_levels, b2_srms.flatten())

# write out the experiments to a file
outfile = open('experiments_to_run_rebaselined.txt', 'w')
np.savetxt(outfile, np.column_stack(['psf [arcsec]', 'nu [ghz]', 'srms [mujy]']), fmt='%s')
np.savetxt(outfile, np.column_stack([psf_sizes, obs_frequencies, noise_levels]), fmt='%.3e')
outfile.close()

xnew = np.linspace(x.min(),x.max(),512)
ynew = np.linspace(y.min(),y.max(),512)

xxn, yyn = np.meshgrid(xnew, ynew)

Srms_surface = rb_curve(xnew, ynew)
Srms_surface_prere = rb_curve_prere(xnew, ynew)

nu_arr = np.exp(ynew)*1.e9
lam_arr = 3.e8/nu_arr
r0_arr = kpsf.fried(nu_arr)

psfnew = kpsf.kolmogorov_fwhm(lam_arr, r0_arr)*uns.rad
psfnew = psfnew.to(uns.arcsec).value
psfnx, psfny = np.meshgrid(psfnew, psfnew)

mask = psfny < np.exp(xxn)

b1_min = band1[0] + b1_width/3
b1_max = band1[1] - b1_width/3
b2_min = band2[0] + b2_width/3
b2_max = band2[1] - b2_width/3

b1_mask = mask*(yyn > np.log(b1_min))*(yyn < np.log(b1_max))
b2_mask = mask*(yyn > np.log(b2_min))*(yyn < np.log(b2_max))

plt.figure(figsize=(15,3.75))
b1_rect = plt.Rectangle((x.min(), np.log(band1[0])),
                      (x.max() - x.min()), np.log(band1[1]) - np.log(band1[0]),
                      facecolor='r', alpha=0.25, edgecolor='0.5')

b2_rect = plt.Rectangle((x.min(), np.log(band2[0])),
                        (x.max() - x.min()), np.log(band2[1]) - np.log(band2[0]),
                        facecolor='g', alpha=0.25, edgecolor='0.5')
plt.subplot(131)
plt.scatter(xxn[mask>0], yyn[mask>0], c=(Srms_surface_prere[mask>0]),
            cmap=plt.get_cmap('gray'), lw=0)
#plt.scatter(np.log(psf_sizes), np.log(obs_frequencies), c=noise_levels, cmap=plt.get_cmap('cool'))
plt.xlim([x.min(), x.max()])
plt.ylim([y.min(), 1.3])
plt.xlabel('$\mathrm{PSF \, Size} \, \ln({\\theta_{\\rm FWHM}}) \, [\mathrm{arcsec}]$')
plt.gca().add_patch(b1_rect)
plt.text(x.min()+0.2, np.log(band1[1]-0.2), 'Band 1', rotation=90, fontsize=10)
plt.text(x.min()+0.2, np.log(band2[1]-0.2), 'Band 2', rotation=90, fontsize=10)
plt.gca().add_patch(b2_rect)
plt.ylabel('$\mathrm{Frequency} \, \ln({\\nu}) \, [\mathrm{GHz}]$')
cbar = plt.colorbar()
#cbar.set_label('$\ln(\ln(\ln(S_{\mathrm{RMS}}))) \, [\mathrm{Jy}]$')
#cbar.set_label('$S_{\mathrm{RMS}} \, [\mu\mathrm{Jy}]$')
plt.title('$\mathrm{Before\,Rebaselining}$')

plt.subplot(132)
plt.scatter(xxn[mask>0], yyn[mask>0], c=(Srms_surface[mask>0]),
            cmap=plt.get_cmap('gray'), lw=0)
#plt.scatter(np.log(psf_sizes), np.log(obs_frequencies), c=noise_levels, cmap=plt.get_cmap('cool'))
plt.xlim([x.min(), x.max()])
plt.ylim([y.min(), 1.3])
plt.xlabel('$\mathrm{PSF \, Size} \, \ln({\\theta_{\\rm FWHM}}) \, [\mathrm{arcsec}]$')
plt.gca().add_patch(b1_rect)
plt.text(x.min()+0.2, np.log(band1[1]-0.2), 'Band 1', rotation=90, fontsize=10)
plt.text(x.min()+0.2, np.log(band2[1]-0.2), 'Band 2', rotation=90, fontsize=10)
plt.gca().add_patch(b2_rect)
#plt.ylabel('$\mathrm{Frequency} \, \ln({\\nu}) \, [\mathrm{GHz}]$')
cbar = plt.colorbar()
#cbar.set_label('$\ln(\ln(\ln(S_{\mathrm{RMS}}))) \, [\mathrm{Jy}]$')
#cbar.set_label('$S_{\mathrm{RMS}} \, [\mu\mathrm{Jy}]$')
plt.title('$\mathrm{After\,Rebaselining}$')

plt.subplot(133)
plt.scatter(xxn[mask>0], yyn[mask>0], c=((Srms_surface[mask>0])-(Srms_surface_prere[mask>0])),
            cmap=plt.get_cmap('bwr'), lw=0)
plt.scatter(np.log(psf_sizes), np.log(obs_frequencies), c=noise_levels, cmap=plt.get_cmap('cool'))
#plt.scatter(lowpsf, lowfeqs, c=lownoise, cmap=plt.get_cmap('cool'))
plt.xlim([x.min(), x.max()])
plt.ylim([y.min(), 1.3])
plt.xlabel('$\mathrm{PSF \, Size} \, \ln({\\theta_{\\rm FWHM}}) \, [\mathrm{arcsec}]$')
#plt.gca().add_patch(b1_rect)
#plt.text(x.min()+0.2, np.log(band1[1]-0.2), 'Band 1', rotation=90, fontsize=10)
#plt.text(x.min()+0.2, np.log(band2[1]-0.2), 'Band 2', rotation=90, fontsize=10)
#plt.gca().add_patch(b2_rect)
#plt.ylabel('$\mathrm{Frequency} \, \ln({\\nu}) \, [\mathrm{GHz}]$')
cbar = plt.colorbar()
#cbar.set_label('$\ln(\ln(\ln(S_{\mathrm{RMS}}))) \, [\mathrm{Jy}]$')
cbar.set_label('$S_{\mathrm{RMS}} \, [\mu\mathrm{Jy}]$')
plt.title('$\mathrm{Difference}$')
plt.savefig('sens_surface_difference.png', bbox_inches='tight', dpi=160)
#for i in np.arange(1,psf_sizes.shape[0]):
for i in np.arange(1,50):
  template = open('parameters.template').read()
  name = ('rb_psf_%.2e-nu_%.2e' % (psf_sizes[i], obs_frequencies[i]))
  params_filename = 'launch/'+name+'_params.ini'
  params_file = template.format(name=name,
                                srms=noise_levels[i],
                                nu=obs_frequencies[i],
                                psf=psf_sizes[i])
  open(params_filename, 'w').write(params_file)
  cmd = 'python gen-pipeline.py {0}'.format(params_filename)
  os.system(cmd)
