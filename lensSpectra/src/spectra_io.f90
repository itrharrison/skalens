module spectra_io
  use healpix_types
  USE fitstools
  USE utilities
  USE pix_tools
  USE paramfile_io, ONLY : paramfile_handle, parse_init, parse_int, &
       parse_string, parse_double, parse_lgt, concatnl
contains

  subroutine get_info(filename,npixtot,nmaps,ordering,nsmax)
    USE healpix_types
    USE fitstools

    IMPLICIT NONE

    INTEGER(I4B) :: nmaps,ordering,nsmax,nside,mlpol,npixtot
    CHARACTER(LEN=filenamelen) :: filename

    npixtot=getsize_fits(filename,nmaps=nmaps,ordering=ordering,nside=nsmax,mlpol=mlpol)
  end subroutine get_info

  subroutine rows_catalogue(filename,Ncol,irow)
    !enquire length of catalogue file 
    !C     read and print data values from an ASCII or binary table

    integer status,unit,readwrite,blocksize,hdutype,ntable
    integer felem,nelems,nullj,diameter,nfound,irow,colnum
    real nulle,density
    character filename*200,nullstr*1,name*8
    character*16 ttype(Ncol),tform(Ncol),tunit(Ncol)!
    logical anynull


1   status=0

    !C     Get an unused Logical Unit Number to use to open the FITS file
2   call ftgiou(unit,status)

    !C     open the FITS file previously created by WRITEIMAGE
    !filename='ATESTFILEZ.FITS'
    readwrite=0
3   call ftopen(unit,filename,readwrite,blocksize,status)



    !C         read the TTYPEn keywords, which give the names of the columns
6   call ftgkns(unit,'TTYPE',1,Ncol,ttype,nfound,status)

    if (nfound==0) then 
       call ftmrhd(unit,1,hdutype,status) ! go to the next extension
       call ftgkns(unit,'TTYPE',1,Ncol,ttype,nfound,status)
    endif
    

    !       write(*,2000)ttype
    !2000   format(8x,3a10)

    !C         read the data, one row at a time, and print them out
    felem=1
    nelems=1
    nullstr=' '
    nullj=0
    nulle=0.
  
    status=0
    irow=1
    do while (status ==0)
       colnum=1
9      call ftgcve(unit,colnum,irow,felem,nelems,nulle,density,anynull,status)

       irow=irow+1
    end do
    irow=irow-2 ! number of elements in table
    print*,'Number of rows',irow

    status=0
    !C     close the file and free the unit number
10  call ftclos(unit, status)
    call ftfiou(unit, status)

    !C     check for any error, and if so print out error messages
11  if (status .gt. 0)call printerror(status)

!stop
  end subroutine rows_catalogue


  subroutine range_catalogue(filename,Ncol,minz,maxz)
    !enquire length of catalogue file 
    !C     read and print data values from an ASCII or binary table

    integer status,unit,readwrite,blocksize,hdutype,ntable
    integer felem,nelems,nullj,diameter,nfound,irow,colnum
    real nulle,density,minz,maxz
    character filename*200,nullstr*1,name*8
    character*16 ttype(Ncol),tform(Ncol),tunit(Ncol)!
    logical anynull


1   status=0

    !C     Get an unused Logical Unit Number to use to open the FITS file
2   call ftgiou(unit,status)

    !C     open the FITS file previously created by WRITEIMAGE
    !filename='ATESTFILEZ.FITS'
    readwrite=0
3   call ftopen(unit,filename,readwrite,blocksize,status)



    !C         read the TTYPEn keywords, which give the names of the columns
6   call ftgkns(unit,'TTYPE',1,Ncol,ttype,nfound,status)

    if (nfound==0) then 
       call ftmrhd(unit,1,hdutype,status) ! go to the next extension
       call ftgkns(unit,'TTYPE',1,Ncol,ttype,nfound,status)
    endif
!!$   status=0
!!$    irow=1
!!$call ftgcve(unit,colnum,irow,felem,nelems,nulle,density,anynull,status)
!!$print*,status
    !       write(*,2000)ttype
    !2000   format(8x,3a10)

    !C         read the data, one row at a time, and print them out
    felem=1
    nelems=1
    nullstr=' '
    nullj=0
    nulle=0.

    minz=1.e30 !inizialise
    maxz=-1.e30 !inizialise 

    colnum=1                      ! true redshift
    if (nfound>=10) colnum=10     ! observed redshift

    status=0
    irow=1
    do while (status ==0)

9      call ftgcve(unit,colnum,irow,felem,nelems,nulle,density,anynull,status)

       if (density < minz) minz=density
       if (density > maxz) maxz=density
       irow=irow+1
    end do
    irow=irow-2 ! number of elements in table
    print*,'Number of rows',irow

    status=0
    !C     close the file and free the unit number
10  call ftclos(unit, status)
    call ftfiou(unit, status)

    !C     check for any error, and if so print out error messages
11  if (status .gt. 0)call printerror(status)

    !stop
  end subroutine range_catalogue



  subroutine read_catalogue(filename,Ncol,Nrows,latitudes,longitudes,ellipticity1,ellipticity2,redshift1,redshift2)

    !C     read and print data values from an ASCII or binary table

    integer status,unit,readwrite,blocksize,hdutype,ntable
    integer felem,nelems,nullj,diameter,nfound,irow,colnum,Nrows
    real nulle,density,density2
    character filename*200,nullstr*1,name*8
    character*16 ttype(Ncol),tform(Ncol),tunit(Ncol)!
    logical anynull
    real(sp)::latitudes(:),longitudes(:),ellipticity1(:),ellipticity2(:),redshift1(:),redshift2(:)

1   status=0

    !C     Get an unused Logical Unit Number to use to open the FITS file
2   call ftgiou(unit,status)

    !C     open the FITS file previously created by WRITEIMAGE
    !filename='ATESTFILEZ.FITS'
    readwrite=0
3   call ftopen(unit,filename,readwrite,blocksize,status)

 

       !C         read the TTYPEn keywords, which give the names of the columns
6      call ftgkns(unit,'TTYPE',1,Ncol,ttype,nfound,status)
    if (nfound==0) then 
       call ftmrhd(unit,1,hdutype,status) ! go to the next extension
       call ftgkns(unit,'TTYPE',1,Ncol,ttype,nfound,status)
    endif

          felem=1
          nelems=1
          nullstr=' '
          nullj=0
          nulle=0.
          do irow=1,Nrows
             call ftgcve(unit,4,irow,felem,nelems,nulle,density,anynull,status)
             latitudes(irow)=density
             call ftgcve(unit,5,irow,felem,nelems,nulle,density,anynull,status)
             longitudes(irow)=density
!!$             call ftgcve(unit,6,irow,felem,nelems,nulle,density,anynull,status)
!!$             error1(irow)=density
!!$             call ftgcve(unit,7,irow,felem,nelems,nulle,density,anynull,status)
!!$             error2(irow)=density
             call ftgcve(unit,8,irow,felem,nelems,nulle,density,anynull,status)
             ellipticity1(irow)=density
             call ftgcve(unit,9,irow,felem,nelems,nulle,density,anynull,status)
             ellipticity2(irow)=density
             call ftgcve(unit,1,irow,felem,nelems,nulle,density,anynull,status)
             redshift1(irow)=density ! true redshift
             redshift2(irow)=density !measured redshift (no error)
             if (nfound >= 10) then
                call ftgcve(unit,10,irow,felem,nelems,nulle,density,anynull,status)

                redshift2(irow)=density ! measured redshift
             endif
          end do

    !C     close the file and free the unit number
10  call ftclos(unit, status)
    call ftfiou(unit, status)

    !C     check for any error, and if so print out error messages
11  if (status .gt. 0)call printerror(status)
  end subroutine read_catalogue


  function rows_number(filename,iunit)
    integer:: reason,iunit,nrows,rows_number
    character (LEN=200)::filename
    real(DP)::col

close(iunit)
    open (UNIT=iunit,file=filename,status='unknown',form="formatted")
    print*,filename
    nrows=1
    DO
       READ(iunit,*,IOSTAT=Reason)  col
       IF (Reason > 0)  THEN 
          print*,'Error in reading file'
          print*,filename
          stop
       ELSE IF (Reason < 0) THEN
          exit
       ELSE
          nrows=nrows+1
       END IF
    END DO
   rows_number=nrows-1
    CLOSE (unit=iunit)
print*,'Nrows=',rows_number
    return
  end function rows_number
  
  subroutine read_columns(filename,iunit,nrows,Ncolumns,data)
    character (LEN=200)::filename
    integer:: reason,iunit,nrows,Ncolumns,i
    real(dp):: col1,col2,col3,col4,col5,col6,col7,col8,col9,col10
    real(dp)::data(:,:)

    data(:,:)=0.
    if (Ncolumns >10) then 
       print*,'Number of columns not supported!'
       stop
    endif
close(iunit)
    open (UNIT=iunit,file=filename,status='unknown',form="formatted")

    SELECT CASE(Ncolumns)

    CASE(1)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1
          data(i,1)=col1
       ENDDO
    CASE(2)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2
          data(i,1)=col1
          data(i,2)=col2
       ENDDO
    CASE(3)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2,col3
          data(i,1)=col1
          data(i,2)=col2
          data(i,3)=col3
       ENDDO

    CASE(4)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2,col3,col4
          data(i,1)=col1
          data(i,2)=col2
          data(i,3)=col3
          data(i,4)=col4
       ENDDO
    CASE(5)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2,col3,col4,col5
          data(i,1)=col1
          data(i,2)=col2
          data(i,3)=col3
          data(i,4)=col4
          data(i,5)=col5
       ENDDO
    CASE(6)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2,col3,col4,col5,col6
          data(i,1)=col1
          data(i,2)=col2
          data(i,3)=col3
          data(i,4)=col4
          data(i,5)=col5
          data(i,6)=col6
       ENDDO

    CASE(7)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2,col3,col4,col5,col7
          data(i,1)=col1
          data(i,2)=col2
          data(i,3)=col3
          data(i,4)=col4
          data(i,5)=col5
          data(i,6)=col6
          data(i,7)=col7
       ENDDO
    CASE(8)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2,col3,col4,col5,col7,col8
          data(i,1)=col1
          data(i,2)=col2
          data(i,3)=col3
          data(i,4)=col4
          data(i,5)=col5
          data(i,6)=col6
          data(i,7)=col7
          data(i,8)=col8
       ENDDO

    CASE(9)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2,col3,col4,col5,col7,col8,col9
          data(i,1)=col1
          data(i,2)=col2
          data(i,3)=col3
          data(i,4)=col4
          data(i,5)=col5
          data(i,6)=col6
          data(i,7)=col7
          data(i,8)=col8
          data(i,9)=col9
       ENDDO

    CASE(10)
       DO i=1,nrows
          READ(iunit,*,IOSTAT=Reason)  col1,col2,col3,col4,col5,col7,col8,col9,col10
          data(i,1)=col1
          data(i,2)=col2
          data(i,3)=col3
          data(i,4)=col4
          data(i,5)=col5
          data(i,6)=col6
          data(i,7)=col7
          data(i,8)=col8
          data(i,9)=col9
          data(i,10)=col10
       ENDDO
    END SELECT
  CLOSE (unit=iunit)
 end subroutine read_columns


 subroutine write_columns(filename,iunit,nrows,Ncolumns,data)
    character (LEN=200)::filename
    integer:: reason,iunit,nrows,Ncolumns,i
    real(dp):: col1,col2,col3,col4,col5,col6,col7,col8,col9,col10
    real(dp)::data(:,:)

   
    if (Ncolumns >10) then 
       print*,'Number of columns not supported!'
       stop
    endif

    close(iunit)
    open (UNIT=iunit,file=filename,status='unknown',form="formatted")

    SELECT CASE(Ncolumns)

    CASE(1)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason)  data(i,1)         
       ENDDO
    CASE(2)
       DO i=1,nrows
           write(iunit,*,IOSTAT=Reason)   data(i,1), data(i,2)
        ENDDO
    CASE(3)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason)  data(i,1), data(i,2),data(i,3)
       ENDDO

    CASE(4)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason)  data(i,1), data(i,2),data(i,3),data(i,4)
          
       ENDDO
    CASE(5)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason) data(i,1), data(i,2),data(i,3),data(i,4)&
               &,data(i,5)
          
       ENDDO
    CASE(6)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason) data(i,1), data(i,2),data(i,3),data(i,4)&
               &,data(i,5),data(i,6)
          
       ENDDO

    CASE(7)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason)  data(i,1), data(i,2),data(i,3),data(i,4)&
               &,data(i,5),data(i,6),data(i,7)
          
       ENDDO
    CASE(8)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason)  data(i,1), data(i,2),data(i,3),data(i,4)&
               &,data(i,5),data(i,6),data(i,7),data(i,8)
          
       ENDDO

    CASE(9)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason)  data(i,1), data(i,2),data(i,3),data(i,4)&
               &,data(i,5),data(i,6),data(i,7),data(i,8),data(i,9)
          
       ENDDO

    CASE(10)
       DO i=1,nrows
          write(iunit,*,IOSTAT=Reason)   data(i,1), data(i,2),data(i,3),data(i,4)&
               &,data(i,5),data(i,6),data(i,7),data(i,8),data(i,9),data(i,10)
          
       ENDDO
    END SELECT
  CLOSE (unit=iunit)
 end subroutine write_columns


 



  end module spectra_io
