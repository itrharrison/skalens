program spectra
  use udgrade_nr
  use healpix_types
  USE fitstools
  USE utilities
  USE pix_tools
  use spectra_io
  USE random_tools
  use alm_tools
  USE head_fits, ONLY : write_minimal_header
  USE paramfile_io, ONLY : paramfile_handle, parse_init, parse_int, &
       parse_string, parse_double, parse_lgt, concatnl
  USE extension, ONLY : getEnvironment, getArgument, nArguments
  implicit none
  character(LEN=filenamelen)::paramfile,description,dummy
  character(LEN=filenamelen)::filename,filename2
  character(LEN=filenamelen),allocatable::shearfiles(:),shearfiles_out(:),catfiles(:)
  character(LEN=filenamelen),allocatable::alm_shearfiles(:),alm_maskfiles(:)
  character(LEN=filenamelen),allocatable::cl_shearfiles(:),cl_maskfiles(:),mapfiles(:),maskfiles(:)
  real(dp),allocatable::dw8(:,:),minz(:),maxz(:),minzcats(:),maxzcats(:),deltal(:),deltab(:),data(:,:)
  real(sp),allocatable::cl(:,:),clout(:,:),clsum(:,:)
  complex, DIMENSION(:,:,:), ALLOCATABLE :: alm1, alm2
  real(sp),allocatable::shearmap_copy(:,:),shearmap(:,:),shearmap_low(:,:)
  real(sp),allocatable::latitudes(:),longitudes(:),err1(:),err2(:),ztrue(:),zobs(:)
  real(dp)::theta,phi,zbounds(2),z,db,dl
  real(sp)::eabs,e1,e2,beta,minzcat,maxzcat
  !  integer,allocatable::bincats(:)
  integer::Nbinz,Nbinz_out,Nspectra,pixel,pixel_in,ncl,next,nlheader,nalms,ncount,nit,it,nrows
  integer::npixtot,ordering,ordering_map,npixtot_old,nside,nmaps,nsmax,nlmax,nmmax,npixtot_out,nside_out,ordering_old
  integer::i,ii,j,iostat,l,nbin,l2,p(1),count_f,row,out_choice,mc_choice,noiseweight
  integer::iseed
  integer,allocatable::ngalaxies(:)
  TYPE(paramfile_handle) :: handle
  CHARACTER(LEN=20) :: output,output2,tag
  CHARACTER(LEN=10) :: tag2
  CHARACTER(LEN=80), DIMENSION(1:120) :: header
  CHARACTER(LEN=80), DIMENSION(1:120) :: mapheader
  CHARACTER(LEN=80), DIMENSION(1:120,1:3) :: header2
  integer(4) :: ic4, crate4, cmax4,ni
  INTEGER, DIMENSION(8,2) :: values_time
  REAL(SP) :: clock_time
  CHARACTER(LEN=*), PARAMETER :: code ="LensSpectra"
  LOGICAL(kind=LGT) :: polarisation, bcoupling
  character(len=20)                   :: coordsys
  CHARACTER(LEN=80)::line  
  CHARACTER(LEN=10) ::output3

  !double precision ispline
  save iseed

  !getting seed from the clock
  call system_clock(count=ic4, count_rate=crate4, count_max=cmax4)
  iseed=ic4
  write(tag2,"(i10)")iseed

  call date_and_time(values = values_time(:,1))

  !1)input file:
  !  -bins in redshift
  !  -number of objects

  nit=50
  if (nArguments() == 0) then
     paramfile=''
  else if (nArguments() == 1) then
     call getArgument(1,paramfile)
  else 
     print '("Usage: QML: [parameter file name]")'
     stop 1
  endif

  handle = parse_init(paramfile)
  description = concatnl( &
       & " Enter number of catalogues and shear maps")
  Nbinz = parse_int(handle, 'Nbinz_in', default=5, vmin=1, descr=description)

  handle = parse_init(paramfile)
  description = concatnl( &
       & " Enter number of redshift slices ")
  Nbinz_out = parse_int(handle, 'Nbinz_out', default=Nbinz, vmin=1, vmax=Nbinz,descr=description)
  ! Nbinz_new is the binning scheme. can be different from the number of original slices

  allocate(minz(Nbinz_out),maxz(Nbinz_out),minzcats(Nbinz),maxzcats(Nbinz),ngalaxies(Nbinz_out))

  do i=1,Nbinz_out
     write(output,"(i3)")i
     output=ADJUSTL(output)
     l=LEN_TRIM(output)

     dummy = 'minz'//output(:l)
     description = concatnl( &
          & " Enter the minimum redshift for slice")
     minz(i)=parse_double(handle,dummy, default=0.d0, descr=description)
     dummy = 'maxz'//output(:l)
     maxz(i)=parse_double(handle,dummy, default=0.d0, descr=description)
  enddo

  Nspectra=0
  do i=1,Nbinz_out
     do j=1,i
        Nspectra=Nspectra+1      
     enddo
  enddo

  print*,'number of spectra=',Nspectra

  allocate(deltal(Nbinz),deltab(Nbinz),shearfiles(Nbinz),catfiles(Nbinz),alm_shearfiles(Nbinz_out),&
       &alm_maskfiles(Nbinz_out),mapfiles(Nbinz_out),maskfiles(Nbinz_out),&
       &cl_shearfiles(Nspectra),cl_maskfiles(Nspectra),stat=iostat)
  if (iostat /=0) then
     print*,'Error allocating filenames'
     stop
  endif

  description = concatnl( &
       & " Output fits maps and masks?")
  out_choice = parse_int(handle, 'out_choice', default=0, vmin=0, vmax=1, descr=description)

!!$  description = concatnl( &
!!$       & " Inverse noise weighting? 0=NO, 1=YES")
!!$  noiseweight = parse_int(handle, 'noise_weight', default=1, vmin=0, vmax=1, descr=description)
  !out_choice=0 ->no output maps
  !out_choice=1 ->output maps

  description = concatnl( &
       & " Do you want to perform noise MC?")
  mc_choice = parse_int(handle, 'mc_choice', default=0, vmin=0, vmax=1, descr=description)

  !mc_choice=0 -> no MC
  !mc_choice=1 -> MC

  do i=1,Nbinz
     write(output,"(i3)")i
     output=ADJUSTL(output)
     l=LEN_TRIM(output)

     dummy = 'map'//output(:l)
     description = concatnl( &
          & " Enter the name of the input shear map")
     shearfiles(i)=parse_string(handle,dummy, default='', descr=description)
     ! these are shear maps for map_choice=0 and observed shear maps for map_choice=1

     dummy = 'cat'//output(:l)
     description = concatnl( &
          & " Enter the name of the input catalogue)")
     catfiles(i)=parse_string(handle,dummy, default='', descr=description)
!!$     bincats(i)=i
!!$     if (Nbinz_out /= Nbinz) then 
!!$        dummy = 'bincat'//output(:l)
!!$        description = concatnl( &
!!$             & " Enter bin number for cat")
!!$        bincats(i) = parse_int(handle, dummy, default=i, vmin=0, vmax=Nbinz_out, descr=description)
!!$     endif

     dummy = 'deltab'//output(:l)
     description = concatnl( &
          & " Enter the latitude displacement")
     deltab(i)=parse_double(handle,dummy, default=0.d0, descr=description)

     dummy = 'deltal'//output(:l)
     description = concatnl( &
          & " Enter the longitude displacement")
     deltal(i)=parse_double(handle,dummy, default=0.d0, descr=description)
  enddo


  description = concatnl( &
       & " Enter the tag name for output spectra files")
  tag=parse_string(handle,'tag', default='', descr=description)


  Nspectra=0

  do i=1,Nbinz_out
     write(output,"(i3)")i
     output=ADJUSTL(output)
     l=LEN_TRIM(output)
     alm_shearfiles(i)="alm"//output(:l)//"_"//trim(tag2)//".tmp"
     alm_maskfiles(i)="almm"//output(:l)//"_"//trim(tag2)//".tmp"

     mapfiles(i)="map"//output(:l)//"_"//trim(tag)//".fits"
     maskfiles(i)="mask"//output(:l)//"_"//trim(tag)//".fits"

     do j=1,i
        write(output2,"(i3)")j
        output2=ADJUSTL(output2)
        l2=LEN_TRIM(output2)
        Nspectra=Nspectra+1
        cl_shearfiles(Nspectra)="cl_"//output(:l)//output2(:l2)//"_"//trim(tag)//".fits"
        cl_maskfiles(Nspectra)="cl_mask_"//output(:l)//output2(:l2)//"_"//trim(tag)//".fits" 
     enddo
  enddo

  description = concatnl( &
       & " Enter the NSIDE of the output map")
  nside_out = parse_int(handle, 'nside_out', default=1024, vmin=64, descr=description)
  npixtot_out=12*nside_out*nside_out

  !  if (map_choice==1) then
  !check shear maps parameters and allocate
  !these are input shearmaps for map_choice==0 and opserved shearmaps for map_choice==1
  npixtot_old=-1
  ordering_old=-1


  do i=1,Nbinz
     !read TQU maps. could be different nside? need to convert to RING for map2alm
     !check shear maps
     call get_info(shearfiles(i),npixtot,nmaps,ordering,nside)
     print*,trim(shearfiles(i)),'   npixtot=',npixtot
     if (nmaps  /=3) then
        print*,'Error: 3 fields expected in input map, found',nmaps
        stop
     endif
     if ((npixtot /=npixtot_old) .and. (npixtot_old/=-1))  then
        print*,'Error: the maps have different NSIDE'
     endif
     if ((ordering /=ordering_old) .and. (ordering_old/=-1))  then
        print*,'Error: the maps have different NSIDE'
     endif
     ordering_old=ordering
     npixtot_old=npixtot

     !store  min and max redshift of each catalogue here
     print*,'redshift range catalogue ',trim(catfiles(i))
     !  call rows_catalogue(catfiles(i),11,ncount)
     call range_catalogue(catfiles(i),11,minzcat,maxzcat)

     !allocate(latitudes(ncount),longitudes(ncount),err1(ncount),err2(ncount),ztrue(ncount),zobs(ncount))
     !call read_catalogue(catfiles(i),11,ncount,latitudes,longitudes,err1,err2,ztrue,zobs)
     !minzcat=minval(zobs)
     !maxzcat=maxval(zobs)
     minzcats(i)=minzcat!(1)
     maxzcats(i)=maxzcat!(1)
     print*,minzcats(i),maxzcats(i)
     !deallocate(latitudes,longitudes,err1,err2,ztrue,zobs)
  enddo

  if (nside < nside_out) then 
     print*,'Resolution of maps cannot be better than NSIDE=',nside
     nside_out=nside
     npixtot_out=12*nside_out*nside_out
  endif

  ordering_map=ordering

  allocate(shearmap(0:npixtot-1,1:nmaps),shearmap_copy(0:npixtot_out-1,1:nmaps),&
       &shearmap_low(0:npixtot_out-1,1:nmaps),stat=iostat)
  if (iostat /=0) then
     print*,'Error allocating maps'
     stop
  endif
  shearmap_copy(:,:)=0. ! observed shear maps


  !allocate arrays for alm and cl computation
  nsmax=nside_out
  nlmax=nsmax*2
  nmmax=nlmax
  polarisation=.true.
  !bcoupling=.false.
  !ncl=4
  bcoupling=.true.
  ncl=6
  allocate(dw8(1:2*nsmax,1:3),alm1(1:3,0:nlmax,0:nmmax),cl(0:nlmax,1:6),clout(0:nlmax,1:1))
  if (iostat /=0) then
     print*,'Error allocating arrays for alm computation'
     stop
  endif
  dw8(:,:)=1._dp

  ngalaxies(:)=0
  do ii=1,Nbinz_out
     Print*,'Bin ',ii

     shearmap_copy(:,:)=0. ! adding more redshift bins
     do i=1,Nbinz 
        dl=deltal(i) !displacements
        db=deltab(i)
        minzcat=minzcats(i)
        maxzcat=maxzcats(i)
        print*,'Catalogue redshift range',minzcat,maxzcat

        ! skip this catalogue if no galaxy is in the observed redshift range
        if ((minzcat > maxz(ii)) .or. (maxzcat < minz(ii))) goto 100  

        print*,'loading catalogue ',trim(catfiles(i))
        call rows_catalogue(catfiles(i),11,ncount)
        allocate(latitudes(ncount),longitudes(ncount),err1(ncount),err2(ncount),ztrue(ncount),zobs(ncount))
        call read_catalogue(catfiles(i),11,ncount,latitudes,longitudes,err1,err2,ztrue,zobs)

        print*,'loading shear map ',trim(shearfiles(i))

        call input_map(shearfiles(i),shearmap,npixtot,nmaps,header=mapheader)

        ! here change to ring ordering (for map2alm)
        if (ordering_map==2) then 
           print*,'changing ordering of shearmap from NEST to RING'
           call convert_nest2ring(nside,shearmap)
           !todo:modify header for new ordering
        endif
        !shearmap is now in RING ordering

        if (npixtot_out /=npixtot) then
           print*,'changing resolution of shearmap',nside,nside_out
           call udgrade_ring(shearmap, nside, shearmap_low, nside_out)

           !modify header for new resolution
           write(output,"(i4)")nside_out
           output=ADJUSTL(output)
           l=LEN_TRIM(output)
           line="NSIDE   =                 "//output(:l)//" / Resolution parameter for HEALPIX"
           mapheader(7)=line

           write(output3,"(i10)")npixtot_out-1
           output3=ADJUSTL(output3)
           l=LEN_TRIM(output3)
           line="LASTPIX =             "//output3(:l)//" / Last pixel # (0 based)"
           mapheader(9)=line


        else
           shearmap_low=shearmap
        endif
        !endif

        !   if (map_choice==0) then 

        !read catalogues
        !ncount=number of galaxies


        ! if (noiseweight==0) sigma1(:)=1.

        do j=1,ncount
           theta=(latitudes(j)+db)*pi/180.+pi/2.
           if (theta >pi) theta=theta-pi
           if (theta <0.) theta=theta+pi
           phi=(longitudes(j)+dl)*pi/180.
           if (phi >2.*pi) phi=phi-2.*pi
           if (phi <0.) phi=phi+2.*pi
           z=zobs(j)

           if ((z >=minz(ii)) .and. (z<maxz(ii))) then
              ngalaxies(ii)=ngalaxies(ii)+1
              call ang2pix_ring(nside_out, theta, phi, pixel) !pixel of low resolution map shearmap_low





              shearmap_copy(pixel,1)=shearmap_copy(pixel,1)+1.  !number of galaxies in this pixel
              shearmap_copy(pixel,2)=shearmap_copy(pixel,2)+err1(j)+shearmap_low(pixel,2) 
              shearmap_copy(pixel,3)=shearmap_copy(pixel,3)+err2(j)+shearmap_low(pixel,3) 
           endif
        enddo

        deallocate(latitudes,longitudes,err1,err2,ztrue,zobs)

100     continue
     enddo
     !map normalizations
     do j=0,npixtot_out-1
        if (shearmap_copy(j,1) /= 0.) then
           shearmap_copy(j,2)=shearmap_copy(j,2)/shearmap_copy(j,1)
           shearmap_copy(j,3)=shearmap_copy(j,3)/shearmap_copy(j,1)
        endif
     enddo


     zbounds(:)=0._dp
     !     print*,'call map2alm map'
     call map2alm(nsmax,nlmax,nmmax,shearmap_copy,alm1,zbounds,dw8)


     open(10,file=alm_shearfiles(ii), status='unknown', form='unformatted')
     write(10) alm1
     close(10)
     !     print*,'done'


     !construct mask (over shearmap_low)
     shearmap_low(:,:)=1.
     do j=0,npixtot_out-1
        if (shearmap_copy(j,2) == 0.) shearmap_low(J,:)=0. ! mask ba
     enddo


     if (out_choice==1) then
        print*,'writing maps'
        filename=mapfiles(ii)

        open(unit=1234, iostat=iostat, file=filename, status='old')
        if (iostat.eq.0) close(1234, status='delete')
        call output_map(shearmap_copy, mapheader, filename)
        filename=maskfiles(ii)

        open(unit=1234, iostat=iostat, file=filename, status='old')
        if (iostat.eq.0) close(1234, status='delete')
        call output_map(shearmap_low, mapheader, filename)
        print*,'done'
     endif

     !     print*,'call map2alm mask'
     call map2alm(nsmax,nlmax,nmmax,shearmap_low,alm1,zbounds,dw8)
     open(10,file=alm_maskfiles(ii), status='unknown', form='unformatted')
     write(10) alm1
     close(10)
     !     print*,'done'
     print*,'Bin',ii,'done'
  enddo ! loop on redshift bin


  print*,'CONTEGGI',ngalaxies

  deallocate(shearmap,shearmap_low,dw8,stat=iostat)
  if (iostat /=0) then
     print*,'Deallocation error'
     stop
  endif

  allocate(alm2(1:3,0:nlmax,0:nmmax))


  print*,'Computing and outputting spectra'
  ! crosspectrum alm_i, alm_j shearmmap
  Nspectra=0
  do i=1,Nbinz_out

     open(10,file=alm_shearfiles(i), status='old', form='unformatted')
     read(10) alm1
     close(10)

     do j=1,i
        open(11,file=alm_shearfiles(j), status='old', form='unformatted')
        read(11) alm2
        close(11)

        call alm2cl(nlmax,nmmax,alm1,alm2,cl)
        Nspectra=Nspectra+1
        filename=cl_shearfiles(Nspectra)


        call write_minimal_header(header,'cl', &
             creator = 'LensSpectra', version = '', polar=polarisation, &
             nlmax = nlmax, bcross = bcoupling, nside = nsmax, coordsys = coordsys, &
             units = ' ')
        nlheader = SIZE(header)
        !remove old file
        open(unit=1234, iostat=iostat, file=filename, status='old')
        if (iostat.eq.0) close(1234, status='delete')
        call write_asctab(cl,nlmax, ncl, header, nlheader, filename)

     enddo
  enddo



  ! crosspectrum alm_i, alm_j mask (only TT spectrum!)
  polarisation=.false.
  bcoupling=.false.

  Nspectra=0  
  do i=1,Nbinz_out

     open(10,file=alm_maskfiles(i), status='old', form='unformatted')
     read(10) alm1
     close(10)

     do j=1,i

        open(11,file=alm_maskfiles(j), status='old', form='unformatted')
        read(11) alm2
        close(11)

        call alm2cl(nlmax,nmmax,alm1,alm2,cl)
        Nspectra=Nspectra+1
        filename=cl_maskfiles(Nspectra)

        clout(:,1)=cl(:,1)

        call write_minimal_header(header,'cl', &
             creator = 'LensSpectra', version = '', polar=polarisation, &
             nlmax = nlmax, bcross = bcoupling, nside = nsmax, coordsys = coordsys, &
             units = ' ')

        nlheader = SIZE(header)

        !remove old file
        open(unit=1234, iostat=iostat, file=filename, status='old')
        if (iostat.eq.0) close(1234, status='delete')

        call write_asctab(clout,nlmax, 1, header, nlheader, filename)

     enddo
  enddo

  !deleting temporary alm files

  do i=1,Nbinz_out
     filename=alm_maskfiles(i)
     open(unit=1234, iostat=iostat, file=filename, status='old')
     if (iostat.eq.0) close(1234, status='delete')
     filename=alm_shearfiles(i)
     open(unit=1234, iostat=iostat, file=filename, status='old')
     if (iostat.eq.0) close(1234, status='delete')
  enddo


  if (mc_choice==1) then
     print*,'*************************'
     print*,'Starting Monte Carlo'
     print*,'*************************'
     !MC only for auto spectra
     !only for catalogue input
     polarisation=.true.
     bcoupling=.true.
     ncl=6

     allocate(clsum(0:nlmax,1:6),stat=iostat) ! noise power spectrum
     if (iostat /=0) then
        print*,'Error allocating filenames'
        stop
     endif

     allocate(shearmap(0:npixtot-1,1:nmaps),shearmap_low(0:npixtot_out-1,1:nmaps),stat=iostat)
     if (iostat /=0) then
        print*,'Error allocating maps'
        stop
     endif


     do ii=1,Nbinz_out
        print*,'Redshift bin',ii
        write(output,"(i3)")ii
        output=ADJUSTL(output)
        l=LEN_TRIM(output)
        filename="cl_noise_"//output(:l)//output(:l)//"_"//trim(tag)//".fits"

        ! reading relevant files and keep |e| and coordinates for all gals with obs z in the range
        !filename2="catalogue_"//output(:l)//output(:l)//"_"//trim(tag)//".tmp"

        allocate(data(ngalaxies(ii),3))
        nrows=0


        !    close(6)
        !    open(unit=6, iostat=iostat, file=filename2, status='unknown', form='formatted')
        print*,'allocato',ngalaxies(ii)
        do i=1,Nbinz
           dl=deltal(i)
           db=deltab(i)
           minzcat=minzcats(i)
           maxzcat=maxzcats(i)

           if ((minzcat > maxz(ii)) .or. (maxzcat < minz(ii))) goto 200 

           print*,'loading catalogue ',trim(catfiles(i))
           call rows_catalogue(catfiles(i),11,ncount)
           allocate(latitudes(ncount),longitudes(ncount),err1(ncount),err2(ncount),ztrue(ncount),zobs(ncount))
           call read_catalogue(catfiles(i),11,ncount,latitudes,longitudes,err1,err2,ztrue,zobs)

           print*,'loading shear map ',trim(shearfiles(i))

           call input_map(shearfiles(i),shearmap,npixtot,nmaps,header=mapheader)

           ! here change to ring ordering (for map2alm)
           if (ordering_map==2) then 
              print*,'changing ordering of shearmap from NEST to RING'
              call convert_nest2ring(nside,shearmap)
              !todo:modify header for new ordering
           endif
           !shearmap is now in RING ordering

           if (npixtot_out /=npixtot) then
              print*,'changing resolution of shearmap',nside,nside_out
              call udgrade_ring(shearmap, nside, shearmap_low, nside_out)

              !modify header for new resolution
              write(output,"(i4)")nside_out
              output=ADJUSTL(output)
              l=LEN_TRIM(output)
              line="NSIDE   =                 "//output(:l)//" / Resolution parameter for HEALPIX"
              mapheader(7)=line

              write(output3,"(i10)")npixtot_out-1
              output3=ADJUSTL(output3)
              l=LEN_TRIM(output3)
              line="LASTPIX =             "//output3(:l)//" / Last pixel # (0 based)"
              mapheader(9)=line


           else
              shearmap_low=shearmap
           endif

           do j=1,ncount
              z=zobs(j)

              if ((z >=minz(ii)) .and. (z<maxz(ii))) then

                 theta=(latitudes(j)+db)*pi/180.+pi/2.
                 if (theta >pi) theta=theta-pi
                 if (theta <0.) theta=theta+pi
                 phi=(longitudes(j)+dl)*pi/180.
                 if (phi >2.*pi) phi=phi-2.*pi
                 if (phi <0.) phi=phi+2.*pi


                 call ang2pix_ring(nside_out, theta, phi, pixel) !pixel of low resolution map shearmap_low

                 eabs=sqrt((err1(j)+shearmap_low(pixel,2))**2+(err2(j)+shearmap_low(pixel,3))**2)
!!$                 beta=ran_mwc(iseed)*2.*PI
!!$                 e1=eabs*cos(2.*beta)
!!$                 e2=eabs*sin(2.*beta)
!!$
!!$
!!$                 shearmap_copy(pixel,1)=shearmap_copy(pixel,1)+1.
!!$                 shearmap_copy(pixel,2)=shearmap_copy(pixel,2)+e1
!!$                 shearmap_copy(pixel,3)=shearmap_copy(pixel,3)+e2
                 nrows=nrows+1
                 data(nrows,1)=eabs
                 data(nrows,2)=theta
                 data(nrows,3)=phi
!!$print*,data(nrows,1),data(nrows,2),data(nrows,3)
!!$stop              
                 !   write(6,*),eabs,theta,phi
              endif
           enddo
           deallocate(latitudes,longitudes,err1,err2,ztrue,zobs)
200        continue
        enddo
        !        close(6)

        !  nrows= rows_number(filename2,6)
        !  allocate(data(nrows,3))  !abs(e),theta,phi
        !  call read_columns(filename2,6,nrows,3,data)

!!$        print*,data(:,1)
!!$        stop
        print*,'controllo',nrows,ngalaxies(ii)
        !   open(unit=6, iostat=iostat, file=filename2, status='unknown', form='formatted')
        !   if (iostat.eq.0) close(6, status='delete')

        clsum(:,:)=0.
        do it=1,nit !iterations
           print*,'iteration',it
           shearmap_copy(:,:)=0.

           do i=1,nrows
              eabs=data(i,1)
              theta=data(i,2)
              phi=data(i,3)

              call ang2pix_ring(nside_out, theta, phi, pixel)

              beta=ran_mwc(iseed)*2.*PI
              e1=eabs*cos(2.*beta)
              e2=eabs*sin(2.*beta)


              shearmap_copy(pixel,1)=shearmap_copy(pixel,1)+1.
              shearmap_copy(pixel,2)=shearmap_copy(pixel,2)+e1
              shearmap_copy(pixel,3)=shearmap_copy(pixel,3)+e2
!!$print*,pixel,shearmap_copy(pixel,1),shearmap_copy(pixel,2),shearmap_copy(pixel,3)
!!$stop
           enddo
           !map normalizations
           do j=0,npixtot_out-1
              if (shearmap_copy(j,1) /= 0.) then
                 shearmap_copy(j,2)=shearmap_copy(j,2)/shearmap_copy(j,1)
                 shearmap_copy(j,3)=shearmap_copy(j,3)/shearmap_copy(j,1)
              endif
           enddo


           zbounds(:)=0._dp
           !     print*,'call map2alm map'
           call map2alm(nsmax,nlmax,nmmax,shearmap_copy,alm1,zbounds,dw8)
           call alm2cl(nlmax,nmmax,alm1,alm1,cl)
           clsum=clsum+cl

        enddo

        clsum=clsum/dble(nit)

        call write_minimal_header(header,'cl', &
             creator = 'LensSpectra', version = '', polar=polarisation, &
             nlmax = nlmax, bcross = bcoupling, nside = nsmax, coordsys = coordsys, &
             units = ' ')
        nlheader = SIZE(header)

        !remove old file
        open(unit=1234, iostat=iostat, file=filename, status='old')
        if (iostat.eq.0) close(1234, status='delete')
        call write_asctab(clsum,nlmax, ncl, header, nlheader, filename)

        deallocate(data)
     enddo

     deallocate(clsum,shearmap,shearmap_low)
  endif

  !free all memory
  deallocate(alm1,alm2,cl,clout,shearmap_copy)
  !alm2

  deallocate(maskfiles,alm_maskfiles,mapfiles,alm_shearfiles,ngalaxies)

  call date_and_time(values = values_time(:,2))

  values_time(:,1) = values_time(:,2) - values_time(:,1)
  clock_time =  (  (values_time(3,1)*24 &
       &           + values_time(5,1))*60. &
       &           + values_time(6,1))*60. &
       &           + values_time(7,1) &
       &           + values_time(8,1)/1000.
  PRINT*,"Total clock time [m]:",clock_time/60.
  PRINT*,"               "//code//" > Normal completion"
end program spectra
