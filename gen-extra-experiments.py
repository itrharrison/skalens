'''
Script to generate ini files for extra skalens runs.

Usage:
python gen-extra-experiments.py
python gen-pipeline.py [ini file]

Runs to do:
psf   nu    askies...               bandwidth
0.5   1.4   1000    5000    10000   30%
0.5   1.4   1000    5000    10000   100%
lowest at 0.950   1.085   1000   5000    100000    30%
winnner    winner    1000   5000    100000    30%
'''

import numpy as np
import cPickle as pickle
import os
from matplotlib import pyplot as plt
plt.close('all')

from astropy.constants import c
from astropy import units as uns

import kolmogorov_psf as kpsf
import rb_sens_curve as scurve

# find the ionsphere psf at the lowest nu in band 2
koopmans_r0 = 10.e3
koopmans_nu = 150.e6
b2l_nu = 1.085e9
b2l_fried = kpsf.fried(b2l_nu, r0=koopmans_r0, nu_1=koopmans_nu)
b2l_lam = c.value/b2l_nu

b2l_fwhm = kpsf.kolmogorov_fwhm(b2l_lam, b2l_fried)*uns.rad
b2l_fwhm = b2l_fwhm.to(uns.arcsec).value

# define the experiments and parameters
experiments = [
               'band2canonical',
               'band2canonical-fullband',
               'band2lower',
               'winner'
               ]

experiment_parameters = {
                         'band2canonical' : {'psf' : 0.5, 'nu' : 1.4, 'bw' : 1},
                         'band2canonical-fullband' : {'psf' : 0.5, 'nu' : 1.4, 'bw' : 3},
                         'band2lower' : {'psf' : b2l_fwhm, 'nu' : 1.085, 'bw' : 1},
                         'winner' : {'psf' : 0.556, 'nu' : 1., 'bw' : 1}
                         }

askies = [
          1000,
          5000,
          10000
          ]

# generate the interpolation function for the rebaselined SKA sensitivity
rb_curve, x, y, z = scurve.createSensitivityCurve('data/rebaselined_SKA1_MID_Cont_Survey_Sensitivity.txt')

for exr in experiments:
  for asky in askies:

    parms = experiment_parameters[exr]

    # find the Srms for this experiment, sky area
    a = np.exp(np.exp(np.exp(rb_curve(np.log(parms['psf']), np.log(parms['nu'])))))
    parms['rms'] = (scurve.rescaleSrms(a, asky)/np.sqrt(parms['bw']))[0]

    #print(exr, asky, parms['psf'], parms['nu'], parms['bw'], parms['rms'])

    # generate the ini file from the template
    template = open('parameters.template').read()
    name = (exr+'_rb_psf_%.2e-nu_%.2e-asky_%d' % (parms['psf'], parms['nu'], asky))
    params_filename = 'inis/'+name+'_params.ini'
    params_file = template.format(name=name,
                                  srms=parms['rms'],
                                  asky=asky,
                                  nu=parms['nu'],
                                  psf=parms['psf'])
    open(params_filename, 'w').write(params_file)
    cmd = 'python gen-pipeline.py {0}'.format(params_filename)
    print(cmd)